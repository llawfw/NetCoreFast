/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysAuthorityBll : BaseBll<SysAuthority>, ISysAuthorityBll
    {
        public SysAuthorityBll(IBaseDAL<SysAuthority> dal):base(dal)
        {           
        }
        
    }
}
