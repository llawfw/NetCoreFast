/* author:QinYongcheng */

  using System;
using System.Collections.Generic;
using System.Text;
using DAL;
using Common.Util;
using Model.Entity.Sys;
using Common.MyAttribute;



namespace BLL.Sys.Impl
{
    public class SysUserBll : BaseBll<SysUser>, ISysUserBll
    {
        public SysUserBll(IBaseDAL<SysUser> dal):base(dal)
        {           
        }
        
        
        public new bool Add(SysUser o)
        {
            o.Pswd = Security.Md5(o.Pswd);
            return dal.Add(o);
        }
        public SysUser Login(string userame, string pswd)
        {
            pswd = Security.Md5(pswd);
            return dal.SelectOne(o => o.Username == userame && o.Pswd == pswd);
        }       
    
    }
}
