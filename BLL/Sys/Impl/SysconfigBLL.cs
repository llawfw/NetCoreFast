/* author:QinYongcheng */

  using System;
using System.Collections.Generic;
using System.Text;
using DAL;
using Common.Util;
using Model.Entity.Sys;
using Common.MyAttribute;



namespace BLL.Sys.Impl
{
    public class SysconfigBll : BaseBll<Sysconfig>, ISysconfigBll
    {
        public SysconfigBll(IBaseDAL<Sysconfig> dal):base(dal)
        {           
        }
        
        public Sysconfig SelectOneOrInit()
        {
            Sysconfig sysconfig = SelectOne(o => o.Id > 0);
            if (sysconfig == null)//没有记录 初始化
            {
                sysconfig = new Sysconfig() { Sys = true };
                bool ret = this.Add(sysconfig);
                return sysconfig;
            }
            return sysconfig;
        }
    
    }
}
