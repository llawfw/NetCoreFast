/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysDepartmentBll : BaseBll<SysDepartment>, ISysDepartmentBll
    {
        public SysDepartmentBll(IBaseDAL<SysDepartment> dal):base(dal)
        {           
        }
        
    }
}
