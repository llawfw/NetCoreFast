/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysResBll : BaseBll<SysRes>, ISysResBll
    {
        public SysResBll(IBaseDAL<SysRes> dal):base(dal)
        {           
        }
        
    }
}
