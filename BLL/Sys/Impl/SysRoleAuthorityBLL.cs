/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysRoleAuthorityBll : BaseBll<SysRoleAuthority>, ISysRoleAuthorityBll
    {
        public SysRoleAuthorityBll(IBaseDAL<SysRoleAuthority> dal):base(dal)
        {           
        }
        
    }
}
