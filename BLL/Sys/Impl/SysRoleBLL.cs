/* author:QinYongcheng */


using System;
using System.Collections.Generic; 
using System.Text;
using DAL;
using Model.Entity.Sys;


namespace BLL.Sys.Impl
{
    public class SysRoleBll : BaseBll<SysRole>, ISysRoleBll
    {
        public SysRoleBll(IBaseDAL<SysRole> dal):base(dal)
        {           
        }
        
    }
}
