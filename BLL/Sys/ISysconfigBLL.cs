/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Text;
using Model.Entity.Sys;
namespace BLL.Sys 
{
   
    public interface ISysconfigBll:IBaseBll<Sysconfig>
    {
       
        /// <summary>
        /// 查询配置或初始化
        /// </summary>
        /// <returns></returns>
        Sysconfig SelectOneOrInit();
     
    }
}
