﻿/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.MyAttribute
{
    /// <summary>
    /// controller生成行为注解
    /// <remark>
    /// 指明实体属性对于生成list、add、update、detail哪种行为不需要输出controller方法
    /// </remark>
    /// </summary>
    public class ControllerGenAttribute : Attribute
    {
        /// <summary>
        /// 生成列表查询方法
        /// </summary>
        public bool List
        {
            get;
            set;     
        }=false;
        /// <summary>
        /// 生成添加方法
        /// </summary>
        public bool Add
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 生成修改方法
        /// </summary>
        public bool Update
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 生成详细方法
        /// </summary>
        public bool Detail
        {
            get;
            set;
        } = false;
        /// <summary>
        /// 生成删除方法
        /// </summary>
        public bool Delete
        {
            get;
            set;
        } = false;
    }
}
