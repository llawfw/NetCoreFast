using Common.MyAttribute;
using Common.Util;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Metadata;
using System.Text;
using static System.Net.WebRequestMethods;

namespace Common
{
    /// <summary>
    /// 查询参数。基于System.Linq.Dynamic.Core
    /// <see cref="https://dynamic-linq.net/basic-simple-query"/>
    /// <see cref="https://article.itxueyuan.com/WXdQ2"/>
    /// </summary>
    public class QueryParam<TEntity>
    {
        private StringBuilder whereLINQ = new StringBuilder();
        private StringBuilder sortLINQ = new StringBuilder();
        public List<object> values = new List<object>();
        public string WhereLINQ { get { return whereLINQ.ToString(); } }
        public string SortLINQ { get { return sortLINQ.ToString(); } }
        public object[] Values { get { return values.ToArray(); } }
        /// <summary>
        /// 分页页码
        /// </summary>
        public int PageNo { get; set; } = 1;
        /// <summary>
        /// 分页每页显示记录数
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 解析查询字符串条件
        /// ①格式如key="parent.id__eq" value =1,如果添加时不加操作符 默认是等于eq 即如key=parent,实际key是parent__eq
        /// ②key的其他示例如 key="name__like"、key="name__eq"，查询的字段属性，带操作符合逻辑符，格式如parent.id__eq,如果有逻辑符合则格式：and/or__parent.id__eq
        /// ③value如果是in查询,多个值之间用","分隔。value值可以是null或者"null"字符串
        /// ④如果key是排序，值的格式为：+|-字段名,+|-字段名,多个排序用","隔开
        /// </summary>
        /// <param name="where">键是key,值是value。</param>
        public QueryParam<TEntity> Parse(Dictionary<string, string> where)
        {
            foreach (var w in where)
            {
                Parse(w.Key, w.Value);
            }
            return this;
        }
        /// <summary>
        /// 解析查询字符串条件
        /// ①格式如key="parent.id__eq" value =1,如果添加时不加操作符 默认是等于eq 即如key=parent,实际key是parent__eq
        /// ②key的其他示例如 key="name__like"、key="name__eq"
        /// ③value如果是in查询,多个值之间用","分隔。value值可以是null或者"null"字符串
        /// ④如果key是排序，值的格式为：+|-字段名,+|-字段名,多个排序用","隔开
        /// </summary>
        /// <param name="key">查询的字段属性，带操作符合逻辑符，格式如parent.id__eq,如果有逻辑符合则格式：and/or__parent.id__eq。</param>
        /// <param name="value">查询字段属性对应的值，如1,如果操作符是in，多个值之间用","分隔</param>
        public void Parse(string key, string value)
        {
            switch (key)
            {
                case PageParam.pageNo:
                    var pn = Convert.ToInt32(value);
                    PageNo = pn <= 0 ? PageNo : pn;
                    break;
                case PageParam.pageSize:
                    var ps = Convert.ToInt32(value);
                    PageSize = ps <= 0 ? PageSize : ps;
                    break;
                case PageParam.sort://排序 
                    if (!string.IsNullOrEmpty(value))
                    {
                        var orders = value.Split(",");
                        foreach (var order in orders)
                        {
                            if ("+Id" == order)
                            {
                                continue;
                            }
                            var orderType = "asc";
                            string orderkey = order;
                            if (order.StartsWith("+"))
                            {
                                orderkey = order.Substring(1);
                            }
                            else if (order.StartsWith("-"))
                            {
                                orderType = "desc";
                                orderkey = order.Substring(1);
                            }
                            if (sortLINQ.Length > 0)
                            {
                                sortLINQ.Append(",");
                            }
                            sortLINQ.AppendJoin(" ", orderkey, orderType);
                        }
                    }
                    break;
                default://查询字符串
                    var arr = key.Split("__");// and__name__like
                    var logic = " and "; var Key = ""; var op = "eq";
                    if (arr.Length == 3)
                    {
                        logic = arr[0];
                        Key = arr[1];
                        op = arr[2];
                    }
                    if (arr.Length == 2)
                    {
                        if (arr[0] == "and" || arr[0] == "or")
                        {
                            logic = arr[0];
                            Key = arr[1];
                        }
                        else
                        {
                            Key = arr[0];
                            op = arr[1];
                        }
                    }
                    if (arr.Length == 1)
                    {
                        Key = arr[0];
                    }
                    //没有对应属性或值未定义 就不加入条件  value="null" 表示查询条件为空
                    if (value != "null" && (!Reflect.HasProperty<TEntity>(Key) || value == "undefined"))
                    {
                        return;
                    }
                    build(logic, Key, op, value);
                    break;

            }

        }
        private void build(string logic, string key, string op, string value)
        {
            // c.Op = (ConditionOp)Enum.Parse(typeof(ConditionOp), op.ToUpperFirst())
            //判断操作符是否支持,不支持默认为“==”
            op = op.ToUpperFirst();
            if (!Enum.IsDefined(typeof(QueryOp), op))
            {
                op = "eq";
            }
            if (whereLINQ.Length > 0)
            {
                whereLINQ.Append(" ").Append(logic).Append(" ");
            }
            object val = value;
            switch (op)
            {
                case "Contains"://支持Contains
                    whereLINQ.AppendJoin("", key, ".Contains(@", values.Count, ")");
                    break;
                case "In"://支持In,in 后面的值是字符串格式，多个值之间“,”隔开，如"1,2,3,4,5"、"中国,美国,英国"
                    whereLINQ.AppendJoin(" ", key, "in", "@" + values.Count);
                    ParameterExpression Parameter = Expression.Parameter(typeof(TEntity));
                    MemberExpression member = Expression.Property(Parameter, key);//获取字段对应的数据类型
                    var valueArr = value.Split(',');
                    var genericTypeList = typeof(List<>).MakeGenericType(member.Type);//泛型集合list<>  
                    var vlist = Activator.CreateInstance(genericTypeList) as IList;      //创建泛型集合实例对象List<int> vlist = new List<int>();
                    foreach (var v in valueArr)
                    {
                        vlist.Add(Convert.ChangeType(v, member.Type));
                    }
                    val = vlist;//in 操作符后面是列表
                    break;
                default://常规操作符
                    if (value == "null")
                    {
                        whereLINQ.AppendJoin(" ", key, op.ToLower(), " null ");
                    }
                    else
                    {
                        whereLINQ.AppendJoin(" ", key, op.ToLower(), "@" + values.Count);
                    }
                    /*   不需要
                    if (value != "null")//转换为属性对应的值类型
                    {
                        val = Reflect.ConvertPropertyType<TEntity>(key, value);
                    }*/
                    break;
            }
            if (value != "null")
            {
                values.Add(val);//添加值
            }
        }
    }
    /// <summary>
    /// 支持的查询操作符
    /// <see cref="https://dynamic-linq.net/expression-language"/>
    /// </summary>
    public enum QueryOp
    {
        Eq,//等于
        Ne,//NotEqual不等于
        Gt,//Greater 大于
        Ge,// GreaterEqual 大于等于
        Lt,//Less  小于
        Le,//LessEqual 小于等于
        Contains,//字符串包含某个子字符串,等同于like模糊查询，如Name.Contains("覃")
        In,//包含在某个列表，如Id in [1,2,4,5]
        //Between 暂不支持
    }
}
