﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Common.Util
{
    /// <summary>
    /// 工具包
    /// </summary>
    public class Kit
    {
        /// <summary>
        /// 转化为int类型。如果待转化的字符串为空""或null，则默认转换为null
        /// </summary>
        /// <param name="str"></param>
        /// <param name="result"></param>
        /// <returns></returns>
       public static bool IntTryParse(object str,out int? result)
        {
            result = null;
            if (str==null)
            {
                return false;
            }
            try
            {
                result=int.Parse(str.ToString());
                return true;
            }
            catch 
            {
                return false;
            }
        }
        public static string GetSettings(string key)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            return configuration[key];
        }
    }
}

