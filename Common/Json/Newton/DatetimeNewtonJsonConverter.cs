﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;


namespace Common.Json.Newton
{
    public class DatetimeNewtonJsonConverter : JsonConverter<DateTime>
    {
        public override DateTime ReadJson(JsonReader reader, Type objectType, DateTime existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.Value==null)
            {
                //? default(DateTime?) : DateTime.Parse(reader.GetString());]
                return default(DateTime);
            }
            if (reader.TokenType == JsonToken.String)
            {
                if (DateTime.TryParse((string)reader.Value, out DateTime date)) return date;
            }
            return (DateTime)reader.ReadAsDateTime();
        }

        public override void WriteJson(JsonWriter writer, DateTime value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
