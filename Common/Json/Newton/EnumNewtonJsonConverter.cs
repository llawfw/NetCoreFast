﻿using Common.Util;
using Newtonsoft.Json;
using System;

namespace Common.Json.Newton
{
    public class EnumNewtonJsonConverter : JsonConverter<Enum>
    {
        public override Enum ReadJson(JsonReader reader, Type objectType, Enum existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Integer)
            {
                int num;
                num = (int)reader.ReadAsInt32();
                return (Enum)Enum.Parse(objectType, num.ToString());
            }
            if (reader.TokenType == JsonToken.String)
            {
                string str = reader.ReadAsString();
                return (Enum)Enum.Parse(objectType, str);
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, Enum value, JsonSerializer serializer)
        {
            writer.WriteValue(value.GetText());
        }
    }
}
