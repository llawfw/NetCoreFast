﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Common.Json
{
    public class TimeSpanJsonConverter : JsonConverter<TimeSpan>
    {
        public override TimeSpan Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (string.IsNullOrEmpty(reader.GetString()))
            {
                //? default(DateTime?) : DateTime.Parse(reader.GetString());]
                return default(TimeSpan);
            }
            //if (TimeSpan.TryParse(reader.GetString(), out TimeSpan date))
            //    return date;

            TimeSpan.TryParse(reader.GetString(), out TimeSpan date);
            return date;
        }

        public override void Write(Utf8JsonWriter writer, TimeSpan value, JsonSerializerOptions options)
        {
            // writer.WriteStringValue(value.ToString("HH:mm:ss"));
            writer.WriteStringValue(value.ToString());
        }
    }
}
