﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public class Userinfo
    {
        [SwaggerSchema(ReadOnly = true)]
        public int Id{ get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 登录角色类型：teacher教师、student学生、user用户
        /// </summary>
        public string Type { get; set; } = "user";
        /// <summary>
        /// 客户端唯一id值
        /// </summary>
        public string Uuid { get; set; }
        /// <summary>
        /// 验证码（图形、短信）
        /// </summary>
        public string Code { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public String Token { get; set; }
        [SwaggerSchema(ReadOnly = true)]
        public String Avatar { get; set; }
        [SwaggerSchema(ReadOnly = true)]
        public string Realname { get; set; }//真实姓名
        [SwaggerSchema(ReadOnly = true)]
        public String Role { get; set; }
        [SwaggerSchema(ReadOnly = true)]
        public String Tel { get; set; }//电话
        [SwaggerSchema(ReadOnly = true)]
        public String Email { get; set; }//电子邮箱
        [SwaggerSchema(ReadOnly = true)]
        public DateTime Birthday { get; set; }//出生日期

    }
}
