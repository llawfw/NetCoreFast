﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Plugin
{
    public static class PluginExtension
    {
        public static WebApplication UsePlugins(this WebApplication webApplication)
        {
            var plugins = PluginHelper.GetPlugins(webApplication.Environment.ContentRootPath);
            foreach (var plugin in plugins)
            {
                // 映射配置文件
                if (File.Exists(Path.Join(plugin.Info().Path, "config.json")))
                {
                    var configurationRoot = (new ConfigurationBuilder()).SetBasePath(plugin.Info().Path).AddJsonFile("config.json").Build();
                    plugin.Info().Configuration = configurationRoot;
                }
                plugin?.Init(webApplication);
                Console.WriteLine($"加载插件{plugin?.Info().AssemblyName} {plugin?.Info().Name} 成功,版本号:{plugin?.Info().Version} ");
            }
            return webApplication;
        }
    }
}
