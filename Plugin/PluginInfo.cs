﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Plugin
{
    public class PluginInfo
    {
        /// <summary>  
        /// 名称  
        /// </summary>       
        public string? Name { get; set; }

        /// <summary>  
        /// 显示名  
        /// </summary>  
        public string? DisplayName { get; set; }

        /// <summary>  
        /// 描述  
        /// </summary> 
        public string? Description { get; set; }
        /// <summary>  
        /// 插件地址信息  
        /// </summary>  
        public string? Path { get;internal set; }

        /// <summary>  
        /// 程序集名称  
        /// </summary>  
        public string? AssemblyName { get; internal set; }

        /// <summary>  
        /// 程序集版本  
        /// </summary>  
        public string? Version { get; internal set; }

        /// <summary>  
        /// 默认程序集  
        /// </summary>  
        public Assembly? Assembly { get; internal set; }
        /// <summary>
        /// 插件配置信息（自动读取）
        /// </summary>
        public IConfiguration? Configuration { get; internal set; }
    }
}
