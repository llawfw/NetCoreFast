﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq;
using Common;
using Microsoft.EntityFrameworkCore;
using Model;
using Model.Entity;
using Common;
using System.Threading;
using static System.Net.WebRequestMethods;

namespace DAL
{
    /// <summary>
    /// 数据访问类
    /// </summary>
    /// <typeparam name="T">源实体对象</typeparam>
    public interface IBaseDAL<T> where T : ID, new()
    {
        /// <summary>
        /// 获取数据上下文对象
        /// </summary>
        /// <returns></returns>
        MyDbContext DbContext();
        /// <summary>
        /// 添加实体对象
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Add(T entity);
        Task<bool> AddAsync(T entity, CancellationToken token = default);
        /// <summary>
        /// 批量添加实体对象
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        bool Add(IEnumerable<T> entities);
        Task<bool> AddAsync(IEnumerable<T> entities, CancellationToken token = default);
        /// <summary>
        /// 删除指定实体对象
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Delete(T entity);
        Task<bool> DeleteAsync(T entity, CancellationToken token = default);
        /// <summary>
        /// 根据id删除实体uix
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(int id);
        Task<bool> DeleteAsync(int id, CancellationToken token = default);
        /// <summary>
        /// 根据拉姆达条件参数删除实体对象
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        bool Delete(Expression<Func<T, bool>> where);
        Task<bool> DeleteAsync(Expression<Func<T, bool>> where, CancellationToken token = default);
        /// <summary>
        /// 根据查询条件删除。参考DAL.Extension.QueryableExtensions#Conditions<T>(this IQueryable<T> query, Dictionary<string, string> where, string excludes = null)方法。
        /// </summary>
        /// <param name="where">
        /// 添加过滤条件 如key="parent.id__eq" value = 1 如果添加时不加操作符 默认是等于eq 即如key=parent
        /// 实际key是parent_eq
        /// key 如 name__like、name__eq
        /// value 如果是in查询 多个值之间","分隔
        /// 如果是排序 值的格式为：+|-字段名,+|-字段名</param>
        /// <returns></returns>
        //[Obsolete("此方法后续会删除,请使用Delete(QueryParam<T> where)替换")]
        bool Delete(Dictionary<string, string> where);
        Task<bool> DeleteAsync(Dictionary<string, string> where, CancellationToken token = default);
        bool Delete(QueryParam<T> where);
        Task<bool> DeleteAsync(QueryParam<T> where, CancellationToken token = default);
        /// <summary>
        /// 修改实体对象属性,只更新指定的字段
        /// </summary>
        /// <param name="entity">实体对象，即属性值变化后的实体对象</param>
        /// <param name="propertyNames">指定那几个字段是要修改的</param>
        /// <returns></returns>
        bool Update(T entity, params string[] propertyNames);
        /// <summary>
        /// 修改实体对象属性,只更新指定的字段
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <param name="propertyNames"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T entity, CancellationToken token = default, params string[] propertyNames);
        /// <summary>
        /// 修改实体对象。全部属性更新
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update(T entity);
        /// <summary>
        /// 修改实体对象。全部属性更新
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <param name="ignoreNull">是否忽略NULL字段。false不忽略，则全部属性更新。true忽略null字段</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T entity/*,bool ignoreNull=false*/, CancellationToken token = default);
        /// <summary>
        /// 根据传递的值对象修改实体对象
        /// </summary>
        /// <typeparam name="E">值对象类型</typeparam>
        /// <param name="dto">值对象</param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<bool> UpdateAsync<E>(E dto, CancellationToken token = default) where E : IID;
        /// <summary>
        /// 根据id查询单个实体对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T SelectOne(int id, bool? asNoTracking = false);
        Task<T> SelectOneAsync(int id, bool? asNoTracking = false, CancellationToken token = default);
        /// <summary>
        /// 根据拉姆达表达式查询单个实体对象
        /// </summary>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        T SelectOne(Expression<Func<T, bool>> whereLambda, bool? asNoTracking = false);
        Task<T> SelectOneAsync(Expression<Func<T, bool>> whereLambda, bool? asNoTracking = false, CancellationToken token = default);
        /// <summary>
        /// 根据查询字符串查询单个实体对象
        /// </summary>
        /// <param name="where"> 
        /// 添加过滤条件 如key="parent.id__eq" value = 1 如果添加时不加操作符 默认是等于eq 即如key=parent
        /// 实际key是parent_eq
        /// key 如 name__like、name__eq
        /// value 如果是in查询 多个值之间","分隔
        /// 如果是排序 值的格式为：+|-字段名,+|-字段名
        /// 
        /// 参考DAL.Extension.QueryableExtensions#Conditions<T>(this IQueryable<T> query, Dictionary<string, string> where, string excludes = null)方法。
        /// </param>
        /// <param name="excludes">排除那些字段，多个字段用,隔开</param>
        /// <returns></returns>
        T SelectOne(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false);
        T SelectOne(QueryParam<T> where, string excludes = null, bool? asNoTracking = false);
        Task<T> SelectOneAsync(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default);
        Task<T> SelectOneAsync(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default);
        /// <summary>
        /// 查询实体，并将结果转换为指定类R
        /// </summary>
        /// <typeparam name="R"></typeparam>
        /// <param name="where"></param>
        /// <param name="excludes"></param>
        /// <returns></returns>
        Task<R> SelectOneToAsync<R>(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default);
        Task<R> SelectOneToAsync<R>(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default);

        /// <summary>
        /// 条件查询，并附带分页（单表）
        /// </summary>
        /// <param name="where"></param>
        /// <param name="excludes">排除查询那些字段，多个字段用,隔开</param>
        /// <returns></returns>
        Pagination<T> Query(Dictionary<string, string> where, string excludes = null);
        Pagination<T> Query(QueryParam<T> where, string excludes = null);
        Task<Pagination<T>> QueryAsync(Dictionary<string, string> where, string excludes = null, CancellationToken token = default);
        Task<Pagination<T>> QueryAsync(QueryParam<T> where, string excludes = null, CancellationToken token = default);
        /// <summary>
        /// 条件查询，并将结果转换为指定类R
        /// </summary>
        /// <typeparam name="R">查询转换后的类型</typeparam>
        /// <param name="where"></param>
        /// <param name="excludes"></param>
        /// <returns></returns>
        Task<Pagination<R>> QueryToAsync<R>(Dictionary<string, string> where, string excludes = null, CancellationToken token = default);
        Task<Pagination<R>> QueryToAsync<R>(QueryParam<T> where, string excludes = null, CancellationToken token = default);
        /// <summary>
        /// 多表分页条件查询
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <param name="linq"></param>
        /// <param name="where"></param>
        /// <param name="excludes"></param>
        /// <returns></returns>
        Pagination<E> Query<E>(IQueryable<E> linq, Dictionary<string, string> where, string excludes = null);
        Pagination<E> Query<E>(IQueryable<E> linq, QueryParam<T> where, string excludes = null);
        Task<Pagination<E>> QueryAsync<E>(IQueryable<E> linq, Dictionary<string, string> where, string excludes = null, CancellationToken token = default);
        Task<Pagination<E>> QueryAsync<E>(IQueryable<E> linq, QueryParam<T> where, string excludes = null, CancellationToken token = default);
        /// <summary>
        /// 获取查询集合
        /// </summary>
        /// <returns></returns>
        DbSet<T> Query();
        IEnumerable<T> SelectAll(bool? asNoTracking = false);
        Task<IEnumerable<T>> SelectAllAsync(bool? asNoTracking = false, CancellationToken token = default);
        /// <summary>
        /// 根据拉姆达条件表达式查询所有实体集合
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        IEnumerable<T> SelectAll(Expression<Func<T, bool>> where, bool? asNoTracking = false);
        Task<IEnumerable<T>> SelectAllAsync(Expression<Func<T, bool>> where, bool? asNoTracking = false, CancellationToken token = default);
        /// <summary>
        /// 查询所有
        /// </summary>
        /// <param name="where">
        /// 添加过滤条件 如key="parent.id__eq" value = 1 如果添加时不加操作符 默认是等于eq 即如key=parent
        /// 实际key是parent_eq
        /// key 如 name__like、name__eq
        /// value 如果是in查询 多个值之间","分隔
        /// 如果是排序 值的格式为：+|-字段名,+|-字段名
        /// 
        /// 参考DAL.Extension.QueryableExtensions#Conditions<T>(this IQueryable<T> query, Dictionary<string, string> where, string excludes = null)方法。
        /// </param>
        /// <param name="excludes">排除查询那些字段，多个字段用,隔开</param>
        /// <returns></returns>
        IEnumerable<T> SelectAll(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false);
        IEnumerable<T> SelectAll(QueryParam<T> where, string excludes = null, bool? asNoTracking = false);
        Task<IEnumerable<T>> SelectAllAsync(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default);
        Task<IEnumerable<T>> SelectAllAsync(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="whereLambda">条件表达式</param>
        /// <param name="pageNo">页码</param>
        /// <param name="pageSize">每页显示数量</param>
        /// <returns></returns>
        Pagination<T> SelectAll(Expression<Func<T, bool>> whereLambda, int pageNo, int pageSize, bool? asNoTracking = false);
        Task<Pagination<T>> SelectAllAsync(Expression<Func<T, bool>> whereLambda, int pageNo, int pageSize, bool? asNoTracking = false, CancellationToken token = default);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <typeparam name="OrderKey"></typeparam>
        /// <param name="whereLambda">条件表达式</param>
        /// <param name="orderbyLambda">排序字段</param>
        /// <param name="asc">是否升序</param>
        /// <param name="pageNo">页码</param>
        /// <param name="pageSize">每页显示记录数</param>
        /// <returns></returns>
        Pagination<T> SelectAll<OrderKey>(Expression<Func<T, bool>> whereLambda, Func<T, OrderKey> orderbyLambda, bool asc, int pageNo, int pageSize, bool? asNoTracking = false);
        Task<Pagination<T>> SelectAllAsync<OrderKey>(Expression<Func<T, bool>> whereLambda, Func<T, OrderKey> orderbyLambda, bool asc, int pageNo, int pageSize, bool? asNoTracking = false, CancellationToken token = default);
    }
}
