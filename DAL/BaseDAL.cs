﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Common;
using DAL.Extension;
using Model;
using Model.Entity;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Hosting.Server;
using System.Threading;
using Microsoft.Data.SqlClient;
using Common.Util;

namespace DAL
{
    /// <summary>
    /// 数据访问类 
    /// <see cref="https://github.com/yangzhongke/Zack.EFCore.Batch"/>
    /// <see cref="https://github.com/borisdj/EFCore.BulkExtensions"/>
    /// <see cref="https://learn.microsoft.com/zh-cn/ef/core/performance/efficient-querying"/>高性能查询
    /// <see cref="https://dynamic-linq.net/basic-simple-query"/>
    /// <see cref="http://wjhsh.net/sugarwxx-p-14990771.html"/> MySqlBulkCopy报错
    /// <see cref="https://www.jianshu.com/p/784a1a23fb8d"/>缓存
    /// <see cref="https://blog.csdn.net/u012558074/article/details/124792183"/>
    /// </summary>
    /// <typeparam name="T">源实体对象</typeparam>
    public class BaseDAL<T> : IBaseDAL<T> where T : ID, new()
    {
        public MyDbContext db { get; set; }
        public BaseDAL()
        {
        }
        public BaseDAL(MyDbContext db)
        {
            this.db = db;
        }
        public MyDbContext DbContext()
        {
            return db;
        }
        private QueryParam<T> buildQueryParam(Dictionary<string, string> where)
        {
            return new QueryParam<T>().Parse(where);
        }
        public bool Add(T entity)
        {
            db.Set<T>().Add(entity);
            return db.SaveChanges() > 0;
        }
        public async Task<bool> AddAsync(T entity, CancellationToken token = default)
        {
            await db.Set<T>().AddAsync(entity);
            return await db.SaveChangesAsync(token) > 0;
        }
        public bool Add(IEnumerable<T> entities)
        {
            //db.BulkInsert<T>(entities);
            if (db.Database.IsSqlServer())
            {
                MSSQLBulkInsertExtensions.BulkInsert<T>(db, entities);
            }
            if (db.Database.IsMySql())
            {
                MySQLBulkInsertExtensions.BulkInsert<T>(db, entities);
            }
            else
            {
                db.Set<T>().AddRange(entities);
                return db.SaveChanges() > 0;
            }
            return true;
        }
        public async Task<bool> AddAsync(IEnumerable<T> entities, CancellationToken token = default)
        {
            //db.Set<T>().AddRange(entities);
            // return await db.SaveChangesAsync() > 0;
            //await db.BulkInsertAsync<T>(entities);
            if (db.Database.IsSqlServer())
            {
                await MSSQLBulkInsertExtensions.BulkInsertAsync<T>(db, entities, null, SqlBulkCopyOptions.Default, token);
            }
            if (db.Database.IsMySql())
            {
                await MySQLBulkInsertExtensions.BulkInsertAsync<T>(db, entities, null, token);
            }
            else
            {
                await db.Set<T>().AddRangeAsync(entities);
                return await db.SaveChangesAsync(token) > 0;
            }
            return true;
        }
        public bool Delete(T entity)
        {
            db.Set<T>().Remove(entity);
            return db.SaveChanges() > 0;
        }
        public async Task<bool> DeleteAsync(T entity, CancellationToken token = default)
        {
            db.Set<T>().Remove(entity);
            return await db.SaveChangesAsync(token) > 0;
        }
        public bool Delete(int id)
        {
            //var obj = db.Set<T>().SingleOrDefault(o => o.Id == id);
            //db.Set<T>().Remove(obj);
            //return db.SaveChanges() > 0;
            //return db.Set<T>().Where(o => o.Id == id).DeleteRange<T>(db) > 0;
            return db.DeleteRange<T>(o => o.Id == id) > 0;
        }
        public async Task<bool> DeleteAsync(int id, CancellationToken token = default)
        {
            //var obj = await db.Set<T>().SingleOrDefaultAsync(o => o.Id == id);
            //db.Set<T>().Remove(obj);
            //return await db.SaveChangesAsync() > 0;
            // return await db.Set<T>().Where(o => o.Id == id).DeleteRangeAsync<T>(db) > 0;
            return await db.DeleteRangeAsync<T>(o => o.Id == id, false, token) > 0;
        }
        public bool Delete(Expression<Func<T, bool>> where)
        {
            /*
            IQueryable<T> qlist = db.Set<T>().Where(where);
            if (qlist != null)
            {
                db.Set<T>().RemoveRange(qlist);
                return db.SaveChanges() > 0;
            }
            return false;*/
            //return db.Set<T>().Where(where).BatchDelete() > 0;
            return db.DeleteRange<T>(where) > 0;
        }
        public async Task<bool> DeleteAsync(Expression<Func<T, bool>> where, CancellationToken token = default)
        {
            /*
            IQueryable<T> qlist = db.Set<T>().Where(where);
            if (qlist != null)
            {
                db.Set<T>().RemoveRange(qlist);
                return await db.SaveChangesAsync() > 0;
            }
            return false;*/
            //return await db.Set<T>().Where(where).BatchDeleteAsync() > 0;
            return await db.DeleteRangeAsync<T>(where, false, token) > 0;
        }
        public bool Delete(Dictionary<string, string> where)
        {
            /*
            IQueryable<T> qlist = db.Set<T>().Where<T, T>(where);
            if (qlist != null)
            {
                db.Set<T>().RemoveRange(qlist);
                return db.SaveChanges() > 0;
            }
            return false;*/
            var q = buildQueryParam(where);
            return db.Set<T>().Where(q.WhereLINQ, q.Values).DeleteRange<T>(db) > 0;

        }
        public async Task<bool> DeleteAsync(Dictionary<string, string> where, CancellationToken token = default)
        {
            /*
            IQueryable<T> qlist = db.Set<T>().Where<T, T>(where);
            if (qlist != null)
            {
                db.Set<T>().RemoveRange(qlist);
                return await db.SaveChangesAsync() > 0;
            }
            return false;*/
            var q = buildQueryParam(where);
            return await db.Set<T>().Where(q.WhereLINQ, q.Values).DeleteRangeAsync<T>(db, null, false, token) > 0;//.Where(q.WhereLINQ, q.Values);//这个方法第一次运行耗时很严重
        }
        public bool Delete(QueryParam<T> where)
        {
            return db.Set<T>().Where(where.WhereLINQ, where.Values).DeleteRange<T>(db) > 0;
        }
        public async Task<bool> DeleteAsync(QueryParam<T> where, CancellationToken token = default)
        {/*        
            System.Diagnostics.Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start(); //  开始监视代码运行时间
            var s = where.WhereLINQ; var d = where.Values;           
            var qq = db.Set<T>();
            var qq2 = qq.Where(s, d);//这个方法第一次运行耗时很严重,会进行预编译，后面会快
            stopwatch.Stop(); //  停止监视 
            TimeSpan timespan = stopwatch.Elapsed; //  获取当前实例测量得出的总时间         
            double milliseconds = timespan.TotalMilliseconds;  //  总毫秒数
            Console.WriteLine("测试运行时间毫秒24：" + milliseconds);
            return await qq2.BatchDeleteAsync()>0 ;*/
            return await db.Set<T>().Where(where.WhereLINQ, where.Values).DeleteRangeAsync<T>(db, null, false, token) > 0;
        }
        public bool Update(T entity, params string[] propertyNames)
        {
            EntityEntry entry = db.Entry(entity);
            entry.State = EntityState.Unchanged;
            foreach (var item in propertyNames)
            {
                entry.Property(item).IsModified = true;
            }
            return db.SaveChanges() > 0;
        }
        public async Task<bool> UpdateAsync(T entity, CancellationToken token = default, params string[] propertyNames)
        {
            EntityEntry entry = db.Entry(entity);
            entry.State = EntityState.Unchanged;
            foreach (var item in propertyNames)
            {
                entry.Property(item).IsModified = true;
            }
            return await db.SaveChangesAsync(token) > 0;
        }
        public bool Update(T o)
        {
            //db.Entry(o).State = EntityState.Deleted;//先删除掉原来的，为什么？  这样写会在一个操作方法  同时调用Add和Update方法时，导致前面调用的 db.SaveChanges()方法无效，即使加了事务。
            //将上面 的改成如下 可解决 同时调用两个savechange问题。初步测试可行
            //if (db.Entry(o).State != EntityState.Unchanged)
            //{
            //   // db.Entry(o).State = EntityState.Deleted;//先删除掉原来的
            //}
            db.Entry(o).State = EntityState.Modified;
            //db.Set<TValue>().Update(o);
            return db.SaveChanges() > 0;
        }
        public async Task<bool> UpdateAsync(T o/*, bool ignoreNull = false, */, CancellationToken token = default)
        {
            //db.Entry(o).State = EntityState.Deleted;//先删除掉原来的，为什么？  这样写会在一个操作方法  同时调用Add和Update方法时，导致前面调用的 db.SaveChanges()方法无效，即使加了事务。
            //将上面 的改成如下 可解决 同时调用两个savechange问题。初步测试可行
            //if (db.Entry(o).State != EntityState.Unchanged)
            //{
            //   // db.Entry(o).State = EntityState.Deleted;//先删除掉原来的
            //}           
            db.Entry(o).State = EntityState.Modified;
            //db.Set<T>().Update(o);
            return await db.SaveChangesAsync(token) > 0;
        }
        public async Task<bool> UpdateAsync<E>(E dto, CancellationToken token = default) where E : IID
        {
            var batchUpdateBuilder = db.BatchUpdate<T>().Where(o => o.Id == dto.Id);
            //PropertyValue<E> propertyValue = new PropertyValue<E>(dto);
            Type sourceType = typeof(E); 
            foreach (var p in sourceType.GetProperties().Where(x => x.CanRead && x.CanWrite))//遍历值对象属性
            {//性能待改进
                //Console.WriteLine(p.Name);
                //var value = propertyValue.Get(p);//获取属性值
                var value = p.GetValue(dto);//获取属性值
                //Console.WriteLine(p.Name+"-"+value); 
                if (value != null) batchUpdateBuilder = batchUpdateBuilder.Set(p.Name, value);
            }
            return await batchUpdateBuilder.ExecuteAsync() > 0;
        }
        public T SelectOne(int id, bool? asNoTracking = false)
        {
            return asNoTracking == true ? db.Set<T>().AsNoTracking().FirstOrDefault(o => o.Id == id) : db.Set<T>().FirstOrDefault(o => o.Id == id);
        }
        public async Task<T> SelectOneAsync(int id, bool? asNoTracking = false, CancellationToken token = default)
        {
            return asNoTracking == true ? await db.Set<T>().AsNoTracking().FirstOrDefaultAsync(o => o.Id == id, token) : await db.Set<T>().FirstOrDefaultAsync(o => o.Id == id, token);
        }
        public T SelectOne(Expression<Func<T, bool>> where, bool? asNoTracking = false)
        {
            return asNoTracking == true ? db.Set<T>().AsNoTracking().Where(where)?.FirstOrDefault() : db.Set<T>().Where(where)?.FirstOrDefault();
        }
        public async Task<T> SelectOneAsync(Expression<Func<T, bool>> where, bool? asNoTracking = false, CancellationToken token = default)
        {
            return asNoTracking == true ? await db.Set<T>().AsNoTracking().Where(where)?.FirstOrDefaultAsync(token) : await db.Set<T>().Where(where)?.FirstOrDefaultAsync(token);
        }
        public T SelectOne(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false)
        {
            // return db.Set<T>().Where<T, T>(where, excludes)?.FirstOrDefault();
            var q = buildQueryParam(where);
            return asNoTracking == true ? db.Set<T>().AsNoTracking().Where(q.WhereLINQ, q.Values).Select<T, T>(excludes)?.FirstOrDefault() : db.Set<T>().Where(q.WhereLINQ, q.Values).Select<T, T>(excludes)?.FirstOrDefault();
        }
        public T SelectOne(QueryParam<T> where, string excludes = null, bool? asNoTracking = false)
        {
            return asNoTracking == true ? db.Set<T>().AsNoTracking().Where(where.WhereLINQ, where.Values).Select<T, T>(excludes)?.FirstOrDefault() : db.Set<T>().Where(where.WhereLINQ, where.Values).Select<T, T>(excludes)?.FirstOrDefault();
        }
        public async Task<T> SelectOneAsync(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            //return await db.Set<T>().Where<T, T>(where, excludes)?.FirstOrDefaultAsync();
            var q = buildQueryParam(where);
            return asNoTracking == true ? await db.Set<T>().AsNoTracking().Where(q.WhereLINQ, q.Values).Select<T, T>(excludes)?.FirstOrDefaultAsync(token) : await db.Set<T>().Where(q.WhereLINQ, q.Values).Select<T, T>(excludes)?.FirstOrDefaultAsync(token);
        }
        public async Task<T> SelectOneAsync(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            return asNoTracking == true ? await db.Set<T>().AsNoTracking().Where(where.WhereLINQ, where.Values).Select<T, T>(excludes)?.FirstOrDefaultAsync(token) : await db.Set<T>().Where(where.WhereLINQ, where.Values).Select<T, T>(excludes)?.FirstOrDefaultAsync(token);
        }
        public async Task<R> SelectOneToAsync<R>(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            //return await db.Set<T>().Where<T, R>(where, excludes)?.FirstOrDefaultAsync();
            var q = buildQueryParam(where);
            return asNoTracking == true ? await db.Set<T>().AsNoTracking().Where(q.WhereLINQ, q.Values).Select<T, R>(excludes)?.FirstOrDefaultAsync(token) : await db.Set<T>().Where(q.WhereLINQ, q.Values).Select<T, R>(excludes)?.FirstOrDefaultAsync(token);
        }
        public async Task<R> SelectOneToAsync<R>(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            return asNoTracking == true ? await db.Set<T>().AsNoTracking().Where(where.WhereLINQ, where.Values).Select<T, R>(excludes)?.FirstOrDefaultAsync(token) : await db.Set<T>().Where(where.WhereLINQ, where.Values).Select<T, R>(excludes)?.FirstOrDefaultAsync(token);
        }
        public Pagination<T> Query(Dictionary<string, string> where, string excludes = null)
        {
            //return db.Set<T>().Conditions<T, T>(where, excludes);
            var q = buildQueryParam(where);
            return Query(q, excludes);
        }
        public Pagination<T> Query(QueryParam<T> where, string excludes = null)
        {
            //var result = db.Set<T>().AsNoTracking().Where(where.WhereLINQ, where.Values).OrderBy(where.SortLINQ).Select<T, T>(excludes).PageResult(where.PageNo, where.PageSize);
            //return Pagination<T>.Init(result.PageCount, result.RowCount, result.Queryable.ToList());
            return QueryAsync(where, excludes).Result;
        }
        public async Task<Pagination<T>> QueryAsync(Dictionary<string, string> where, string excludes = null, CancellationToken token = default)
        {
            return await QueryToAsync<T>(where, excludes, token);
        }
        public async Task<Pagination<T>> QueryAsync(QueryParam<T> where, string excludes = null, CancellationToken token = default)
        {
            return await QueryToAsync<T>(where, excludes, token);
        }
        public async Task<Pagination<R>> QueryToAsync<R>(Dictionary<string, string> where, string excludes = null, CancellationToken token = default)
        {
            // return await db.Set<T>().ConditionsAsync<T, R>(where, excludes);
            var q = buildQueryParam(where);
            return await QueryToAsync<R>(q, excludes, token);
        }
        public async Task<Pagination<R>> QueryToAsync<R>(QueryParam<T> where, string excludes = null, CancellationToken token = default)
        {
            var q = db.Set<T>().AsNoTracking().Select<T, R>(excludes);//延迟加载与无跟踪在一起使用的时候就不能加载导航属性了。
            if (!string.IsNullOrEmpty(where.WhereLINQ)) q = q.Where(where.WhereLINQ, where.Values);
            if (!string.IsNullOrEmpty(where.SortLINQ)) q = q.OrderBy(where.SortLINQ);
            var result = q.PageResult(where.PageNo, where.PageSize);
            
            // Console.WriteLine("sql：" + result.Queryable.ToQueryString());
            return Pagination<R>.Init(result.PageCount, result.RowCount, await result.Queryable.ToListAsync(token));
        }
        //LINQ多表分页查询
        public Pagination<E> Query<E>(IQueryable<E> linq, Dictionary<string, string> where, string excludes = null)
        {
            //if (where != null) where.Remove("sort");//linq加上sort排序会报错的bug 后续需要解决
            //return linq.Conditions<E, E>(where, excludes);
            var q = buildQueryParam(where);
            //db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;  //全局取消
            var result = linq.Where(q.WhereLINQ, q.Values).Select<E, E>(excludes).PageResult(q.PageNo, q.PageSize);
            return Pagination<E>.Init(result.PageCount, result.RowCount, result.Queryable.ToList());
        }
        public Pagination<E> Query<E>(IQueryable<E> linq, QueryParam<T> where, string excludes = null)
        {
            // if (where != null) where.Remove("sort");//linq加上sort排序会报错的bug 后续需要解决           
            var result = linq.Where(where.WhereLINQ, where.Values).Select<E, E>(excludes).PageResult(where.PageNo, where.PageSize);
            return Pagination<E>.Init(result.PageCount, result.RowCount, result.Queryable.ToList());
        }
        public async Task<Pagination<E>> QueryAsync<E>(IQueryable<E> linq, Dictionary<string, string> where, string excludes = null, CancellationToken token = default)
        {
            //if (where != null) where.Remove("sort");//linq加上sort排序会报错的bug 后续需要解决    
            //return await linq.ConditionsAsync<E, E>(where, excludes);
            var q = buildQueryParam(where);
            var result = linq.Where(q.WhereLINQ, q.Values).OrderBy(q.SortLINQ).Select<E, E>(excludes).PageResult(q.PageNo, q.PageSize);
            return Pagination<E>.Init(result.PageCount, result.RowCount, await result.Queryable.ToListAsync(token));
        }
        public async Task<Pagination<E>> QueryAsync<E>(IQueryable<E> linq, QueryParam<T> where, string excludes = null, CancellationToken token = default)
        {
            //if (where != null) where.Remove("sort");//linq加上sort排序会报错的bug 后续需要解决    
            var result = linq.Where(where.WhereLINQ, where.Values).OrderBy(where.SortLINQ).Select<E, E>(excludes).PageResult(where.PageNo, where.PageSize);
            return Pagination<E>.Init(result.PageCount, result.RowCount, await result.Queryable.ToListAsync(token));
        }
        public DbSet<T> Query()
        {
            return db.Set<T>();
        }
        public IEnumerable<T> SelectAll(bool? asNoTracking = false)
        {
            return asNoTracking == true ? db.Set<T>().AsNoTracking().ToList() : db.Set<T>().ToList();
        }
        public async Task<IEnumerable<T>> SelectAllAsync(bool? asNoTracking = false, CancellationToken token = default)
        {
            return asNoTracking == true ? await db.Set<T>().AsNoTracking().ToListAsync(token) : await db.Set<T>().ToListAsync(token);
        }
        public IEnumerable<T> SelectAll(Expression<Func<T, bool>> where, bool? asNoTracking = false)
        {
            return asNoTracking == true ? db.Set<T>().AsNoTracking().Where(where).ToList() : db.Set<T>().Where(where).ToList();
        }
        public async Task<IEnumerable<T>> SelectAllAsync(Expression<Func<T, bool>> where, bool? asNoTracking = false, CancellationToken token = default)
        {
            return asNoTracking == true ? await db.Set<T>().AsNoTracking().Where(where).ToListAsync(token) : await db.Set<T>().Where(where).ToListAsync(token);
        }
        public IEnumerable<T> SelectAll(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false)
        {
            //return db.Set<T>().Where<T, T>(where, excludes).ToList();
            var q = buildQueryParam(where);
            return SelectAll(q, excludes, asNoTracking);
        }
        public IEnumerable<T> SelectAll(QueryParam<T> where, string excludes = null, bool? asNoTracking = false)
        {
            return SelectAllAsync(where, excludes, asNoTracking).Result;
        }
        public async Task<IEnumerable<T>> SelectAllAsync(Dictionary<string, string> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            //return await db.Set<T>().Where<T, T>(where, excludes).ToListAsync();
            var q = buildQueryParam(where);
            return await SelectAllAsync(q, excludes, asNoTracking, token);
        }
        public async Task<IEnumerable<T>> SelectAllAsync(QueryParam<T> where, string excludes = null, bool? asNoTracking = false, CancellationToken token = default)
        {
            var q = db.Set<T>().Select<T, T>(excludes);
            if (asNoTracking == true) q = q.AsNoTracking();
            if (!string.IsNullOrEmpty(where.WhereLINQ)) q = q.Where(where.WhereLINQ, where.Values);
            if (!string.IsNullOrEmpty(where.SortLINQ)) q = q.OrderBy(where.SortLINQ);
            return await q.ToListAsync(token);
        }
        public Pagination<T> SelectAll(Expression<Func<T, bool>> where, int pageNo, int pageSize, bool? asNoTracking = false)
        {
            return SelectAll<object>(where, o => o.Id, true, pageNo, pageSize, asNoTracking);
        }
        public async Task<Pagination<T>> SelectAllAsync(Expression<Func<T, bool>> where, int pageNo, int pageSize, bool? asNoTracking = false, CancellationToken token = default)
        {
            return await SelectAllAsync<object>(where, o => o.Id, true, pageNo, pageSize, asNoTracking);
        }
        public Pagination<T> SelectAll<OrderKey>(Expression<Func<T, bool>> whereLambda, Func<T, OrderKey> orderbyLambda, bool asc, int pageNo, int pageSize, bool? asNoTracking = false)
        {
            var objs = db.Set<T>();
            int total = objs.Count();
            int pageCount = (int)Math.Ceiling(total * 1.0 / pageSize);
            if (asc)
            {
                var q = asNoTracking == true ? objs.AsNoTracking().Where(whereLambda).OrderBy(orderbyLambda).Skip((pageNo - 1) * pageSize).Take(pageSize) : objs.Where(whereLambda).OrderBy(orderbyLambda).Skip((pageNo - 1) * pageSize).Take(pageSize);
                return Pagination<T>.Init(pageCount, total, q);
            }
            else
            {
                var q = asNoTracking == true ? objs.AsNoTracking().Where(whereLambda).OrderByDescending(orderbyLambda).Skip((pageNo - 1) * pageSize).Take(pageSize) : objs.Where(whereLambda).OrderByDescending(orderbyLambda).Skip((pageNo - 1) * pageSize).Take(pageSize);
                return Pagination<T>.Init(pageCount, total, q);
            }
        }
        public async Task<Pagination<T>> SelectAllAsync<OrderKey>(Expression<Func<T, bool>> whereLambda, Func<T, OrderKey> orderbyLambda, bool asc, int pageNo, int pageSize, bool? asNoTracking = false, CancellationToken token = default)
        {
            var objs = db.Set<T>();
            int total = objs.Count();
            int pageCount = (int)Math.Ceiling(total * 1.0 / pageSize);
            if (asc)
            {
                var q = asNoTracking == true ? objs.AsNoTracking().Where(whereLambda).OrderBy(orderbyLambda).Skip((pageNo - 1) * pageSize).Take(pageSize) : objs.Where(whereLambda).OrderBy(orderbyLambda).Skip((pageNo - 1) * pageSize).Take(pageSize);
                return Pagination<T>.Init(pageCount, total, q);
            }
            else
            {
                var q = asNoTracking == true ? objs.AsNoTracking().Where(whereLambda).OrderByDescending(orderbyLambda).Skip((pageNo - 1) * pageSize).Take(pageSize) : objs.Where(whereLambda).OrderByDescending(orderbyLambda).Skip((pageNo - 1) * pageSize).Take(pageSize);
                return Pagination<T>.Init(pageCount, total, q);
            }
        }
    }
}
