using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.Generic;
using static System.Linq.Expressions.Expression;
using System.Diagnostics;
using System.Security.AccessControl;
using System.Collections.Concurrent;
using Newtonsoft.Json.Linq;
using Common.MyAttribute;
using Model.Entity;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;
using Microsoft.VisualBasic.FileIO;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Common.Util;

namespace DAL.Extension
{
    /// <summary>
    /// EFCore扩展Select方法(根据实体定制查询语句) 
    /// <see cref="https://www.cnblogs.com/castyuan/p/10186619.html"/>
    /// <see cref="https://blog.csdn.net/liyou123456789/article/details/119967779"/>
    /// </summary>
    public static class QueryableExtentions
    {
        // <summary>
        /// 泛型缓存。每个不同的T，都会生成一份不同的副本
        /// 适合不同类型，需要缓存一份数据的场景，效率高
        /// 不能主动释放
        /// <see cref="https://www.cjavapy.com/amp/502/"/>
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        public class GenericCache<TSource, TResult> where TSource : ID, new()
        {
            private static Expression<Func<TSource, TResult>> cache = null;
            static GenericCache()
            {
                cache = GetLamda<TSource, TResult>(null, null);
                //Console.WriteLine("This is GenericCache 静态构造函数");
                //_TypeTime = string.Format("{0}_{1}", typeof(T).FullName, DateTime.Now.ToString("yyyyMMddHHmmss.fff"));
            }
            /// <summary>
            /// 获取缓存
            /// </summary>
            /// <returns></returns>
            public static Expression<Func<TSource, TResult>> GetCache()
            {
                return cache;
            }
        }
        /// <summary>
        /// 字典缓存：静态属性常驻内存
        /// </summary>
        public class DictionaryCache
        {
            private static readonly ConcurrentDictionary<string, object> cache = null;
            private static readonly ConcurrentDictionary<string, List<string>> includecache = null;
            static DictionaryCache()
            {
                //Console.WriteLine("This is DictionaryCache 静态构造函数");
                cache = new ConcurrentDictionary<string, object>();
                includecache = new ConcurrentDictionary<string, List<string>>();
            }
            public static Expression<Func<TSource, TResult>> GetCache<TSource, TResult>(string excludes = null)
            {
                string key = $"funckey_{typeof(TSource).FullName}_{typeof(TResult).FullName}_{excludes}".Replace(",", "_");
                if (!cache.ContainsKey(key))
                {
                    var exp = GetLamda<TSource, TResult>(null, excludes);
                    cache[key] = exp;
                }
                return (Expression<Func<TSource, TResult>>)cache[key];
            }
            public static List<string> GetIncludeCache(Type t)
            {
                string key = $"key_{t.FullName}";
                if (!includecache.ContainsKey(key))
                {
                    includecache[key] = GetIncludeProps(t);
                }
                return includecache[key];
            }
        }
        /// <summary>
        /// 构建查询。构建的表达式不会缓存
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <param name="excludes">排除那些属性不用查询。多个属性排除字段用","隔开</param>
        /// <returns></returns>
        public static IQueryable<TResult> Select<TResult>(this IQueryable<object> query, string? excludes = null)
        {
            return Queryable.Select(query, GetLamda<object, TResult>(query.GetType().GetGenericArguments()[0], excludes));
        }
        /// <summary>
        /// 构建查询投影（查询字段）TSource。若excludes参数存在则表达式字典缓存，excludes不存在则表达式会泛型缓存以提高性能
        /// </summary>
        /// <typeparam name="TSource"> 查询源实体类型</typeparam>
        /// <typeparam name="TResult"> 查询结果类型，属性名与查询源实体类型一致，数量不必一致</typeparam>
        /// <param name="query"></param>
        /// <param name="excludes">排除那些属性不用查询。多个属性排除字段用","隔开</param>
        /// <returns></returns>
        public static IQueryable<TResult> Select<TSource, TResult>(this IQueryable<TSource> query, string? excludes = null) where TSource : ID, new()
        {
            //query = query.BuildInclude<TSource, TResult>().AsNoTracking();//构建
            //var exp= GetLamda<TSource, TResult>(null, excludes);
            var exp = excludes == null ? GenericCache<TSource, TResult>.GetCache() : DictionaryCache.GetCache<TSource, TResult>(excludes);
            return Queryable.Select(query, exp);
        }
        /// <summary>
        /// https://www.cnblogs.com/mryux/p/15863282.html
        /// https://www.shuzhiduo.com/A/pRdBNkR9dn/
        /// 有待纠正
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <param name="propertyNames"></param>
        /// <returns></returns>
        public static IQueryable<TResult> Select<TSource, TResult>(this IQueryable<TSource> query, params string[] propertyNames)
        {/*
            var source = typeof(User);
            var target = typeof(User);

            var t = Expression.Parameter(source, "t");

            //var sourceName = Expression.MakeMemberAccess(t, source.GetProperty("Name"));
            var sourceId = Expression.MakeMemberAccess(t, source.GetProperty("Id"));

            // var assignName = Expression.Bind(target.GetProperty("Name"), sourceName);
            var assignValue = Expression.Bind(target.GetProperty("Id"), sourceId);
            var targetNew = Expression.New(target);
            var init = Expression.MemberInit(targetNew, assignValue);

            var lambda =
              (Expression<Func<User, User>>)Expression.Lambda(init, t);*/
            var p = Parameter(typeof(TSource));
            List<Expression> propExprList = new List<Expression>();
            foreach (string propertyName in propertyNames)
            {
                Expression propExpr = Convert(MakeMemberAccess(p, typeof(TSource).GetProperty(propertyName)), typeof(TResult));
                propExprList.Add(propExpr);
            }

            var newArrayExpr = NewArrayInit(typeof(TResult), propExprList.ToArray());
            var selectExpr = Lambda<Func<TSource, TResult>>(newArrayExpr, p);
            // return query.Select(selectExpr);
            return Queryable.Select(query, selectExpr);
            //return Lambda<Func<TSource, TResult>>(GetExpression(propertyParameter, sourceType, resultType, excludes), parameter);
        }
        /// <summary>
        /// 构建查询拉姆达表达式
        /// </summary>
        /// <typeparam name="TSource">查询源实体类型</typeparam>
        /// <typeparam name="TResult">查询结果类型，属性名与查询源实体类型一致，数量不必一致</typeparam>
        /// <param name="type">IQueryable<T>泛型具体类型</param>
        /// <param name="excludes"></param>
        /// <returns></returns>
        public static Expression<Func<TSource, TResult>> GetLamda<TSource, TResult>(Type _sourceType = null, string excludes = null/*,out Expression<Func<TSource>> navigationPropertyPath*/)
        {
            //.Select(c => new { c.City, c.CompanyName } )
            Type sourceType = typeof(TSource);//查询源实体类型
            Type resultType = typeof(TResult);//查询结果类型
            ParameterExpression parameter = Parameter(sourceType);// Parameter(sourceType, "c");//创建参数 c 
            Expression propertyParameter;
            if (_sourceType != null)
            {
                sourceType = _sourceType;
                propertyParameter = Convert(parameter, _sourceType);  //创建一个表示类型转换运算的 UnaryExpression。          
            }
            else
                propertyParameter = parameter;
            return Lambda<Func<TSource, TResult>>(GetExpression(propertyParameter, sourceType, resultType, excludes), parameter);
        }
        /// <summary>
        /// 构架表达式
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="sourceType"></param>
        /// <param name="resultType"></param>
        /// <param name="excludes"></param>
        /// <returns></returns>
        public static MemberInitExpression GetExpression(Expression parameter, Type sourceType, Type resultType, string excludes = null)
        {
            var memberBindings = new List<MemberBinding>();
            //循环要查询的字段
            foreach (var resultItem in resultType.GetProperties().Where(x => x.CanWrite))
            {
                //先排除哪些字段
                if (excludes != null && excludes.ToLower().Contains(resultItem.Name.ToLower())) continue;
                //获取关联字段,根据获取字段上面的注解
                var fromEntityAttr = resultItem.GetCustomAttribute<FromModelAttribute>();
                if (fromEntityAttr != null)
                {
                    var property = GetFromEntityExpression(parameter, sourceType, fromEntityAttr);
                    if (property != null)
                        memberBindings.Add(Bind(resultItem, property));
                    continue;
                }
                //源字段
                var sourceItem = sourceType.GetProperty(resultItem.Name);//从源实体类型去查询对应结果字段
                if (sourceItem == null)//当没有对应的属性时，查找 实体名+属性
                {
                    var complexSourceItemProperty = GetCombinationExpression(parameter, sourceType, resultItem);
                    if (complexSourceItemProperty != null)
                        memberBindings.Add(Bind(resultItem, complexSourceItemProperty));
                    continue;
                }
                //判断实体的读写权限
                if (sourceItem == null || !sourceItem.CanRead)
                    continue;

                //标注NotMapped特性的属性忽略转换
                if (sourceItem.GetCustomAttribute<NotMappedAttribute>() != null)
                    continue;
                //构建源属性 c.City
                var sourceProperty = Property(parameter, sourceItem);

                //当非值类型且源字段类型和目标只读类型不相等 且目标字段类不等于不等于结果类型  递归字段 如user.Student.Teacher
                if (!sourceItem.PropertyType.IsValueType && sourceItem.PropertyType != resultItem.PropertyType && resultItem.PropertyType != resultType)
                {
                    //判断都是(非泛型、非数组)class
                    if (sourceItem.PropertyType.IsClass && resultItem.PropertyType.IsClass
                        && !sourceItem.PropertyType.IsArray && !resultItem.PropertyType.IsArray
                        && !sourceItem.PropertyType.IsGenericType && !resultItem.PropertyType.IsGenericType)
                    {
                        var expression = GetExpression(sourceProperty, sourceItem.PropertyType, resultItem.PropertyType);
                        memberBindings.Add(Bind(resultItem, expression));
                    }
                    continue;
                }
                //源类型和目标类型不相等则不查询
                if (resultItem.PropertyType != sourceItem.PropertyType)
                    continue;
                //添加到属性集合，绑定结果字段和源字段
                memberBindings.Add(Bind(resultItem, sourceProperty));
            }
            return MemberInit(New(resultType), memberBindings);//构建new Student{Name=c.Name}
        }
        public static IQueryable<TSource> BuildInclude<TSource, TResult>(this IQueryable<TSource> query) where TSource : ID, new()
        {
            Type resultType = typeof(TResult);//查询结果类型
            var includes = DictionaryCache.GetIncludeCache(resultType);
            if (includes != null)
            {
                foreach(var i in includes)
                {
                    query= query.Include(i);
                }
            }
            return query;
        }

        private static List<string> GetIncludeProps(Type resultType)
        {
            List<string> list = new List<string>();
            //循环要查询的字段
            foreach (var resultItem in resultType.GetProperties().Where(x => x.CanWrite))
            {
                //获取关联字段,根据获取字段上面的注解
                var fromEntityAttr = resultItem.GetCustomAttribute<FromModelAttribute>();
                if (fromEntityAttr != null)
                {
                    var tableNames = fromEntityAttr.ModelNames;
                    if (tableNames != null)
                    {
                        list.AddRange(tableNames);
                    }
                }
                //集合加载
                if (resultItem.PropertyType.HasImplementedRawGeneric(typeof(ICollection<>)))
                {
                    list.Add(resultItem.Name);
                }
            }
            return list;
        }

        /// <summary>
        /// 根据FromModelAttribute 的值获取属性对应的路径
        /// </summary>
        /// <param name="sourceProperty"></param>
        /// <param name="sourceType"></param>
        /// <param name="FromModelAttribute"></param>
        /// <returns></returns>
        private static Expression GetFromEntityExpression(Expression sourceProperty, Type sourceType, FromModelAttribute FromModelAttribute)
        {
            var findType = sourceType;
            var resultProperty = sourceProperty;
            var tableNames = FromModelAttribute.ModelNames;
            if (tableNames == null)
            {
                var columnProperty = findType.GetProperty(FromModelAttribute.ModelColumn);
                if (columnProperty == null)
                    return null;
                else
                    return Property(resultProperty, columnProperty);
            }

            for (int i = 0; i < tableNames.Length; i++)
            {
                var tableProperty = findType.GetProperty(tableNames[i]);
                if (tableProperty == null)
                    return null;

                findType = tableProperty.PropertyType;
                resultProperty = Property(resultProperty, tableProperty);
            }

            var property = findType.GetProperty(FromModelAttribute.ModelColumn);
            if (property == null)
                return null;
            else
                return Property(resultProperty, property);
        }

        /// <summary>
        /// 根据组合字段获取其属性路径
        /// </summary>
        /// <param name="sourceProperty"></param>
        /// <param name="sourcePropertys"></param>
        /// <param name="resultItem"></param>
        /// <returns></returns>
        private static Expression GetCombinationExpression(Expression sourceProperty, Type sourceType, PropertyInfo resultItem)
        {
            foreach (var item in sourceType.GetProperties().Where(x => x.CanRead))
            {
                if (resultItem.Name.StartsWith(item.Name))
                {
                    if (item != null && item.CanRead && item.PropertyType.IsClass && !item.PropertyType.IsGenericType)
                    {
                        var rightName = resultItem.Name.Substring(item.Name.Length);

                        var complexSourceItem = item.PropertyType.GetProperty(rightName);
                        if (complexSourceItem != null && complexSourceItem.CanRead)
                            return Property(Property(sourceProperty, item), complexSourceItem);
                    }
                }
            }

            return null;
        }
    }
}
