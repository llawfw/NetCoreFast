﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Diagnostics;
using System.IO;
namespace CodeGen
{
    /// <summary>
    /// https://blog.csdn.net/weixin_53370274/article/details/123100708
    /// https://www.jb51.net/article/209233.htm
    /// https://blog.csdn.net/hjssss/article/details/119651581
    /// https://blog.csdn.net/WPwalter/article/details/90344417?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_baidulandingword~default-0-90344417-blog-119651581.pc_relevant_default&spm=1001.2101.3001.4242.1&utm_relevant_index=3
    /// </summary>
    public class CommandRunner
    {
        public string ExecutablePath { get; }
        public string WorkingDirectory { get; }
        private Process p;
        public CommandRunner(string executablePath, string? workingDirectory = null)
        {
            ExecutablePath = executablePath ?? throw new ArgumentNullException(nameof(executablePath));
            WorkingDirectory = workingDirectory ?? Path.GetDirectoryName(executablePath);                    
        }

        public void Run(string cmdAndarguments, EventHandler callback = null)
        {
            try
            {
                //Console.WriteLine($"开始执行命令:{cmdAndarguments}");
                p = new Process();
                p.StartInfo.FileName = ExecutablePath; //设置要启动的应用程序           
                p.StartInfo.CreateNoWindow = true;// //不显示程序窗口.表示不要为这个命令单独创建一个控制台窗口
                                                  //实际上如果使用此代码的程序也是一个控制台程序，这句是没有必要的，因为子进程会共用父进程的控制台窗口；但是对于 GUI 程序来说，这句还是很重要的，这可以避免在执行命令的过程中意外弹出一个黑色的控制台窗口出来。

                p.StartInfo.UseShellExecute = false;////是否使用操作系统shell启动,设置为 false 表示不要使用 ShellExecute 函数创建进程                                           
                p.StartInfo.RedirectStandardInput = true;  // 接受来自调用程序的输入信息
                p.StartInfo.RedirectStandardOutput = true;//进行输出的重定向
                                                          //这是一定要设置为 true 的属性，因为我们希望拿到命令的输出结果。
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.WorkingDirectory = WorkingDirectory;//进行输出的重定向
                                                                // 这是一定要设置为 true 的属性，因为我们希望拿到命令的输出结果。
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //  p.OutputDataReceived += call;
                p.OutputDataReceived += new DataReceivedEventHandler(OnOutputDataReceived);
                p.ErrorDataReceived += new DataReceivedEventHandler(OnErrorDataReceived);

                //p.OutputDataReceived -= OnOutputDataReceived;
                //p.OutputDataReceived += OnOutputDataReceived;
                //启用Exited事件
                p.EnableRaisingEvents = true;
                p.Exited += new EventHandler(Process_Exited); // 注册进程结束事件
                                                              // 
                p.StartInfo.Arguments = cmdAndarguments;
                if (callback != null)
                {
                    p.Exited += new EventHandler(callback); // 注册进程结束事件 
                }
                //启动程序
                p.Start();
                p.BeginOutputReadLine();// 开启异步读取输出操作
                p.BeginErrorReadLine();// 开启异步读取错误操作
                p.StandardInput.AutoFlush = true;

                //输入命令
                //////向cmd窗口发送输入信息
                p.StandardInput.WriteLine(cmdAndarguments);
                p.StandardInput.WriteLine("exit");
                //获取输出信息
                //string strOuput = p.StandardOutput.ReadToEnd();
                //等待程序执行完退出进程
                // 如果打开注释，则以同步方式执行命令，此例子中用Exited事件异步执行。
                p.WaitForExit();
                //p.Close();
                //return strOuput;
                //return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 输出指令执行结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            /*
            if (null == e || string.IsNullOrEmpty(e.Data))
            {
                Console.WriteLine($">>>>>>{ExecutablePath} command error!!!!!");
                // Debug.Log(">>>>>>Git command error!!!!!");
                return;
            }*/
            //if (e.Data != null)
            //{
            Console.WriteLine(e.Data);
            // }
        }
        private void OnErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            //if (e.Data != null)
            //{
            Console.WriteLine(e.Data);
            //}
        }

        private void Process_Exited(object sender, EventArgs e)
        {
            if (p != null) { 
                p.Close();
            }
            Console.WriteLine($"{ExecutablePath} {p.StartInfo.Arguments}命令执行完毕!");
        }
    }


}
