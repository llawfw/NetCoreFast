﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGen
{
    public class FileUtil
    {
        /// <summary>
        /// 复制文件夹及文件
        /// </summary>
        /// <param name="sourceFolder">原文件路径</param>
        /// <param name="destFolder">目标文件路径</param>
        /// <returns></returns>
        public static bool CopyFolder(string sourceFolder, string destFolder, List<string> excludes = null)
        {
            try
            {               
                //int num = 0;
                //得到原文件根目录下的所有文件
                string[] files = System.IO.Directory.GetFiles(sourceFolder);
                foreach (string file in files)
                {
                    // num++;
                    
                    if (excludes != null && excludes.Where(o => file.Contains(o)).Count() > 0)
                    {
                        Console .WriteLine("忽略:"+file);
                        continue;
                    }
                    //如果目标路径不存在,则创建目标路径
                    if (!System.IO.Directory.Exists(destFolder))
                    {
                        System.IO.Directory.CreateDirectory(destFolder);
                    }
                    string name = System.IO.Path.GetFileName(file);
                    string dest = System.IO.Path.Combine(destFolder, name);
                    System.IO.File.Copy(file, dest,true);//复制文件
                    Console.WriteLine($"from:{file} to:{dest}");
                }
                //得到原文件根目录下的所有文件夹
                string[] folders = System.IO.Directory.GetDirectories(sourceFolder);
                foreach (string folder in folders)
                {
                    string name = System.IO.Path.GetFileName(folder);
                    string dest = System.IO.Path.Combine(destFolder, name);
                    CopyFolder(folder, dest, excludes);//构建目标路径,递归复制文件
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        /// <summary>

        /// 用来遍历删除目录下的文件以及该文件夹

        /// </summary>

        public static bool DeleteFolder(string path)
        {
            DirectoryInfo info=new DirectoryInfo(path);
            foreach (DirectoryInfo newInfo in info.GetDirectories())
            {
                DeleteFolder(newInfo.FullName);
            }
            foreach (FileInfo newInfo in info.GetFiles())
            {
                newInfo.Attributes = newInfo.Attributes & ~(FileAttributes.Archive | FileAttributes.ReadOnly | FileAttributes.Hidden);
                newInfo.Delete();
            }
            info.Attributes = info.Attributes & ~(FileAttributes.Archive | FileAttributes.ReadOnly | FileAttributes.Hidden);
            info.Delete();
            return true;
        }       
        /// <summary>
        /// 重命名文件
        /// </summary>
        /// <param name="srcfilepath">源文件全路径（包括名字）</param>
        /// <param name="newFileName">新文件名字（不包括路径）</param>
        /// <returns></returns>
        public static bool RenameFile(string srcfilepath, string newFileName)
        {
            if (!File.Exists(srcfilepath))
            {
                return false; ;
            }
            string filePath = Path.GetDirectoryName(srcfilepath);
            string newFilePath = Path.Combine(filePath, newFileName);
            System.IO.FileInfo file = new System.IO.FileInfo(srcfilepath);
            file.MoveTo(newFilePath);
            return true;
        }
    }
}
