﻿// See https://aka.ms/new-console-template for more information
using System.Reflection;
using CodeGen;
using Plugin;
//获取项目目录   
string projectDir = Environment.CurrentDirectory;
//项目名称
string projectName = projectDir.Substring(projectDir.LastIndexOf(@"\") + 1);
int index = projectDir.IndexOf(@"\bin\Debug");
if (index == -1)
{
    index = projectDir.IndexOf(@"\bin\Release");
}
if (index != -1)
{
    projectDir = projectDir.Substring(0, index);
    projectDir = projectDir.Substring(0, projectDir.LastIndexOf(@"\"));
    projectName = projectDir.Substring(projectDir.LastIndexOf(@"\") + 1);
}
Console.WriteLine("项目目录:" + projectDir);
Console.WriteLine("项目名称:" + projectName);
List<Menu> menus = new List<Menu>();
Menu m1 = new Menu("NetCoreFast Api接口项目生成");
menus.Add(m1);
Menu m11 = new Menu("初始项目");
m11.Method = "InitProject";
Menu m12 = new Menu("生成代码");
m12.Method = "GenCode";
Menu m13 = new Menu("删除生成的代码");
m13.Method = "DelCode";
Menu m14 = new Menu("升级项目");
m14.Method = "UpdateProject";
m1.Childs.Add(m11);
m1.Childs.Add(m12);
m1.Childs.Add(m13);
m1.Childs.Add(m14);
List<string> excludes = new List<string>();
excludes.Add(".git");
excludes.Add(@"\.git");
excludes.Add(@"\bin");
excludes.Add(@"\obj");
excludes.Add(@"\Properties");
m1.Excludes = excludes;
Menu m2 = new Menu("Elementplus-admin-codegen后台前端管理界面项目生成");
menus.Add(m2);
Menu m21 = new Menu("初始项目");
m21.Method = "InitProject";
Menu m22 = new Menu("升级项目");
m22.Method = "UpdateProject";
m2.Childs.Add(m21);
m2.Childs.Add(m22);
List<string> excludes2 = new List<string>();
excludes2.Add(".git");
excludes2.Add(@"\.git");
m2.Excludes = excludes2;
while (true)
{
    List<string> _excludes = new List<string>();
    Console.WriteLine("***主功能菜单***");
    for (var i = 0; i < menus.Count; i++)
    {
        Menu m = menus[i];
        Console.WriteLine($"{i + 1}.{m.Name}");
    }
    int idx = 0;
    while (idx <= 0 || idx > menus.Count)
    {
        Console.WriteLine("***请选择生成项目序号***");
        var xh = Console.ReadLine();
        idx = Convert.ToInt32(xh);
    }
    var currentMenu = menus[idx - 1];//当前菜单
    _excludes.AddRange(currentMenu.Excludes);
    Console.WriteLine($"***子功能菜单***");
    for (var i = 0; i < currentMenu.Childs.Count; i++)
    {
        Menu m = currentMenu.Childs[i];
        Console.WriteLine($" {i + 1}.{m.Name}");
    }
    int idx2 = 0;
    while (idx2 <= 0 || idx2 > currentMenu.Childs.Count)
    {
        Console.WriteLine($"***请选择操作序号***");
        var xh = Console.ReadLine();
        idx2 = Convert.ToInt32(xh);
    }
    currentMenu = currentMenu.Childs[idx2 - 1];
    _excludes.AddRange(currentMenu.Excludes);
    var invokemethod = currentMenu.Method;//调用方法
    if (idx == 1)
    {
        NetCoreFastProjectGen obj = new NetCoreFastProjectGen(projectDir, projectName, _excludes);
        Type type = obj.GetType();      // 通过类名获取同名类
        MethodInfo method = type.GetMethod(invokemethod, new Type[] { });      // 获取方法信息
        object[] parameters = null;
        method.Invoke(obj, parameters);
    }
    if (idx == 2)
    {
        Vue3ElementPlusProjectGen obj = new Vue3ElementPlusProjectGen(projectDir, projectName, _excludes);
        Type type = obj.GetType();      // 通过类名获取同名类
        MethodInfo method = type.GetMethod(invokemethod, new Type[] { });      // 获取方法信息
        object[] parameters = null;
        method.Invoke(obj, parameters);
    }
    Console.WriteLine($"---按任意键继续---");
    Console.ReadLine();
}

class NetCoreFastProjectGen
{
    public string projectRootDir { get; set; }
    public string projectDir { get; set; }
    //项目名称
    public string projectName { get; set; }
    public List<string> excludes = new List<string>();
    public NetCoreFastProjectGen(string projectDir, string projectName, List<string> excludes)
    {
        this.projectRootDir = projectDir;
        if (projectName != "NetCoreFast")
        {
            this.projectDir = projectDir + $@"\{projectName}Api\";
            this.projectName = $"{projectName}Api";
        }
        else
        {
            this.projectDir = projectDir;
            this.projectName = projectName;
        }
        this.excludes = excludes;
    }
    //1初始化项目
    public void InitProject()
    {
        if (projectName == "NetCoreFast")//本项目不下载 担心覆盖
        {
            return;
        }
        /*
        using (StreamReader sr = new StreamReader(typeof(Program).Assembly.GetManifestResourceStream(typeof(Program).Assembly.GetName().Name + "." +"AutoMapperProfile.txt")))
        {
            //var template = Mustachio.Parser.Parse(sr.ReadToEnd());
            // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
            // 解析获取最终数据
            Console.WriteLine(sr.ReadToEnd());
        }
        */
        //projectDir = projectDir.Substring(0, projectDir.IndexOf("CodeGen"));
        //Console.WriteLine("请输入项目名称:");
        //string projectName = Console.ReadLine();
        /*
        var projectDir = @"d:\"
        var cmd = new CommandRunner("cmd", projectDir);
        cmd.Run(@"mkdir d", Download_Project);
        ;*/
        Download_Project(null, null);
        //Copy_Project(null, null);
        // Delete_Old_Project(null, null);
        // Alter_Project_Name(null, null);
        // Alter_Project_Name(null, null);
        Console.WriteLine("初始项目成功！");
    }
    private void Download_Project(object sender, EventArgs e)
    {
        Console.WriteLine();
        Console.WriteLine("正在下载原始项目……");
        Console.WriteLine();
        Console.WriteLine($"开始克隆NetCoreFast……【https://gitee.com/qinyongcheng/NetCoreFast.git 分支NET6 】");
        var git = new CommandRunner("git", projectRootDir);
        Console.WriteLine();
        //git.Run(@"config --global http.postBuffer 524288000");
        git.Run(@"clone -b NET6 https://gitee.com/qinyongcheng/NetCoreFast.git --depth 1", Copy_Project);
    }
    private void Copy_Project(object sender, EventArgs e)
    {
        Console.WriteLine();
        Console.WriteLine("正在复制原始项目文件……");
        Console.WriteLine();
        Console.WriteLine("以下文件不会被复制:");
        foreach (var i in excludes)
        {
            Console.WriteLine(i);
        }
        Console.WriteLine();
        //xcopy /d c:\123\* d:\abc\
        string f = projectRootDir + @"\NetCoreFast";
        string t = projectDir;
        //cmd.Run($@"xcopy /d /s /e /y {excludes} " + f + " " + t, Delete_Old_Project);
        FileUtil.CopyFolder(f, t, excludes);
        Console.WriteLine("复制完成!");
        Delete_Old_Project(null, null);
    }
    private void Delete_Old_Project(object sender, EventArgs e)
    {
        Console.WriteLine();
        Console.WriteLine("正在删除原始项目……");
        Console.WriteLine();
        //rd /s/q d:\123 这样就删除了
        string f = projectRootDir + @"\NetCoreFast";
        //cmd.Run(@$"rd /s/q {f}", Alter_Project_Name);
        FileUtil.DeleteFolder(f);
        Console.WriteLine("删除完成!");
        Alter_Project_Name(null, null);
    }
    private void Alter_Project_Name(object sender, EventArgs e)
    {
        Console.WriteLine();
        Console.WriteLine("正在修改项目名称……");
        Console.WriteLine();
        //ren 123.docx 321.txt
        string f = $@"{projectDir}\NetCoreFast.sln";
        string t = @$"{projectName}.sln";
        //cmd.Run(@$"rename {f} {t}");
        FileUtil.RenameFile(f, t);
        Console.WriteLine("修改完成!");
    }
    //2生成代码
    public void GenCode()
    {
        //获取项目目录
        //string projectDir = Environment.CurrentDirectory;
        //projectDir = projectDir.Substring(0, projectDir.IndexOf("CodeGen"));    
        var modelDir = projectDir + "\\Model";
        var webDir = projectDir + "\\Web";
        var dtoDir = modelDir + "\\Dto";
        //实体类集合
        List<Type> allTypes = new List<Type>();
        //获取model.dll目录  
        string dllDir = Environment.CurrentDirectory;
        int index = dllDir.IndexOf(@"\CodeGen\bin");
        //如果是生成的单个项目
        if (index == -1)
        {
            Console.WriteLine(dllDir);
            dllDir = modelDir + @"\bin\Debug\net6.0";
            Console.WriteLine(dllDir);
            AssemblyLoader loader = new AssemblyLoader(dllDir);
            var types = loader.Load("Model.dll");
            allTypes.AddRange(types);
            Console.WriteLine(types.Count());
        }
        else
        {
            //获取model.entity下的所有类 
            List<string> _Assemblies = new List<string>() { "Model" };
            var assemblys = _Assemblies.Select(x => Assembly.Load(x)).ToList();
            assemblys.ForEach(aAssembly =>
            {
                allTypes.AddRange(aAssembly.GetTypes());
            });
        }
        //List<Type> allTypes = ClassCreator.LoadTypes(modelDir);
        var entities = allTypes.Where(c => c.Namespace != null && c.Namespace.StartsWith("Model.Entity") && !c.IsAbstract && c.IsPublic && c.IsClass);
        Console.WriteLine("正在生成dto……");
        foreach (var t in entities)
        {
            Gen.GenDtoClass(t, dtoDir, webDir);
            //GetProperties(t);
        }
        //自动映射生成dal、bll、automapping、controller映射
        Console.WriteLine("正在生成Bll、Automapping、Controller……");
        Gen.GenBllControllerMappingCode(projectDir, dtoDir, entities);
        Console.WriteLine("代码生成成功");

        //Type type = t.GetType();
        //type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)//不要父类的
        //也可以这么写，去掉父类的Id//type.GetProperties.Where(p => !p.Name.Equals("Id"));
        //
    }
    public void DelCode()
    {
        //获取项目目录
        //string projectDir = Environment.CurrentDirectory;
        //projectDir = projectDir.Substring(0, projectDir.IndexOf("CodeGen"));    
        var modelDir = projectDir + "\\Model";
        var webDir = projectDir + "\\Web";
        var dtoDir = modelDir + "\\Dto";
        //实体类集合
        List<Type> allTypes = new List<Type>();
        //获取model.dll目录  
        string dllDir = Environment.CurrentDirectory;
        int index = dllDir.IndexOf(@"\CodeGen\bin");
        //如果是生成的单个项目
        if (index == -1)
        {
            Console.WriteLine(dllDir);
            dllDir = modelDir + @"\bin\Debug\net6.0";
            Console.WriteLine(dllDir);
            AssemblyLoader loader = new AssemblyLoader(dllDir);
            var types = loader.Load("Model.dll");
            allTypes.AddRange(types);
            Console.WriteLine(types.Count());
        }
        else
        {
            //获取model.entity下的所有类 
            List<string> _Assemblies = new List<string>() { "Model" };
            var assemblys = _Assemblies.Select(x => Assembly.Load(x)).ToList();
            assemblys.ForEach(aAssembly =>
            {
                allTypes.AddRange(aAssembly.GetTypes());
            });
        }
        //List<Type> allTypes = ClassCreator.LoadTypes(modelDir);
        var entities = allTypes.Where(c => c.Namespace != null && c.Namespace.StartsWith("Model.Entity") && !c.IsAbstract && c.IsPublic && c.IsClass);
        Console.WriteLine("正在删除Dto、Bll、Automapping、Controller……");
        Gen.DelBllControllerMappingCode(projectDir, dtoDir, entities);
        Console.WriteLine("删除成功！");
    }
    public void UpdateProject()
    {
        if (projectName == "NetCoreFast")//本项目不下载 担心覆盖
        {
            return;
        }
        Console.WriteLine("正在备份原项目!");
        string from = projectDir;
        string to = projectDir.Substring(0, projectDir.Length - 1) + @"_Backup\";
        FileUtil.CopyFolder(from, to, new List<string>());
        Console.WriteLine("备份完成!开始升级……");

        //string excludes = "";
        //var modelDir = projectDir + @"\Model\";
        //var webDir = projectDir + @"\Web\";
        excludes.Add(@"\Migrations");
        excludes.Add(@"README.md");
        excludes.Add(@"LICENSE");
        excludes.Add($"Startup.cs");
        excludes.Add($"Program.cs");
        excludes.Add($"InitDatabase.cs");
        excludes.Add($"MyDbContext.cs");
        excludes.Add($"appsettings.Development.json");
        excludes.Add($"appsettings.Production.json");
        excludes.Add($"host.json");
        excludes.Add($"nlog.config");
        excludes.Add(@".csproj");
        excludes.Add(@".csproj.user");
        excludes.Add(@".sln");
        Console.WriteLine("以下文件不会被更新:");
        foreach (var f in excludes)
        {
            Console.WriteLine(f);
        }
        //excludes = $"/exclude:{webDir}Startup.cs+{webDir}Program.cs+{modelDir}MyDbContext.cs+.config+.json";
        Download_Project(null, null);
        // Copy_Project(null, null, excludes);
        // Delete_Old_Project(null, null);
        Console.WriteLine("升级项目成功！");
    }
}
class Vue3ElementPlusProjectGen
{
    public string projectRootDir { get; set; }
    public string projectDir { get; set; }
    //项目名称
    public string projectName { get; set; }
    public List<string> excludes = new List<string>();
    public Vue3ElementPlusProjectGen(string projectDir, string projectName, List<string> excludes)
    {
        this.projectRootDir = projectDir;
        this.projectDir = projectDir + $@"\{projectName}UI\";
        this.projectName = $"{projectName}UI";
        this.excludes = excludes;
    }
    //1初始化项目
    public void InitProject()
    {
        if (projectName == "elementplus-admin-codegen")//本项目不下载 担心覆盖
        {
            return;
        }
        Download_Project(null, null);
        Console.WriteLine("初始项目成功！");
    }
    private void Download_Project(object sender, EventArgs e)
    {
        Console.WriteLine();
        Console.WriteLine("正在下载原始项目……");
        Console.WriteLine();
        Console.WriteLine($"开始克隆elementplus-admin-codegen……【https://gitee.com/qinyongcheng/elementplus-admin-codegen.git 分支master 】");
        var git = new CommandRunner("git", projectRootDir);
        Console.WriteLine();
        //git.Run(@"config --global http.postBuffer 524288000");
        git.Run(@"clone -b master https://gitee.com/qinyongcheng/elementplus-admin-codegen.git --depth 1", Copy_Project);
    }
    private void Copy_Project(object sender, EventArgs e)
    {
        Console.WriteLine();
        Console.WriteLine("正在复制原始项目文件……");
        Console.WriteLine();
        Console.WriteLine("以下文件不会被复制:");
        foreach (var i in excludes)
        {
            Console.WriteLine(i);
        }
        Console.WriteLine();
        //xcopy /d c:\123\* d:\abc\
        string f = projectRootDir + @"\elementplus-admin-codegen";
        string t = projectDir;
        //cmd.Run($@"xcopy /d /s /e /y {excludes} " + f + " " + t, Delete_Old_Project);
        FileUtil.CopyFolder(f, t, excludes);
        Console.WriteLine("复制完成!");
        Delete_Old_Project(null, null);
    }
    private void Delete_Old_Project(object sender, EventArgs e)
    {
        Console.WriteLine();
        Console.WriteLine("正在删除原始项目……");
        Console.WriteLine();
        //rd /s/q d:\123 这样就删除了
        string f = projectRootDir + @"\elementplus-admin-codegen";
        //cmd.Run(@$"rd /s/q {f}", Alter_Project_Name);
        FileUtil.DeleteFolder(f);
        Console.WriteLine("删除完成!");
        Alter_Project_Name(null, null);
    }
    private void Alter_Project_Name(object sender, EventArgs e)
    {
        Console.WriteLine();
        //Console.WriteLine("正在修改项目名称……");
        Console.WriteLine();
        //ren 123.docx 321.txt
        // string f = $@"{projectDir}\NetCoreFast.sln";
        //string t = @$"{projectName}.sln";
        //cmd.Run(@$"rename {f} {t}");
        //FileUtil.RenameFile(f, t);
        //Console.WriteLine("修改完成!");
    }
    //2生成代码
    public void GenCode()
    {

    }
    public void UpdateProject()
    {
        Console.WriteLine("正在备份原项目!");
        string from = projectDir;
        string to = projectDir.Substring(0, projectDir.Length - 1) + @"_Backup\";
        FileUtil.CopyFolder(from, to, new List<string>());
        Console.WriteLine("备份完成!开始升级……");

        excludes.Add(@".editorconfig");
        excludes.Add(@".env.development");
        excludes.Add(@".env.production");
        excludes.Add(@".env.staging");
        excludes.Add(@".eslintignore");
        excludes.Add(@".eslintrc.js");
        excludes.Add(@".gitignore");
        excludes.Add(@".prettierignore");
        excludes.Add(@".prettierrc.js");
        excludes.Add(@"commitlint.config.js");
        excludes.Add(@"index.html");
        excludes.Add(@"LICENSE");
        excludes.Add(@"package.json");
        excludes.Add(@"README.en.md");
        excludes.Add(@"README.md");
        excludes.Add(@"tsconfig.json");
        excludes.Add(@"vite.config.ts");
        excludes.Add(@"\lang");
        excludes.Add(@"\layout");
        excludes.Add(@"\router");
        excludes.Add(@"settings.ts");
        excludes.Add(@"main.ts");
        excludes.Add(@"App.vue");
        excludes.Add(@"views\index.vue");
        excludes.Add(@"\dashboard");
        excludes.Add(@"user.ts");
        excludes.Add(@"store\index.ts");
        excludes.Add(@"login");
        Console.WriteLine("以下文件不会被更新:");
        foreach (var f in excludes)
        {
            Console.WriteLine(f);
        }
        //excludes = $"/exclude:{webDir}Startup.cs+{webDir}Program.cs+{modelDir}MyDbContext.cs+.config+.json";
        Download_Project(null, null);
        Console.WriteLine("升级项目成功！");
    }
}
class Menu
{
    public string Name { get; set; }
    public List<Menu> Childs { get; set; } = new List<Menu>();
    public List<string> Excludes { get; set; } = new List<string>();
    public string Method { get; set; }
    public Menu(string name)
    {
        Name = name;
    }
}