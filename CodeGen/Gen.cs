﻿/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using Model.Entity;
using Common.Util;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using System.Text.Json.Serialization;
using System.CodeDom;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Reflection.Metadata;

namespace CodeGen
{
    /// <summary>
    ///代码生成器。 自动生成dal、bll、AutoMapperProfile\Controller
    ///<seealso cref="https://github.com/wildbit/mustachio/wiki"/>
    ///<see cref="https://postmarkapp.com/blog/special-delivery-postmark-templates"/>
    /// </summary>
    public static class Gen
    {

        public static void GenBllControllerMappingCode(string projectDir, string dtoDir, IEnumerable<Type> entities)

        {
            var webDir = projectDir + "\\Web";
            //Console.WriteLine(Environment.CurrentDirectory);
            var content = "";
            string[] projects = new string[] { "BLL" };
            Dictionary<string, dynamic> model = new Dictionary<string, dynamic>();
            List<dynamic> mappings = new List<dynamic>();

            foreach (var e in entities)
            {
                string typeComment = GetTypeComment(webDir, e);//获取实体类注释
                var namespaceStrO = e.Namespace.Replace("Model.Entity.", "");
                var namespaceDirStr = namespaceStrO.Replace("\\.", "\\");

                model.Remove("entityName");//实体类名
                model.Add("entityName", e.Name);
                model.Remove("typeComment");//类注释
                model.Add("typeComment", typeComment);
                model.Remove("entityNamespace");//实体类的命名空间
                model.Add("entityNamespace", e.Namespace);
                model.Remove("bllNamespace");//业务类的命名空间
                model.Add("bllNamespace", $"BLL.{namespaceStrO}");
                //生成bll
                foreach (var p in projects)
                {
                    // Console.WriteLine(e.Name);
                    //查看bll是否有扩展方法（代码自动生成，重新生成会覆盖掉原来的生成的文件。为了避免人工写的代码不被覆盖，人工写的代码放在单独的类文件，生成的时候自动把人工写的和代码生成的合并）
                    //人工写的代码放在etc扩展目录
                    var etcf = @$"{projectDir}\{p}\Etc\{namespaceDirStr}\{e.Name}Bll.cs";
                    if (File.Exists(etcf))//有扩展方法，则合并
                    {
                        using (StreamReader sr = new StreamReader(etcf))
                        {
                            var template = Mustachio.Parser.Parse(sr.ReadToEnd());
                            // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
                            // 解析获取最终数据
                            content = template(model);
                            //Console.WriteLine(content);
                            //正则表表达式获取人工写的接口代码（方法）
                            //https://www.5axxw.com/questions/content/bchikz
                            Regex regexinterface = new Regex(@"\s*public\s*interface\s*I" + e.Name + @"Bll\s*{([\s\S]*?)}");
                            //Regex regexinterface = new Regex(@"\s*public interface IUserBll\s*(?<=\{)[^}]*(?=\})");
                            Match match = regexinterface.Match(content);
                            /*
                            var results = Regex.Matches(content, @"(?<=(?<!{)(?:{{)*{)[^{}]*(?=}(?:}})*(?!}))")
    .Cast<Match>()
    .Select(x => x.Value)
    .ToList();*/
                            if (match.Success)
                            {
                                //Console.WriteLine(match.Groups[1]);
                                model.Remove("iblletc");
                                model.Add("iblletc", match.Groups[1].Value);
                            }
                            //正则表表达式获取人工写的接口实现代码（方法）
                            Regex regexclass = new Regex(@"\s*public\s*class\s*" + e.Name + @"Bll\s*:\s*BaseBll<" + e.Name + @">\s*,\s*I" + e.Name + @"Bll\s*{([\s\S]*?)}\s*}", RegexOptions.RightToLeft);
                            Match match2 = regexclass.Match(content);
                            // Console.WriteLine(match2.Success);
                            if (match2.Success)
                            {
                                // Console.WriteLine(match2.Groups[1]);
                                model.Remove("blletc");
                                string b = match2.Groups[1].Value;
                                model.Add("blletc", b);
                            }
                            ////正则表表达式获取人工写的扩展类的引入using，并合并
                            Regex regexusing = new Regex(@"\s*([\s\S]*?)\s*namespace");
                            Match match3 = regexusing.Match(content);
                            // Console.WriteLine(match3.Success);
                            if (match3.Success)
                            {
                                // Console.WriteLine(match3.Groups[1]);
                                model.Remove("using");
                                model.Add("using", match3.Groups[1].Value);
                            }
                            else
                            {
                                var u = $"";
                                model.Remove("using");
                                model.Add("using", u);
                            }
                        }
                    }
                    //生成业务接口IBll
                    //using (StreamReader sr = new StreamReader(projectDir + "/CodeGen/" + $"/I{p}.txt"))
                    using (StreamReader sr = new StreamReader(typeof(Program).Assembly.GetManifestResourceStream(typeof(Program).Assembly.GetName().Name + "." + $"I{p}.txt")))
                    {
                        model.Remove("namespace");
                        model.Add("namespace", $"{p}.{namespaceDirStr}");

                        var template = Mustachio.Parser.Parse(sr.ReadToEnd());
                        // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
                        // 解析获取最终数据
                        content = template(model);
                    }
                    Directory.CreateDirectory(@$"{projectDir}\{p}\{namespaceDirStr}");
                    using (StreamWriter sw = new StreamWriter(projectDir + $"\\{p}\\{namespaceDirStr}\\I{e.Name}{p}.cs", false))
                    {
                        //保存数据到文件
                        sw.Write(content.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&").Replace("&quot;", "\"").Replace("&quot;", "\""));
                    }
                    //生成业务接口实现BLL.Impl
                    //using (StreamReader sr = new StreamReader(projectDir + "/CodeGen/" + $"/{p}.txt"))
                    using (StreamReader sr = new StreamReader(typeof(Program).Assembly.GetManifestResourceStream(typeof(Program).Assembly.GetName().Name + "." + $"{p}.txt")))
                    {
                        model.Remove("namespace");
                        model.Add("namespace", $"{p}.{namespaceDirStr}.Impl");
                        var template = Mustachio.Parser.Parse(sr.ReadToEnd());
                        // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
                        // 解析获取最终数据
                        content = template(model);
                    }
                    //Console.WriteLine(content);
                    Directory.CreateDirectory(@$"{projectDir}\{p}\{namespaceDirStr}\\Impl");
                    using (StreamWriter sw = new StreamWriter(projectDir + $"\\{p}\\{namespaceDirStr}\\Impl\\{e.Name}{p}.cs", false))
                    {
                        //保存数据到文件
                        sw.Write(content.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&").Replace("&quot;", "\"").Replace("&quot;", "\""));
                    }
                }
                model.Remove("iblletc");
                model.Remove("blletc");

                model.Remove("using");
                model.Remove("controlleretc");
                //查看controller是否有扩展方法（代码自动生成，重新生成会覆盖掉原来的生成的文件。为了避免人工写的代码不被覆盖，人工写的代码放在单独的类文件，生成的时候自动把人工写的和代码生成的合并）
                //人工写的代码放在etc扩展目录
                var etcf2 = @$"{projectDir}\Web\Controllers\Etc\{namespaceDirStr}\{e.Name}Controller.cs";
                if (File.Exists(etcf2))//有扩展方法，则合并
                {
                    using (StreamReader sr = new StreamReader(etcf2))
                    {
                        var template = Mustachio.Parser.Parse(sr.ReadToEnd());
                        // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
                        // 解析获取最终数据
                        content = template(model);
                        //Console.WriteLine(content);                    
                        //正则表表达式获取人工写的对应controller类的方法
                        Regex regexclass = new Regex(@"\s*public\s*class\s*" + e.Name + @"Controller\s*:\s*MyApiBaseController<\s*" + e.Name + @"\s*,\s*" + e.Name + @"ListDto\s*,\s*" + e.Name + @"AddDto\s*,\s*" + e.Name + @"UpdateDto" + @"\s*>\s*{([\s\S]*?)}\s*}", RegexOptions.RightToLeft);
                        Match match2 = regexclass.Match(content);
                        // Console.WriteLine(match2.Success);
                        if (match2.Success)
                        {
                            //Console.WriteLine(match2.Groups[1]);
                            model.Remove("controlleretc");
                            string b = match2.Groups[1].Value;
                            model.Add("controlleretc", b);
                        }
                        else
                        {
                            model.Remove("controlleretc");
                            model.Add("controlleretc", "");
                        }
                        ////正则表表达式获取人工写的扩展类的引入using，并合并
                        Regex regexusing = new Regex(@"\s*([\s\S]*?)\s*namespace");
                        Match match = regexusing.Match(content);
                        // Console.WriteLine(match3.Success);
                        if (match.Success)
                        {
                            // Console.WriteLine(match3.Groups[1]);
                            model.Remove("using");
                            model.Add("using", match.Groups[1].Value);
                        }
                        else
                        {
                            var u = $"";
                            model.Remove("using");
                            model.Add("using", u);
                        }
                    }

                }
                //生成控制器
                var modulePath = namespaceStrO.Replace("\\.", "/");//模块路径
                model.Remove("modulePath");
                model.Add("modulePath", modulePath);
                //实体映射
                var moduleName = namespaceStrO.Replace("\\.", "_");//模块名
                model.Remove("moduleName");
                model.Add("moduleName", moduleName);

                model.Remove("baseClass");
                model.Add("baseClass", "MyApiBaseController");

                var controllerGenAttribute = e.GetCustomAttribute<ControllerGenAttribute>();//只读属性

                model.Remove("genListAction");
                model.Remove("genAddAction");
                model.Remove("genUpdateAction");
                model.Remove("genDetailAction");
                model.Remove("genDeleteAction");
                if (controllerGenAttribute != null)
                {
                    model.Remove("baseClass");
                    model.Add("baseClass", "MyEmptyApiBaseController");
                    model.Add("genListAction", controllerGenAttribute.List);
                    model.Add("genAddAction", controllerGenAttribute.Add);
                    model.Add("genUpdateAction", controllerGenAttribute.Update);
                    model.Add("genDetailAction", controllerGenAttribute.Detail);
                    model.Add("genDeleteAction", controllerGenAttribute.Delete);
                }
                //using (StreamReader sr = new StreamReader(projectDir + "/CodeGen/Controller.txt"))
                using (StreamReader sr = new StreamReader(typeof(Program).Assembly.GetManifestResourceStream(typeof(Program).Assembly.GetName().Name + "." + $"Controller.txt")))
                {
                    var template = Mustachio.Parser.Parse(sr.ReadToEnd());
                    // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
                    // 解析获取最终数据
                    content = template(model);
                }
                string dir = projectDir + $@"\Web\Controllers\{namespaceDirStr}\";
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                using (StreamWriter sw = new StreamWriter($@"{dir}\{e.Name}Controller.cs", false))
                {
                    //保存数据到文件
                    sw.Write(content.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&").Replace("&quot;", "\"").Replace("&quot;", "\""));
                    // Console.WriteLine(content);
                }
                model.Remove("using");


                Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
                dict.Add("entity_name", e.Name);
                dict.Add("entity_title", typeComment);
                dict.Add("moduleName", moduleName);
                dict.Add("entityNamespace", e.Namespace);
                dict.Add("dtoNamespace", e.Namespace.Replace("Entity", "Dto"));
                mappings.Add(dict);

            }

            //生成auotomapping映射文件
            model.Add("mappings", mappings);
            //using (StreamReader sr = new StreamReader(projectDir + "/CodeGen/AutoMapperProfile.txt"))
            using (StreamReader sr = new StreamReader(typeof(Program).Assembly.GetManifestResourceStream(typeof(Program).Assembly.GetName().Name + "." + $"AutoMapperProfile.txt")))
            {
                var template = Mustachio.Parser.Parse(sr.ReadToEnd());
                // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
                // 解析获取最终数据
                content = template(model);
            }
            using (StreamWriter sw = new StreamWriter(dtoDir + $"\\AutoMapperProfile.cs", false))
            {
                //保存数据到文件
                sw.Write(content);
                // Console.WriteLine(content);
            }

            //生成初始数据文件
            // using (StreamReader sr = new StreamReader(projectDir + "/CodeGen/InitDatas.txt"))
            using (StreamReader sr = new StreamReader(typeof(Program).Assembly.GetManifestResourceStream(typeof(Program).Assembly.GetName().Name + "." + $"InitDatas.txt")))
            {
                var template = Mustachio.Parser.Parse(sr.ReadToEnd());
                // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
                // 解析获取最终数据
                content = template(model);
            }
            using (StreamWriter sw = new StreamWriter(webDir + $"\\DB\\InitDatas.cs", false))
            {
                //保存数据到文件
                sw.Write(content);
                // Console.WriteLine(content);
            }

        }
        public static void DelBllControllerMappingCode(string projectDir, string dtoDir, IEnumerable<Type> entities)

        {
            var webDir = projectDir + "\\Web";
            string[] projects = new string[] { "BLL" };

            foreach (var e in entities)
            {
                var namespaceStrO = e.Namespace.Replace("Model.Entity.", "");
                var namespaceDirStr = namespaceStrO.Replace("\\.", "\\");
                //删除dto
                var path = @$"{dtoDir}\{namespaceDirStr}\{e.Name}Dto.cs";
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                Dictionary<string, dynamic> model = new Dictionary<string, dynamic>();
                List<dynamic> mappings = new List<dynamic>();
                //生成auotomapping映射文件
                model.Add("mappings", mappings);
                var content = "";
                //using (StreamReader sr = new StreamReader(projectDir + "/CodeGen/AutoMapperProfile.txt"))
                using (StreamReader sr = new StreamReader(typeof(Program).Assembly.GetManifestResourceStream(typeof(Program).Assembly.GetName().Name + "." + $"AutoMapperProfile.txt")))
                {
                    var template = Mustachio.Parser.Parse(sr.ReadToEnd());
                    // 创建模板数据，也可以是 Dictionary<string,object> 类型的             
                    // 解析获取最终数据
                    content = template(model);
                }
                using (StreamWriter sw = new StreamWriter(dtoDir + $"\\AutoMapperProfile.cs", false))
                {
                    //保存数据到文件
                    sw.Write(content);
                    // Console.WriteLine(content);
                }

                //删除bll
                foreach (var p in projects)
                {
                    path = projectDir + $"\\{p}\\{namespaceDirStr}\\I{e.Name}{p}.cs";
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    path = projectDir + $"\\{p}\\{namespaceDirStr}\\Impl\\{e.Name}{p}.cs";
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                }
                //删除controller
                string dir = projectDir + $@"\Web\Controllers\{namespaceDirStr}\";
                path = $@"{dir}\{e.Name}Controller.cs";
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
        }
        public static void GenInitDatas(Type e, string webDir)
        {
            var typeComment = Gen.GetTypeComment(webDir, e);
            List<dynamic> mappings = new List<dynamic>();
        }
        public static PropertyInfo[] GetProperties(Type type)
        {
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo f in properties)
            {
                //System.Console.WriteLine(f.PropertyType+"-"+ f.Name);
                //Debug.Log(f); // + "  " + "属性 " + f.Attributes
                // Debug.Log(f.Name);
                // Debug.Log(f.FieldType);
            }
            return properties;
        }
        /// <summary>
        /// 生成dto类
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dtoDir"></param>
        /// <param name="webDir"></param>
        public static void GenDtoClass(Type type, string dtoDir, string webDir)
        {
            var namespaceStr = type.Namespace.Replace("Model.Entity.", "").Replace("\\.", "\\");
            //System.Console.WriteLine(namespaceStr); 
            string typeComment = GetTypeComment(webDir, type);//获取类注释

            StringBuilder sb = new StringBuilder();
            var dtoList = GenDtoListClass(type, typeComment, webDir);
            sb.Append(dtoList);
            var dtoAdd = GenDtoAddClass(type, typeComment, webDir);
            sb.Append(dtoAdd);
            var dtoUpdate = GenDtoUpdateClass(type, typeComment, webDir);
            sb.Append(dtoUpdate);
            Directory.CreateDirectory(@$"{dtoDir}\{namespaceStr}");
            using (StreamWriter sourceWriter = new StreamWriter(@$"{dtoDir}\{namespaceStr}\{type.Name}Dto.cs"))
            {
                sourceWriter.Write(sb);
            }
        }
        /// <summary>
        /// 生成列表dto类
        /// </summary>
        /// <param name="type"></param>
        /// <param name="typeComment"></param>
        /// <param name="webDir"></param>
        /// <returns></returns>
        static string GenDtoListClass(Type type, string typeComment, string webDir)
        {
            ClassCreator creator = new ClassCreator();
            //creator.AddFields("A", typeof(int));
            //creator.AddFields("A", typeof(string));
            //creator.AddProperties("B", typeof(string));
            //creator.AddProperties("BList", typeof(List<string>));
            //继承或实现父类接口
            Type[] myInterfaces = type.GetInterfaces();
            foreach (var i in myInterfaces)
            {
                if (typeof(IUser).FullName == i.FullName)
                {
                    creator.AddBaseType(typeof(IUser));
                }
                if (typeof(ISys).FullName == i.FullName)
                {
                    creator.AddBaseType(typeof(ISys));
                }
                if (typeof(ITenant).FullName == i.FullName)
                {
                    creator.AddBaseType(typeof(ITenant));
                }
            }
            /*
            if (typeof(IUser).IsAssignableFrom(type))
            {
                creator.AddBaseType(typeof(IUser));
            }
            if (typeof(ISys).IsAssignableFrom(type))
            {
                creator.AddBaseType(typeof(ISys));
            }
            if (typeof(ITenant).IsAssignableFrom(type))
            {
                creator.AddBaseType(typeof(ITenant));
            }*/
            PropertyInfo[] properties = GetProperties(type);
            foreach (PropertyInfo p in properties)
            {
                bool flag = false;
                var types = Reflect.GetParentTypes(p.PropertyType);
                foreach (var t in types)
                {
                    if (!t.IsClass) break;
                    if (typeof(ID).FullName == t.FullName) { flag = true; break; }//如果是导航属性则不显示
                }
                if (flag) continue;//如果是导航属性则不显示
                //if (typeof(ID).IsAssignableFrom(p.PropertyType)) continue;//如果是导航属性则不显示
                //if (p.PropertyType.HasImplementedRawGeneric(typeof(ICollection<>))) continue;//如果是导航属性则不显示
                if (p.GetCustomAttribute(typeof(JsonIgnoreAttribute)) != null) continue;//忽略
                var outputAttribute = p.GetCustomAttribute<DtoGenAttribute>();//只读属性
                if (outputAttribute != null && (outputAttribute.NotList)) continue;//忽略
                //获取属性注释
                string propertieComment = GetPropertieComment(webDir, p);
                List<Attribute> customAttributes = new List<Attribute>();
                //获取自定义属性注解
                IEnumerable<Attribute> customAttributes1 = p.GetCustomAttributes();
                foreach (var c in customAttributes1)
                {
                    if (c.GetType() == typeof(FromModelAttribute))//列表可以有FromModelAttribute注解
                    {
                        customAttributes.Add(c);
                    }
                }
                creator.AddProperties(p.Name, p.PropertyType, true, true, propertieComment, customAttributes, p);
            }
            return creator.GenerateCode(type.Namespace.Replace("Entity", "Dto"), type.Name + "ListDto", TypeAttributes.Public, typeComment);
            /*
            if (creator.GenerateCode(type.Namespace, type.Name, TypeAttributes.Public, @$"D:\project\csharp\NFastApi\CodeGen\{type.Name}Dto.cs"))
                Console.WriteLine("Success");
            else
                Console.WriteLine("Failed");
            */
        }
        /// <summary>
        /// 生成添加dto类
        /// </summary>
        /// <param name="type"></param>
        /// <param name="typeComment"></param>
        /// <param name="webDir"></param>
        /// <returns></returns>
        static string GenDtoAddClass(Type type, string typeComment, string webDir)
        {
            ClassCreator creator = new ClassCreator();
            Type[] myInterfaces = type.GetInterfaces();
            foreach (var i in myInterfaces)
            {
                if (typeof(IUser).FullName == i.FullName)
                {
                    creator.AddBaseType(typeof(IUser));
                }
                if (typeof(ISys).FullName == i.FullName)
                {
                    creator.AddBaseType(typeof(ISys));
                }
            }
            //继承或实现父类接口
            /*
            if (typeof(IUser).IsAssignableFrom(type))
            {
                creator.AddBaseType(typeof(IUser));
            }
            if (typeof(ISys).IsAssignableFrom(type))
            {
                creator.AddBaseType(typeof(ISys));
            }*/
            PropertyInfo[] properties = GetProperties(type);
            foreach (PropertyInfo p in properties)
            {
                bool flag = false;
                var types = Reflect.GetParentTypes(p.PropertyType);
                foreach (var t in types)
                {
                    if (!t.IsClass) break;
                    if (typeof(ID).FullName == t.FullName) { flag = true; break; }//如果是导航属性则不显示
                }
                if (flag) continue;//如果是导航属性则不显示
                //if (typeof(ID).IsAssignableFrom(p.PropertyType)) continue;//如果是导航属性则不显示
                if (p.PropertyType.HasImplementedRawGeneric(typeof(ICollection<>))) continue;//如果是导航属性则不显示
                var outputAttribute = p.GetCustomAttribute<DtoGenAttribute>();//只读属性
                if (outputAttribute != null && (outputAttribute.NotAdd || outputAttribute.NotAddUpdate)) continue;//忽略
                                                                                                                  //特性注解
                List<Attribute> customAttributes = new List<Attribute>();
                //生成到添加dto，但是不显示给UI用户，主要用于系统自动赋值
                if (outputAttribute != null && (outputAttribute.AddUpdateButInvisible || outputAttribute.AddButInvisible))
                {
                    SwaggerSchemaAttribute s = new SwaggerSchemaAttribute();
                    customAttributes.Add(s);
                }
                //获取自定义属性注解
                IEnumerable<Attribute> customAttributes1 = p.GetCustomAttributes();
                customAttributes.AddRange(customAttributes1.ToArray());

                //获取属性注释
                string propertieComment = GetPropertieComment(webDir, p);
                var pt = p.PropertyType == typeof(DateTime?) ? typeof(DateTime?) : p.PropertyType;
                creator.AddProperties(p.Name, pt, true, true, propertieComment, customAttributes, p);
            }
            return creator.GenerateCode(type.Namespace.Replace("Entity", "Dto"), type.Name + "AddDto", TypeAttributes.Public, typeComment);
        }
        /// <summary>
        /// 生成修改dto类
        /// </summary>
        /// <param name="type"></param>
        /// <param name="typeComment"></param>
        /// <param name="webDir"></param>
        /// <returns></returns>
        public static string GenDtoUpdateClass(Type type, string typeComment, string webDir)
        {
            ClassCreator creator = new ClassCreator();
            //继承或实现父类接口
            Type[] myInterfaces = type.GetInterfaces();
            foreach (var i in myInterfaces)
            {
                if (typeof(IUser).FullName == i.FullName)
                {
                    creator.AddBaseType(typeof(IUser));
                }
                if (typeof(ISys).FullName == i.FullName)
                {
                    creator.AddBaseType(typeof(ISys));
                }
                if (typeof(IID).FullName == i.FullName)
                {
                    creator.AddBaseType(typeof(IID));
                }
            }
            /*
            if (typeof(IUser).IsAssignableFrom(type))
            {
                creator.AddBaseType(typeof(IUser));
            }
            if (typeof(ISys).IsAssignableFrom(type))
            {
                creator.AddBaseType(typeof(ISys));
            }
            if (typeof(IID).IsAssignableFrom(type))
            {
                creator.AddBaseType(typeof(IID));
            }*/
            PropertyInfo[] properties = GetProperties(type);
            foreach (PropertyInfo p in properties)
            {
                bool flag = false;
                var types = Reflect.GetParentTypes(p.PropertyType);
                foreach (var t in types)
                {
                    if (!t.IsClass) break;
                    if (typeof(ID).FullName == t.FullName) { flag = true; break; }//如果是导航属性则不显示
                }
                if (flag) continue;//如果是导航属性则不显示
                //if (typeof(ID).IsAssignableFrom(p.PropertyType)) continue;//如果是导航属性则不显示
                if (p.PropertyType.HasImplementedRawGeneric(typeof(ICollection<>))) continue;//如果是导航属性则不显示      
                var outputAttribute = p.GetCustomAttribute<DtoGenAttribute>();//只读属性
                if (outputAttribute != null && (outputAttribute.NotUpdate || outputAttribute.NotAddUpdate)) continue;//忽略                                                                                                           //获取自定义属性注解
                                                                                                                     //特性注解
                List<Attribute> customAttributes = new List<Attribute>();
                //生成到添加dto，但是不显示给UI用户，主要用于系统自动赋值
                if (outputAttribute != null && (outputAttribute.AddUpdateButInvisible || outputAttribute.UpdateButInvisible))
                {
                    SwaggerSchemaAttribute s = new SwaggerSchemaAttribute();
                    customAttributes.Add(s);
                }
                //获取自定义属性注解
                IEnumerable<Attribute> customAttributes1 = p.GetCustomAttributes();
                customAttributes.AddRange(customAttributes1.ToArray());
                //获取属性注释
                string propertieComment = GetPropertieComment(webDir, p);

                creator.AddProperties(p.Name, p.PropertyType, true, true, propertieComment, customAttributes, p);
            }
            return creator.GenerateCode(type.Namespace.Replace("Entity", "Dto"), type.Name + "UpdateDto", TypeAttributes.Public, typeComment);
        }
        /// <summary>
        /// 从xml文档获取类注释
        /// </summary>
        /// <param name="webDir"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetTypeComment(string webDir, Type type)
        {
            var xmlCommentHelper = new XmlCommentHelper();
            //从api文档加载注释文档
            xmlCommentHelper.LoadAll(webDir + "/Apidoc");

            //Type type = typeof(MyClass);
            var typeComment = xmlCommentHelper.GetTypeComment(type);
            //Console.WriteLine($"{type.Name}的注释：{typeComment}");
            return typeComment;
        }
        /// <summary>
        /// 从xml文档获取属性注释
        /// </summary>
        /// <param name="webDir"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public static string GetPropertieComment(string webDir, PropertyInfo property)
        {
            var xmlCommentHelper = new XmlCommentHelper();
            //从api文档加载注释文档
            xmlCommentHelper.LoadAll(webDir + "/Apidoc");
            /*
            var fields = type.GetFields(bindingAttr: System.Reflection.BindingFlags.NonPublic);
            foreach (var field in fields)
            {
                var fieldComment = xmlCommentHelper.GetFieldOrPropertyComment(field);
                Console.WriteLine($"{field.Name}字段的注释：{fieldComment}");
            }
            */
            var propertyComment = xmlCommentHelper.GetFieldOrPropertyComment(property);
            return propertyComment;
            /*
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                var propertyComment = xmlCommentHelper.GetFieldOrPropertyComment(property);
                Console.WriteLine($"{property.Name}属性的注释：{propertyComment}");
            }*/
            /*
            var method = type.GetMethod(nameof(MyClass.MyMethod));
            var methodComment = xmlCommentHelper.GetMethodComment(method);
            Console.WriteLine($"{nameof(MyClass.MyMethod)}方法的注释：{methodComment}");

            var dict = xmlCommentHelper.GetParameterComments(method);
            foreach (var pair in dict)
            {
                Console.WriteLine($"{nameof(MyClass.MyMethod)}方法的参数{pair.Key}注释：{pair.Value}");
            }
            */
        }
    }

}
