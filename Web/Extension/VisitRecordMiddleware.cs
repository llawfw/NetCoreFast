﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System;
using System.Threading.Tasks;
using Web.Util;
using System.Net.Http;
using BLL.Sys.Impl;
using BLL.Sys;
using Model.Entity.Sys;
using System.Linq;
using Web.Security;
using IdentityModel;
using Model.Entity;
using Web.Redis;

namespace Web.Extension
{
    /// <summary>
    /// 访问日志中间件
    /// <see cref="https://www.cnblogs.com/personage/p/16091780.html"/>
    /// </summary>
    public class VisitRecordMiddleware
    {
        public readonly static string Key_Today_Visit_Count = "Today_Visit_Count";
        private readonly RequestDelegate _next;
        public ISysVisitRecordBll visitRecordBll { get; set; }
        protected IClaimsAccessor user { get; set; }
        public VisitRecordMiddleware(RequestDelegate next, IClaimsAccessor ClaimsAccessor, ISysVisitRecordBll visitRecordBll)
        {
            this._next = next;
            this.user = ClaimsAccessor;
            this.visitRecordBll = visitRecordBll;
        }
        public async Task Invoke(HttpContext context)
        {
            //检测是否包含api请求接口，如果不是则直接放行
            if (!context.Request.Path.ToString().StartsWith("/api"))
            {
                await _next(context);
                return;
            }
            Stopwatch stopwatch = new Stopwatch();
            //NLogHelper.logger.Info($@"====接口{context.Request.Path}开始计时");
            stopwatch.Start();//启动计时器
            await _next.Invoke(context);
            stopwatch.Stop();//停止秒表。
                             //NLogHelper.logger.Info($@"---接口{context.Request.Path}耗时:{stopwatch.ElapsedMilliseconds}ms");
                             //Console.WriteLine($@"---接口{context.Request.Path}耗时:{stopwatch.ElapsedMilliseconds}ms====");
            /*
            var userids = context.User?.Claims.FirstOrDefault(c => c.Type == JwtClaimTypes.Id)?.Value;
            if (userId != null)
            {
                int id = 0;
                int.TryParse(userId, out id);
                return id;
            }*/
            var ip = context.Request.Headers["X-Forwarded-For"].FirstOrDefault(); // 解决 nginx、docker等 获取ip问题
            ip = !string.IsNullOrEmpty(ip) ? ip : context.Request.HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            if (user.Id > 0)
            {
                var task = Task.Run(async () =>
                  {
                      SysVisitRecord o = new SysVisitRecord();
                      o.Path = context.Request.Path;
                      o.Time = stopwatch.ElapsedMilliseconds;
                      o.Method = context.Request.Method;
                      o.QueryString = context.Request.QueryString.Value;
                      o.UserId = user.Id;
                      o.Ip = ip;
                      await visitRecordBll.AddAsync(o);
                      //Console.WriteLine("SimpleTask");
                  });
            }
            //访问量统计 1个IP1天只统计一次
            var redis = MyRedisHelper.Instance();
            if (await redis.SetContainsAsync($"{Key_Today_Visit_Count}_Set", ip) == false) //keyPerfix+访问者的IP地址+Id为 key，记录这个IP是否点击过
            {
                //说明没有找到
                //使用StringIncrementAsync来进行计数，效率很高
                await redis.StringIncrementAsync($"{Key_Today_Visit_Count}", 1);             
                await redis.SetAddAsync($"{Key_Today_Visit_Count}_Set", ip);
                //设置key有效期
                if(await redis.KeyExistsAsync($"{Key_Today_Visit_Count}_Expire") == false)
                {
                    await redis.KeyExpireAsync($"{Key_Today_Visit_Count}", TimeSpan.FromDays(1));
                    await redis.KeyExpireAsync($"{Key_Today_Visit_Count}_Set", TimeSpan.FromDays(1));
                    await redis.StringSetAsync($"{Key_Today_Visit_Count}_Expire", "true", TimeSpan.FromDays(1));
                }             
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class VisitRecordMiddlewareExtension
    {
        public static IApplicationBuilder UseVisitRecordMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<VisitRecordMiddleware>();
        }
    }
}
