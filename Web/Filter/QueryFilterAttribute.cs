﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Security;
using Microsoft.AspNetCore.Mvc.Controllers;
using IdentityModel;
using System.Security.Claims;
using Model.Entity.Sys;
using Model.Entity.Sys.Enum;
using Model.Entity;

namespace Web.Filter
{
    /// <summary>
    /// 查询过滤器。
    /// (1)为模型添加用户标志,添加修改设置用户标志。
    /// a、对于add和update方法，如实体类继承了IUser并且用户已经登录，则给实体类自动添加UserId值
    /// b、对于add和update方法，如添加或修改的用户角色为管理员（管理员角色以"admin"开头），则给实体类sys值设置为true，表示系统资源，否则为false
    /// (2)用于list列表查询、get单个查询、query、delete、batchdelete删除查询添加查询过滤滤条件
    /// a、加入了某租户，只能查询某租户的数据（默认）
    /// b、非管理员只能查看自己的数据（默认）
    /// c、非管理员（普通会员）不能删除系统数据（默认）
    /// d、非管理员（普通会员）不能获取系统数据（默认）
    /// e、Added_time降序排序列表查询（默认）
    /// 
    /// </summary>
    public class QueryFilterAttribute : ActionFilterAttribute, IActionFilter
    {
        private string addmethods = "add";
        private string[] addupdatemethods = new string[] { "add", "update" };
        private string[] querygetdeletemethods = new string[] { "list", "delete", "batchdelete", "get", "query" };
        private string[] getmethods = new string[] { "get" };
        private string[] deletegetmethods = new string[] { "delete", "batchdelete", "get" };
        private string[] deletemethods = new string[] { "delete", "batchdelete" };
        // public IClaimsAccessor MyUser { get; set; }
        /// <summary>
        /// OnActionExecuting方法在Controller的Action执行前执行
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //循环获取在Controller的Action方法中定义的参数
            var actionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            var actionName = actionDescriptor.ActionName.ToLower();
            //Console.WriteLine(actionName); 
            //获取自定义特性 ，用于判断是否需要给model添加身份标志
            var customAttributes = actionDescriptor.MethodInfo.GetCustomAttributes(typeof(QueryableAttribute), false);
            QueryableAttribute queryableAttribute = customAttributes.Length > 0 ? (QueryableAttribute)customAttributes[0] : null;
            var queryable = queryableAttribute != null && queryableAttribute.Enable;
            //用户id
            var userIdStr = context.HttpContext?.User?.Claims.FirstOrDefault(c => c.Type == JwtClaimTypes.Id)?.Value;
            int userId = 0;
            int.TryParse(userIdStr, out userId);
            //用户角色类型
            var roleType = context.HttpContext?.User?.Claims.FirstOrDefault(c => c.Type == "Type")?.Value;
            //角色
            var role = context.HttpContext?.User?.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;
            //租户编号
            var TenantIdStr = context.HttpContext?.User?.Claims.FirstOrDefault(c => c.Type == "TenantId")?.Value;
            int TenantId = 0;
            int.TryParse(TenantIdStr, out TenantId);
            //如果是增加和修改方法  根据角色类和角色添加 标志id 、添加系统资源标志
            if (addupdatemethods.Any(o => actionName.Contains(o)) || queryable)
            {
                foreach (var parameter in actionDescriptor.Parameters)
                {
                    var parameterName = parameter.Name;//获取Action方法中参数的名字
                    var parameterType = parameter.ParameterType;//获取Action方法中参数的类型                  
                    //自动添加用户id
                    if (typeof(IUser).IsAssignableFrom(parameterType))
                    {
                        var model = context.ActionArguments[parameterName] as IUser;
                        if (userId != 0)
                        {
                            model.UserId = userId;
                        }
                    }
                    //自动添加租户id
                    if (typeof(ITenant).IsAssignableFrom(parameterType))
                    {
                        var model = context.ActionArguments[parameterName] as ITenant;
                        if (TenantId != 0)
                        {
                            model.TenantId = TenantId;
                        }
                    }
                    //如果是ISys类型，添加系统资源标志
                    if (typeof(ISys).IsAssignableFrom(parameterType))
                    {
                        var model = context.ActionArguments[parameterName] as ISys;
                        //if (addmethods == actionName) model.Added_time = DateTime.Now; //增加时间
                        //else model.Added_time = null; //添加时间不能修改
                        model.Modify_time = DateTime.Now;                                //
                        //添加系统资源标志  非系统管理员或者非校区管理员、学生、老师添加的资源为非系统资源。系统管理员、校区管理员添加的资源为系统资源                   
                        if (role.StartsWith("admin"))
                        {
                            model.Sys = true;
                        }
                        else
                        {
                            model.Sys = false;
                        }
                    }

                }
            }
            //查询删除方法 过滤条件查询。只能查自己的，除非管理员
            if (querygetdeletemethods.Any(o => actionName.Contains(o)) || queryable)
            {
                foreach (var parameter in actionDescriptor.Parameters)
                {
                    var parameterName = parameter.Name;//获取Action方法中参数的名字
                    var parameterType = parameter.ParameterType;//获取Action方法中参数的类型
                    //判断该Controller的Action方法是否有类型为Dictionary<string, string>的参数
                    if (typeof(Dictionary<string, string>).IsAssignableFrom(parameterType))
                    {
                        var where = context.ActionArguments[parameterName] as Dictionary<string, string>;
                        //所属租户身份过滤，只能查询所属租户的数据（默认）
                        if (TenantId != 0)
                        {
                            where.Add("TenantId", TenantId.ToString());
                        }
                        //非管理员只能查看自己的数据，管理员查看所有信息（默认）
                        if (userId != 0 && !role.StartsWith("admin"))
                        {
                            where.Add("UserId", userId.ToString());
                        }
                        //删除\查询单个、batchdelete
                        if (deletegetmethods.Any(t => actionName.Contains(t)))
                        {
                            where.Remove("id");
                            //首先从路由参数获取
                            string id = context.RouteData.Values["id"] as string;//id=1单个删除,id=1,2,3,4批量删除
                            //再从查询字符串获取
                            id = id ?? context.HttpContext.Request.Query["id"];
                            //再从表单域获取
                            id = id ?? context.HttpContext.Request.Form["id"];
                            if (id != null)
                            {
                                where.Add("Id__In", id);
                            }
                            //非管理员（普通会员）不能删除系统数据（默认）
                            if (deletemethods.Any(t => actionName.Contains(t)) && !role.StartsWith("admin"))
                            {
                                where.Add("Sys", false.ToString());
                            }
                            //非管理员（普通会员）不能获取系统数据（默认）
                            if (getmethods.Any(t => actionName.Contains(t)) && !role.StartsWith("admin"))
                            {
                                where.Add("Sys", false.ToString());
                            }
                        }
                        else//查询排序
                        {
                            if (!where.ContainsKey("sort")) where.Add("sort", "-Sort,-Added_time");//排序
                        }
                    }
                }
            }
        }

        /// <summary>
        /// OnActionExecuted方法在Controller的Action执行后执行
        /// </summary>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            //TODO
        }
    }
}
