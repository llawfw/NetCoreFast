using Autofac;
using BD;
using Common.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Model;
using Model.Dto;
using Web.Autofac;
using Web.Extension;
using Web.Jwt;
using Web.Redis;
using Web.Util;
using Weixin;
using System.Text.Json;
using Newtonsoft.Json;
using Common.Json.Newton;
using Newtonsoft.Json.Serialization;
using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
//
namespace Web
{
    /// <summary>
    /// 框架参考https://gitee.com/AprilBlank/April.Util.Public/blob/master/April.Simple.WebApi/Startup.cs
    /// windows服务 https://learn.microsoft.com/zh-cn/dotnet/core/extensions/windows-service
    /// 参考 https://gitee.com/laozhangIsPhi/Blog.Core/blob/master/Blog.Core/Startup.cs
    /// https://www.cnblogs.com/laozhang-is-phi/p/9511869.html
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConfigHelper.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        //在ConfigureServices中注册依赖项。在下面的ConfigureContainer方法之前由运行时调用。任何IServiceProvider或ConfigureContainer方法会被调用。
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // 配置Controller全部由Autofac创建。默认情况下，Controller的参数会由容器创建，但Controller的创建是有AspNetCore框架实现的。要通过容器创建Controller，需要在Startup中配置一下：
            services.Replace(ServiceDescriptor.Transient<IControllerActivator, ServiceBasedControllerActivator>());
            //获取身份证
            //services.AddSingleton<IPrincipalAccessor, PrincipalAccessor>();
            //services.AddSingleton<IPrincipalAccessor, PrincipalAccessor>();
            //services.AddSingleton<IClaimsAccessor, ClaimsAccessor>();

            //注册跨域服务
            //services.AddCors(corsOptions =>
            //{
            //    //添加跨域策略
            //    corsOptions.AddPolicy("any", buidler =>
            //    {
            //        //允许所有访问域、允许所有请求头、允许所有的请求方法
            //        // Console.WriteLine(Configuration.GetValue<string>("CorsOrigins").Split(',').Length);
            //        buidler.WithOrigins(Configuration.GetValue<string>("CorsOrigins").Split(',')).AllowAnyHeader().AllowAnyMethod().AllowCredentials();
            //        //buidler.WithOrigins("http://localhost:8080").AllowAnyHeader().AllowAnyMethod().AllowCredentials();
            //    });
            //});
            //asp.net core 3.1 MVC/WebApi JSON 全局配置 
            //https://docs.microsoft.com/zh-cn/aspnet/core/web-api/advanced/formatting?view=aspnetcore-3.1
            //System.Text.Json(default) https://cloud.tencent.com/developer/article/1597403
            //https://blog.csdn.net/sD7O95O/article/details/123725054?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-2-123725054-blog-123798040.pc_relevant_blogantidownloadv1&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-2-123725054-blog-123798040.pc_relevant_blogantidownloadv1&utm_relevant_index=4
            services.AddControllers(options =>
            {
                // options.Filters.Add(new UserModelActionFilterAttribute()); //注册全全局模型过滤器
                //使用自定义模型绑定器提供程序，请将其添加到 ConfigureServices 中
                options.ModelBinderProviders.Insert(0, new QueryModelBinderProvider());
            })
            //用内置的JSON 会出现问题6.0 暂时用NewtonsoftJson
            //https://docs.microsoft.com/zh-cn/aspnet/core/web-api/advanced/formatting?view=aspnetcore-6.0
            /*.AddJsonOptions(options =>
            {
                //格式化日期时间格式
                options.JsonSerializerOptions.Converters.Add(new DatetimeJsonConverter());
                options.JsonSerializerOptions.Converters.Add(new TimeSpanJsonConverter());
                options.JsonSerializerOptions.Converters.Add(new EnumJsonConverter());
                //数据格式首字母小写
                //options.JsonSerializerOptions.PropertyNamingPolicy =JsonNamingPolicy.CamelCase;
                //数据格式原样输出
                //options.JsonSerializerOptions.PropertyNamingPolicy = null;
                //取消Unicode编码
                //options.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
                //忽略空值
                //options.JsonSerializerOptions.IgnoreNullValues = true;              
                //允许额外符号
                //options.JsonSerializerOptions.AllowTrailingCommas = true;
                //反序列化过程中属性名称是否使用不区分大小写的比较
                //options.JsonSerializerOptions.PropertyNameCaseInsensitive = false;
            })*/.AddNewtonsoftJson(options =>
            {
                //设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                //忽略循环引用
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.Converters.Add(new DatetimeNewtonJsonConverter());
                options.SerializerSettings.Converters.Add(new TimeSpanNewtonJsonConverter());
                options.SerializerSettings.Converters.Add(new EnumNewtonJsonConverter());
                //数据格式首字母小写
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                //数据格式按原样输出
                //options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                //忽略空值
                //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            //注册上下文对象
            //services.AddDbContext<MyDbContext>(options =>
            //{
            //    options.UseSqlServer(Configuration.GetConnectionString("SqlServer"));
            //    //options.EnableSensitiveDataLogging();
            //});//ServiceLifetime.Transient   https://www.cnblogs.com/yaopengfei/p/11349644.html   https://docs.microsoft.com/zh-cn/ef/core/miscellaneous/configuring-dbcontext
            //使用数据池提高性能https://docs.microsoft.com/zh-cn/ef/core/what-is-new/ef-core-2.0/#explicitly-compiled-queries
            var maxPoolSize = Configuration.GetConnectionString("maxPoolSize");
            int poolSize = 1024;
            if (!string.IsNullOrEmpty(maxPoolSize))
            {
                poolSize = Convert.ToInt32(maxPoolSize);
            }
            services.AddDbContextPool<MyDbContext>(options =>
            {
                //支持sqlserver和mysql数据库。
                var sqlserverconstr = Configuration.GetConnectionString("SqlServer");
                var mysqlconstr = Configuration.GetConnectionString("MySql");
                var sqlite = Configuration.GetConnectionString("Sqlite");
                //Console.WriteLine($"数据库：{sqlserverconstr}");
                if (!string.IsNullOrEmpty(sqlserverconstr))
                {
                    options.UseSqlServer(sqlserverconstr);
                    options.UseBatchEF_MSSQL();//MySQL 用户用这个https://github.com/yangzhongke/Zack.EFCore.Batch
                }
                else if (!string.IsNullOrEmpty(mysqlconstr))
                {
                    //mysql驱动：MySql.EntityFrameworkCore。更多驱动https://docs.microsoft.com/zh-cn/ef/core/providers/?tabs=dotnet-core-cli
                    //mysql连接字符串ssl问题： https://blog.csdn.net/qq_41165844/article/details/80372160?utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control
                    //https://docs.microsoft.com/zh-cn/ef/core/providers/?tabs=dotnet-core-cli
                    //https://segmentfault.com/a/1190000039821570
                    //options.UseMySQL(mysqlconstr);
                    //https://github.com/PomeloFoundation/Pomelo.EntityFrameworkCore.MySql
                    options.UseMySql(mysqlconstr, ServerVersion.AutoDetect(mysqlconstr));                    
                   // builder.EnableRetryOnFailure(15, TimeSpan.FromSeconds(30), null);
                    // The following three options help with debugging, but should                                                                                       
                    // be changed or removed for production.
                    //.LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().EnableDetailedErrors();
                    options.UseBatchEF_MySQLPomelo();//MySQL 用户用这个https://github.com/yangzhongke/Zack.EFCore.Batch
                }
                else if (!string.IsNullOrEmpty(sqlite))
                {
                    //https://www.cnblogs.com/whuanle/p/10144101.html
                    options.UseSqlite(sqlite);
                    options.UseBatchEF_Sqlite();//MySQL 用户用这个https://github.com/yangzhongke/Zack.EFCore.Batch
                }
                //开启EF Core的延迟加载功能  使用延迟加载的最简单方式是安装 Microsoft.EntityFrameworkCore.Proxies 包，并通过调用 UseLazyLoadingProxies 来启用。
                //序列化最好关闭延迟加载，且延迟加载与无跟踪在一起使用的时候就不能加载导航属性了。  https://learn.microsoft.com/zh-cn/ef/ef6/querying/related-data
                //options.UseLazyLoadingProxies();
                // options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
                //options.EnableSensitiveDataLogging();
                // 配置标准日志，执行EFCore时会在控制台打印对应的SQL语句
                //ILoggerFactory myLoggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
                //options.UseLoggerFactory(myLoggerFactory);
            }, poolSize);

            //将 Swagger 生成器添加到 Startup.ConfigureServices 方法中的服务集合中： Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerService(Configuration);

            //将appsettings.json中的JwtSettings部分文件读取到JwtSettings中，这是给其他地方用的
            services.Configure<JwtConfig>(Configuration.GetSection("JwtConfig"));

            //由于初始化的时候我们就需要用，所以使用Bind的方式读取配置
            //将配置绑定到JwtConfig实例中
            var jwtConfig = new JwtConfig();
            Configuration.Bind("JwtConfig", jwtConfig);
            //配置认证授权
            services.ConfigureJwt(jwtConfig);

            //初始化redis
            services.InitRedisConnect(Configuration);
            //RedisClient.redisClient.InitConnect(Configuration);

            //注册微信配置
            services.Configure<WxConfig>(Configuration.GetSection("Weixin"));
            //注册百度配置 
            services.Configure<BDConfig>(Configuration.GetSection("Baidu"));
            //添加redis  https://docs.microsoft.com/zh-cn/aspnet/core/performance/caching/distributed?view=aspnetcore-3.1
            //分布式缓存          
            if (string.IsNullOrWhiteSpace(RedisConnectionHelp.RedisConnectionString))
            {
                //内存缓存 开发和测试
                services.AddDistributedMemoryCache();
            }
            else
            {
                //非开发环境中的
                services.AddStackExchangeRedisCache(options =>
                {
                    options.Configuration = RedisConnectionHelp.RedisConnectionString;
                    options.InstanceName = RedisConnectionHelp.SysCustomKey;//InstanceName 是自定义的实例名称，创建缓存时会以此名称开头。
                });
            }
            //将HttpClient注入IOC容器
            services.AddHttpClient();
            //启用服务定时器
            services.UseQuartz();
            //注册响应式缓存https://www.pianshen.com/article/24171627661/
            services.AddResponseCaching(options =>
            {
                options.UseCaseSensitivePaths = false;//确定是否将响应缓存在区分大小写的路径上。 默认值是 false。
                options.MaximumBodySize = 1024;//响应正文的最大可缓存大小（以字节为单位）。 默认值为 64 * 1024 * 1024 （64 MB）。
                options.SizeLimit = 100 * 1024 * 1024;//响应缓存中间件的大小限制（以字节为单位）。 默认值为 100 * 1024 * 1024 （100 MB）。
            });
            //https://zhuanlan.zhihu.com/p/89550593
            //https://www.cnblogs.com/mq0036/p/12409047.html
            //https://blog.csdn.net/wtf123654789/article/details/90445513?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-1.pc_relevant_paycolumn_v2&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-1.pc_relevant_paycolumn_v2&utm_relevant_index=1
            //var config = new MapperConfiguration(e => e.AddProfile(new AutoMapperProfile()));
            //services.AddSingleton(config);
            services.AddAutoMapper(typeof(AutoMapperProfile).Assembly);//添加自动映射支持
            services.AddHostedService<LifetimeEventsHostedService>();//添加应用生命周期服务
        }

        //配置autofac依赖注入
        //您可以在ConfigureContainer中直接注册内容使用Autofac。 这在ConfigureServices之后运行，这里将覆盖在ConfigureServices中进行的注册。    
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.ConfigureAutofac();
        }
        //添加中间件的位置。
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(WebApplication app, IWebHostEnvironment env)
        {
            ConfigHelper.WebApplication = app;
            ConfigHelper.ServiceProvider = app.Services;
            //https://docs.microsoft.com/zh-cn/aspnet/core/migration/50-to-60?view=aspnetcore-6.0&tabs=visual-studio#smhm
            /*net6.0不需要
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }*/
            // using Microsoft.AspNetCore.HttpOverrides;
            //https://docs.microsoft.com/zh-cn/aspnet/core/host-and-deploy/linux-nginx?view=aspnetcore-2.2
            //app.UseForwardedHeaders(new ForwardedHeadersOptions
            //{
            //    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            //});
            //在 Startup.Configure 方法中，启用中间件为生成的 JSON 文档和 Swagger UI 提供服务：
            app.UseSwaggerService();
            //app.UseCors("any");//启动跨域请求
            //app.UseOptions(); // 启动跨域请求  预检测         
            app.UseCorsMiddleware();//自定义跨域
            // 跳转https
            //app.UseHttpsRedirection();

            //DefaultFilesOptions options = new DefaultFilesOptions();
            //options.DefaultFileNames.Add("index2.html");    //将index.html改为需要默认起始页的文件名.
            //app.UseDefaultFiles(options); 
            // 使用静态文件
            app.UseStaticFiles();//用于访问wwwroot下的文件
            //net6.0不需要
            //https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/first-web-api?view=aspnetcore-6.0&tabs=visual-studio
            //app.UseRouting();
            // 先启动认证中间件
            app.UseAuthentication();
            // 然后是授权中间件
            app.UseAuthorization();//启用授权中间件  必须放在   app.UseRouting(); 和 app.UseEndpoints之间      
            // 自定义授权，放在 app.UseAuthorization()之后 主要用于PrincipalAccessor 
            app.UseMiddleware<JwtAuthorizationMiddleware>();
            app.UseVisitRecordMiddleware();//操作日志中间件 
            app.MapControllers();
            /* //net6.0不需要
             app.UseEndpoints(endpoints =>
             {
                 endpoints.MapControllers();
             });*/
            #region IM服务端配置
            app.UseImServer(new ImServerOptions
            {
                Redis = new CSRedis.CSRedisClient(Configuration["ImServerOption:CSRedisClient"]),
                Servers = Configuration["ImServerOption:Servers"].Split(";"),
                Server = Configuration["ImServerOption:Server"],
                PathMatch = "/wss"
            });
            #endregion
            #region IM客户端配置，WebApi业务端
            ImHelper.Initialization(new ImClientOptions
            {
                Redis = new CSRedis.CSRedisClient(Configuration["ImClientOption:CSRedisClient"]),
                Servers = Configuration["ImServerOption:Servers"].Split(";"),
                PathMatch = "/wss"
            });

            ImHelper.Instance.OnSend += (s, e) =>
            {
                // Console.WriteLine($"ImClient.SendMessage(server={e.Server},data={JsonSerializer.Serialize(e.Message)})");
            };

            ImHelper.EventBus(
                t =>
                {
                    //Console.WriteLine(t.clientId + "上线了");
                    var onlineUids = ImHelper.GetClientListByOnline();
                    //$"用户{t.clientId}上线了"
                    ImHelper.SendMessage(t.clientId, onlineUids, System.Text.Json.JsonSerializer.Serialize(ImMsg.Online(null, new Sender() { ClientId = t.clientId }), DefaultJsonOptions.Get()));
                },
                t =>
                {
                    //Console.WriteLine(t.clientId + "下线了");
                    var onlineUids = ImHelper.GetClientListByOnline();
                    ImHelper.SendMessage(t.clientId, onlineUids, System.Text.Json.JsonSerializer.Serialize(ImMsg.Offline(null, new Sender() { ClientId = t.clientId }), DefaultJsonOptions.Get()));
                });
            ImHelper.EventChan(
               t =>
               {
                   var onlineUids = ImHelper.GetChanClientList(t.chan);
                   //Console.WriteLine(t.clientId + "加入频道" + t.chan + "在线列表" + onlineUids.Length + "人数" + ImHelper.GetChanOnline(t.chan));
                   //Console.WriteLine(t.clientId + "加入频道" + t.chan + "在线人数" + onlineUids.Length);
                   if (onlineUids.Length > 0)
                   {
                       ImHelper.SendMessage(t.clientId, onlineUids, System.Text.Json.JsonSerializer.Serialize(ImMsg.Hello(t.chan, onlineUids.Length, new Sender() { ClientId = t.clientId }), DefaultJsonOptions.Get()));
                   }
               },
               t =>
               {
                   var onlineUids = ImHelper.GetChanClientList(t.chan);
                   // Console.WriteLine(t.clientId + "离开频道" + t.chan + "在线列表" + onlineUids.Length + "人数" + ImHelper.GetChanOnline(t.chan)) ;
                   //Console.WriteLine(t.clientId + "离开频道" + t.chan + "在线人数" + onlineUids.Length);
                   if (onlineUids.Length > 0)
                   {
                       ImHelper.SendMessage(t.clientId, onlineUids, System.Text.Json.JsonSerializer.Serialize(ImMsg.Bye(t.chan, onlineUids.Length, new Sender() { ClientId = t.clientId }), DefaultJsonOptions.Get()));
                   }
               });
            #endregion
        }
    }
}
