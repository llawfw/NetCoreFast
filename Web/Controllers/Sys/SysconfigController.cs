/* author:QinYongcheng */


using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;  
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Sys;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Web.Filter;
using Model.Entity.Sys;
using Model.Dto.Sys;
using Web.Security;

namespace Web.Controllers.Sys  
{
    /// <summary>
    /// 系统配置
    /// <p>模块名称:<b>Sys</b></p>
    /// <p>实体类名:<b>Sysconfig</b></p>
    /// </summary>
    [ApiController]
    [Route("api/Sys/[controller]/[action]")]
    [Authority(Module ="Sys")]
    public class SysconfigController : MyApiBaseController<Sysconfig, SysconfigListDto, SysconfigAddDto, SysconfigUpdateDto>
    {
        
        
        public SysconfigController(ISysconfigBll bll) : base(bll)
        {
        }
       
       
        
       
        
        
    }
}
