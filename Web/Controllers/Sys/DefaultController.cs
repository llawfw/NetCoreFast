﻿using System;
using Microsoft.AspNetCore.Mvc;
using BLL.Sys;
using Microsoft.AspNetCore.Hosting;
using Web.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Weixin;
using System.Net.Http;
using Common;
using Model.Entity.Sys;
using Web.Security;
using Web.Redis;
using Model.Entity;
using Web.Extension;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Web.Controllers.Job;
using Web.Extension;
namespace Web.Controllers.Sys
{
    /// <summary>
    ///系统常用接口
    /// </summary>
    [Route("api/sys/")]
    [ApiController]
    public class DefaultController : MyBaseController<Object>
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        public ISysUserBll userBll { get; set; }//通过属性依赖注入
        public IDistributedCache cache { get; set; }
        private IConfiguration configuration { get; set; }
        private IOptions<WxConfig> wxConfig;
        public DefaultController(IWebHostEnvironment webHostEnvironment, IConfiguration configuration)
        {
            this.webHostEnvironment = webHostEnvironment;
            this.configuration = configuration;
        }
        [HttpGet("statistics")]
        public async Task<Result<object>> StatisticsAsync()
        {
            var redis = MyRedisHelper.Instance();
            string today_Visit_Count = await redis.StringGetAsync(VisitRecordMiddleware.Key_Today_Visit_Count);
            var total_Visit_Count = await cache.GetAsync<long?>(StatisticsJob.Key_Total_Visit_Count);
            return Result<object>.Success("查询成功").SetData(new { totalVisitCount = total_Visit_Count + Convert.ToInt32(today_Visit_Count), todayVisitCount = today_Visit_Count });
        }
    }
}

