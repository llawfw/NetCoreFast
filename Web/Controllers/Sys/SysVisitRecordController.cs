/* author:QinYongcheng */


using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;  
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Sys;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Web.Filter;
using Model.Entity.Sys;
using Model.Dto.Sys;
using Web.Security;

namespace Web.Controllers.Sys  
{
    /// <summary>
    /// 访问记录
    /// <p>模块名称:<b>Sys</b></p>
    /// <p>实体类名:<b>SysVisitRecord</b></p>
    /// </summary>
    [ApiController]
    [Route("api/Sys/[controller]/[action]")]
    [Authority(Module ="Sys")]
    public class SysVisitRecordController : MyEmptyApiBaseController<SysVisitRecord, SysVisitRecordListDto, SysVisitRecordAddDto, SysVisitRecordUpdateDto>
    {
        
        
        public SysVisitRecordController(ISysVisitRecordBll bll) : base(bll)
        {
        }
       
       
        /// <summary>
        /// 分页列表查询
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**Sys_SysVisitRecord_list**</br> 
        /// <br>查询条件格式：api/SysVisitRecord/List?sort=-Id&name=武汉&pageNo=1 </br>
        /// <br>sort：排序，值的格式为：+|-字段名,+|-字段名。如-Id根据Id字段递减排序；+Id根据Id字段递增排序。可多个字段排序：如+Id,-Added_time。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
        /// <br>pageNo：查询页码；<=0则查询所有，默认1。</br>
        /// <br>pageSize：每页显示数量；默认12。</br>
        /// <br>条件查询，格式：字段名__操作符=值。如"parent.id__eq=1"、 ParentId=null如果不加操作符,默认是等于eq 即如：parent.id=1与parent.id__eq=1等价。注意：字段名必须是实体类里面的属性名，大小写保存一致</br>
        /// <br>条件查询操作符有：contains包含,eq等于,ne不等于,gt大于,ge大于等于,lt小于, le小于等于,in包含。如果是in查询，多个值之间","分隔，比如“id__in=1,2,3”</br>
        /// </remarks>
        /// <param name="where">此参数用于根据查询字符串自动组装查询条件字符串参数，无须人工干预传值</param>
        /// <param name="excludes">排除要查询那些字段(可选)，比如提高查询效率，排除导航属性（即对象属性）</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<Result<Pagination<SysVisitRecordListDto>>> List([FromQuery] Dictionary<string, string> where, String excludes = null, CancellationToken token = default)
        {
            // mapper.Map<TListDto>(await bll.QueryAsync(where, excludes));
            return Result<Pagination<SysVisitRecordListDto>>.Success("succeed").SetData(await bll.QueryToAsync<SysVisitRecordListDto>(where, excludes, token));
        }
        
        
       
        
        
        /// <summary>
        /// 删除实体对象
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**Sys_SysVisitRecord_delete**</br> 
        /// <br>（1）根据id单个删除，如：/api/SysVisitRecord/Delete/id值或/api/SysVisitRecord/Delete?id=1</br>
        /// <br>（2）根据id批量删除，如：/api/SysVisitRecord/Delete/1,2,3 或/apiSysVisitRecord/Delete?id = 1,2,3  多个id值用“,”隔开</br>
        /// <br>（3）条件批量删除，如：/api/SysVisitRecord/Delete?name=武汉  ，条件格式与list查询的条件一致</br>
        /// </remarks> 
        /// <param name="id">id值，单个值如：1，多个值如：1,2,3</param>
        /// <param name="where">查询其他条件，条件格式与list查询的条件一致。此参数用于根据查询字符串自动组装查询条件字符串参数，无须人工干预传值</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public virtual async Task<Result<object>> Delete(string id, [FromQuery] Dictionary<string, string> where, CancellationToken token = default)
        {
            return await bll.DeleteAsync(where, token) ? Result<object>.Success("删除成功") : Result<object>.Error("删除失败");
        }
        /// <summary>
        /// 批量删除实体对象
        /// </summary>
        /// <remarks>
        /// <br>本接口权限资源值value：**Sys_SysVisitRecord_batchdelete**</br> 
        /// <br>如：api/SysVisitRecord/BatchDelete?name=武汉 </br>
        /// <br>注：条件格式与list查询的条件一致</br>
        /// </remarks>
        /// <param name="where">查询其他条件，条件格式与list查询的条件一致。此参数用于根据查询字符串自动组装查询条件字符串参数，无须人工干预传值</param>
        /// <param name="token">异步任务取消令牌</param>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<Result<object>> BatchDelete([FromQuery] Dictionary<string, string> where, CancellationToken token = default)
        {
            return await bll.DeleteAsync(where, token) ? Result<object>.Success("删除成功") : Result<object>.Error("删除失败");
        }
        
    }
}
