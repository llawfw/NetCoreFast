﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BD;
using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Web.Oauth2;
using Web.Redis;
using Web.Util;
using Microsoft.AspNetCore.Http.Extensions;
using BLL.Sys;
using Model.Entity.Sys;

namespace Web.Controllers
{
    /// <summary>
    /// 百度网盘接口
    /// </summary>
    /// <see cref="https://pan.baidu.com/union/doc/3ksg0s9r7"/>
    [Route("api/[controller]")]
    [Controller]
    public class BaidupanController :MyBaseController<Object>
    {
        private IOptions<BDConfig> bdConfig;
        private MyRedisHelper redis; 
        public ISysconfigBll sysconfigBll { get; set; }
        //百度账号信息   
        public BaidupanController(IOptions<BDConfig> bdConfig)
        {
            this.bdConfig = bdConfig;
            this.redis = MyRedisHelper.Instance();
            this.redis.SetSysCustomKey("baidupan");//设置前缀
        }
        /// <summary>
        /// 获取百度网盘授权地址
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAuthorizeUrl")]
        public Result<object> GetAuthorizeUrl()
        {
            OauthConnector c = OauthConnectorFactory.Build("baidu", bdConfig.Value.PanAppKey, bdConfig.Value.PanSecretKey);
            var uuid = Guid.NewGuid().ToString();
            redis.StringSet("session_baidupan_oauth_state", uuid, TimeSpan.FromSeconds(60));
            //string url = c.GetAuthorizeUrl(uuid, Request.Scheme+"://"+Request.Host.Host+ "/api/baidupan/bind");
            string url = c.GetAuthorizeUrl(uuid, "https://" + Request.Host.Host + "/api/baidupan/bind");
            return Result<object>.Success().SetData(new { url = url });
        } 
        /// <summary>
        /// 百度网盘回调地址
        /// </summary>
        /// <param name="state"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("Bind")]
        public Task Bind(String state, String code)
        {
            var uuid = redis.StringGet("session_baidupan_oauth_state");
            if(String.IsNullOrEmpty(uuid) || uuid != state)
            {
                return ReturnHtml("参数state不正确");
            }
            if (String.IsNullOrEmpty(code) )
            {
                return ReturnHtml("参数code不存在");
            }
            if (redis.KeyExists("session_baidupan_access_token"))
            {
                return ReturnHtml("已授权！请勿重复授权操作");
            }
            OauthConnector c = OauthConnectorFactory.Build("baidu", bdConfig.Value.PanAppKey, bdConfig.Value.PanSecretKey);
            AccessToken a = c.GetAccessToken(code);
            if (!a.IsSuccess())
            {
                return ReturnHtml($"授权失败！错误码：{a.errno},错误消息{a.errmsg}");
            }

            Sysconfig sysconfig = sysconfigBll.SelectOneOrInit();
            sysconfig.Baidupan_refresh_token = a.refresh_token;
            sysconfigBll.Update(sysconfig);
            redis.StringSet("session_baidupan_access_token", a.access_token, TimeSpan.FromSeconds(a.expires_in));
            redis.StringSet("session_baidupan_refresh_token", a.refresh_token);
            redis.StringSet("session_baidupan_expires_in", a.expires_in, TimeSpan.FromSeconds(a.expires_in));
            return ReturnHtml("授权成功！<script>setTimeout(function(){window.close();}, 3000)</script>");
        }
        [NonAction]
        public async Task ReturnHtml(string html)
        {
            StringBuilder htmlStringBuilder = new StringBuilder();
            htmlStringBuilder.Append("<html>");
            htmlStringBuilder.Append("<head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'/></head>");//支持中文
            htmlStringBuilder.Append("<body>");
            htmlStringBuilder.Append("<span style='font-size:200%'>");//让字体变大
            htmlStringBuilder.Append(html);
            htmlStringBuilder.Append("</span>");
            htmlStringBuilder.Append("</body>");
            htmlStringBuilder.Append("</html>");
            var result = htmlStringBuilder.ToString();
            var data = Encoding.UTF8.GetBytes(result);            
            Response.ContentType = "text/html";                      
            await Response.Body.WriteAsync(data, 0, data.Length);
        }
    }
}