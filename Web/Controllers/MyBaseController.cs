﻿using AutoMapper;
using BLL;
using IdentityModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Web.Security;

namespace Web.Controllers
{
    /// <summary>
    /// 公共的属性、方法、变量
    /// </summary>
    //[Controller]
    public abstract class MyBaseController<E> : ControllerBase
    {      
        /// <summary>
        /// 注入身份信息
        /// </summary>
        public IClaimsAccessor MyUser { get; set; }
        public IDistributedCache Cache { get; set; }
        public MyBaseController() { }
    }
}