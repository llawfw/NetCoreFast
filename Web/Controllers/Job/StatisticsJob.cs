﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Extension;
using Web.Security;
using Web.Util;
using Web.Redis;
using BLL.Sys;
using Microsoft.Extensions.Caching.Distributed;

namespace Web.Controllers.Job
{
    /// <summary>
    /// 统计定时任务
    /// </summary>
    [DisallowConcurrentExecution]//DisallowConcurrentExecution, 该属性可防止Quartz.NET尝试同时运行同一作业
    public class StatisticsJob : IJob
    {
        public readonly static string Key_Total_Visit_Count = "Total_Visit_Count";
        public IClaimsAccessor MyUser { get; set; }
        public ISysconfigBll sysconfigBll { get; set; }
        public IDistributedCache cache { get; set; }
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(async () =>
            {
                //var timetable_sn = context.JobDetail.JobDataMap.GetString("timetable_sn");
                //1 今日访问量保存到数据库中 
                var redis = MyRedisHelper.Instance();
                string today_Visit_Count = await redis.StringGetAsync(VisitRecordMiddleware.Key_Today_Visit_Count);
                var sysconfig = sysconfigBll.SelectOneOrInit();
                sysconfig.Visit_count = sysconfig.Visit_count + Convert.ToInt32(today_Visit_Count);
                await sysconfigBll.UpdateAsync(sysconfig);
                //2 同步到缓存
                await cache.SetAsync(Key_Total_Visit_Count, sysconfig.Visit_count);
                //3 清空今日访问量
                await redis.KeyDeleteAsync($"{VisitRecordMiddleware.Key_Today_Visit_Count}");
                await redis.KeyDeleteAsync($"{VisitRecordMiddleware.Key_Today_Visit_Count}_Set");
                await redis.KeyDeleteAsync($"{VisitRecordMiddleware.Key_Today_Visit_Count}_Expire");
            });
        }
    }
    public class StatisticsJobProvider : IJobProvider
    {
        private List<JobSchedule> jobs = new List<JobSchedule>();
        public List<JobSchedule> Jobs()
        {
            return jobs;
        }
        public void Init()
        {
            //开始时间 每天23时49分
            var start_time = new TimeSpan(23, 49, 0);
            var start_h = start_time.Hours;
            var start_m = start_time.Minutes;
            var cron = $"0 {start_m} {start_h} * * ?";
            //结束时间
            //var end_time = obj.From_time + new TimeSpan(0, 10, 0);
            //添加定时任务
            Dictionary<string, object> data = new Dictionary<string, object>();
            //data.Add("expire", 30 * 60);
            NLogHelper.logger.Info($"发布定时任务:开始{start_h}:{start_m},cors表达式:{cron}");
            //Console.WriteLine($"发布定时任务:开始{start_h}:{start_m},cors表达式:{cron}");
            this.jobs.Add(new JobSchedule(name: "StatisticsJob", jobType: typeof(StatisticsJob), cronExpression: cron, data: data));
        }
    }
}
