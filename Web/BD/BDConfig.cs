﻿/* author:QinYongcheng */
using Common.MyAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BD
{

    /// <summary>
    /// 百度配置
    /// 申请 https://console.bce.baidu.com/
    /// https://ai.baidu.com/ai-doc/SPEECH/sk4o0bnzp
    /// </summary>
    [NotAutofac]
    public class BDConfig
    {
        public string AppID { get; set; }
        public string APIKey { get; set; }
        public string SecretKey { get; set; }
        /// <summary>
        /// 百度网盘AppKey
        /// </summary>
        public string PanAppKey { get; set; }
        /// <summary>
        /// 百度网盘SecretKey
        /// </summary>
        public string PanSecretKey { get; set; }
    }
}
