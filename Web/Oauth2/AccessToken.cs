﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Oauth2
{
    public class AccessToken : ResultMsg
    {
        public AccessToken(string access_token)
        {
            this.access_token = access_token;
        }

        public string access_token { get ; set ; }
        public string refresh_token { get ; set ; }
        public string scope { get ; set; }
        public int expires_in { get ; set ; }
        /// <summary>
        /// 微信需要
        /// </summary>
        public string openid { get; set; }
        
    }
}
