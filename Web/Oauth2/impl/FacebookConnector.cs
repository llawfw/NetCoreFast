﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Oauth2.impl
{
    public class FacebookConnector : OauthConnector
    {
        public FacebookConnector(String appkey, String appSecret) : base(appkey, appSecret)
        {
            Type = OauthType.facebook;
        }

        protected override string CreateAuthorizeUrl(string state)
        {
            throw new NotImplementedException();
        }

        public override AccessToken  GetAccessToken(string code)
        {
            throw new NotImplementedException();
        }

        protected override OauthUser GetOauthUser(string code)
        {
            throw new NotImplementedException();
        }

        public override AccessToken RefreshAccessToken(string refresh_token)
        {
            throw new NotImplementedException();
        }
    }
}
