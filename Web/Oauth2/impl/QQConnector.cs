﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Oauth2.impl
{
    public class QQConnector : OauthConnector
    {
        public QQConnector(String appkey, String appSecret) : base(appkey, appSecret)
        {
            Type = OauthType.qq;
        }

        protected override String CreateAuthorizeUrl(String state)
        {
            StringBuilder sb = new StringBuilder("https://graph.qq.com/oauth2.0/authorize?");
            sb.Append("response_type=code");
            sb.Append("&client_id=" + ClientId);
            sb.Append("&redirect_uri=" + RedirectUri);
            sb.Append("&state=" + state);

            return sb.ToString();
        }

        protected override OauthUser GetOauthUser(String code)
        {
            String accessToken = GetAccessToken(code).access_token;
            String openId = OpenId(accessToken, code);

            StringBuilder sb = new StringBuilder("https://graph.qq.com/user/_user_info?");
            sb.Append("access_token=" + accessToken);
            sb.Append("&oauth_consumer_key=" + ClientId);
            sb.Append("&openid=" + openId);
            sb.Append("&format=format");

            String httpString = HttpGet(sb.ToString());
            if (String.IsNullOrEmpty(httpString))
            {
                return null;
            }
            var jObject = JObject.Parse(httpString);
            OauthUser user = new OauthUser();

            user.Avatar= jObject["figureurl_2"].ToString();
            user.Nickname= jObject["nickname"].ToString();
            user.OpenId= jObject["openid"].ToString();
            user.Source = Type.ToString();

            return user;
        }
        protected String OpenId(String accessToken, String code)
        {

            StringBuilder sb = new StringBuilder("https://graph.qq.com/oauth2.0/me?");
            sb.Append("access_token=" + accessToken);

            String httpString = HttpGet(sb.ToString());
            // callback(
            // {"client_id":"10***65","openid":"F8D32108D*****D"}
            // );

            return httpString.Substring(httpString.IndexOf(":") + 2, httpString.IndexOf(",") - 1);
        }
        public override AccessToken GetAccessToken(String code)
        {

            StringBuilder sb = new StringBuilder("https://graph.qq.com/oauth2.0/token?");
            sb.Append("grant_type=authorization_code");
            sb.Append("&code=" + code);
            sb.Append("&client_id=" + ClientId);
            sb.Append("&client_secret=" + ClientSecret);
            sb.Append("&redirect_uri=" + RedirectUri);

            String httpString = HttpGet(sb.ToString());
            // access_token=2D6FE76*****24AB&expires_in=7776000&refresh_token=7CD56****218

            return new AccessToken( httpString.Substring(httpString.IndexOf("=") + 1, httpString.IndexOf("&")));
        }

        public override AccessToken RefreshAccessToken(string refresh_token)
        {
            throw new NotImplementedException();
        }
    }
}
