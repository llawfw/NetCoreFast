﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Oauth2.impl
{
    public class LinkedinConnector : OauthConnector
    {
        public LinkedinConnector(String appkey, String appSecret) : base(appkey, appSecret)
        {
            Type = OauthType.linkedin;
        }

        protected override String CreateAuthorizeUrl(String state)
        {
            return null;
        }

        protected override OauthUser GetOauthUser(String code)
        {
            return null;
        }

        public override AccessToken GetAccessToken(String code)
        {

            return null;
        }

        public override AccessToken RefreshAccessToken(string refresh_token)
        {
            throw new NotImplementedException();
        }
    }
}
