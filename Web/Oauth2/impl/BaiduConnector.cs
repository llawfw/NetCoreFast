﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Oauth2.impl
{
    /// <summary>
    /// https://pan.baidu.com/union/doc/0ksg0sbig
    /// </summary>
    public class BaiduConnector : OauthConnector
    {
        public BaiduConnector(String appkey, String appSecret) : base(appkey, appSecret)
        {
            Type = OauthType.baidu;
        }
        //http://openapi.baidu.com/oauth/2.0/authorize?
        //response_type=code&
        //client_id=YOUR_CLIENT_ID
        //&redirect_uri=YOUR_REGISTERED_REDIRECT_URI
        //&scope=basic,netdisk&display=tv&qrcode=1&force_login=1&device_id=820921428tp8x63q51
        protected override String CreateAuthorizeUrl(String state)
        { 
            StringBuilder sb = new StringBuilder("http://openapi.baidu.com/oauth/2.0/authorize?");
            sb.Append("response_type=code");
            sb.Append("&client_id=" + ClientId);
            sb.Append("&redirect_uri=" + RedirectUri);
            sb.Append("&scope=basic,netdisk");
            sb.Append("&display=popup");
            sb.Append("&qrcode=1");
            sb.Append("&force_login=0");
            sb.Append("&state="+state);
            return sb.ToString();
        }
        //https://openapi.baidu.com/oauth/2.0/token?
        //grant_type=authorization_code
        //&code=CODE
        //&client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET
        //&redirect_uri=YOUR_REGISTERED_REDIRECT_URI
        public override AccessToken GetAccessToken(String code)
        {

            StringBuilder urlBuilder = new StringBuilder("https://openapi.baidu.com/oauth/2.0/token?");
            urlBuilder.Append("grant_type=authorization_code");
            urlBuilder.Append("&code=" + code);
            urlBuilder.Append("&client_id=" + ClientId);
            urlBuilder.Append("&client_secret=" + ClientSecret);
            urlBuilder.Append("&redirect_uri=" + RedirectUri);

            String url = urlBuilder.ToString();

            String httpString = HttpGet(url);
            //var jObject = JObject.Parse(httpString);
            //return jObject["access_token"].ToString();
            return JsonConvert.DeserializeObject<AccessToken>(httpString);
        }
        //https://pan.baidu.com/rest/2.0/xpan/nas?
        //access_token=12.a6b7dbd428f731035f771b8d15063f61.86400.1292922000-2346678-124328
        //&method=uinfo
        protected override OauthUser GetOauthUser(String code)
        {
            String accessToken = GetAccessToken(code).access_token;
            String url = "https://pan.baidu.com/rest/2.0/xpan/nas?method=uinfo&access_token=" + accessToken;

            String httpString = HttpGet(url);
            if (String.IsNullOrEmpty(httpString))
            {
                return null;
            }
            //var jObject = JsonConvert.DeserializeObject(httpString);
            var jObject = JObject.Parse(httpString);
            OauthUser user = new OauthUser();
            user.Avatar = jObject["avatar_url"].ToString();
            user.OpenId = jObject["uk"].ToString();
            user.Nickname = jObject["netdisk_name"].ToString();
            user.Source = Type.ToString();
            user.errmsg = jObject["errmsg"].ToString();
            user.errno = jObject["errno"].ToString();
            return user;
        }

        public override AccessToken RefreshAccessToken(string refresh_token)
        {
            //https://openapi.baidu.com/oauth/2.0/token?
            //grant_type=refresh_token&refresh_token=REFRESH_TOKEN&client_id=API_KEY&client_secret=SECRET_KEY 
            StringBuilder urlBuilder = new StringBuilder("https://openapi.baidu.com/oauth/2.0/token?");
            urlBuilder.Append("grant_type=refresh_token");
            urlBuilder.Append("&refresh_token=" + refresh_token);
            urlBuilder.Append("&client_id=" + ClientId);
            urlBuilder.Append("&client_secret=" + ClientSecret);

            String url = urlBuilder.ToString();

            String httpString = HttpGet(url);
            //var jObject = JObject.Parse(httpString);
            //return jObject["access_token"].ToString();
            return JsonConvert.DeserializeObject<AccessToken>(httpString);
        }
    }
}
