﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Oauth2
{
    public class ResultMsg
    {
        #region 百度网盘
        public string errmsg { get; set; }
        /// <summary>
        /// 0请求成功,否则请求错误
        /// <seealso cref="https://pan.baidu.com/union/doc/okumlx17r"/>
        /// </summary>
        public string errno { get; set; }
        #endregion
        /// <summary>
        /// 是否请求成功
        /// </summary>
        /// <returns></returns>
        public bool IsSuccess()
        {
            return string.IsNullOrEmpty(errno)||this.errno == "0";
        }
    }
}
