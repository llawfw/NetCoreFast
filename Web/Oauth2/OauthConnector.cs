﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Util;

namespace Web.Oauth2
{
    /// <summary>
    /// 第一步，构建跳转的URL，跳转后用户登录成功，返回到callback url，并带上code
    /// </p>
    /// 第二步，通过code，获取access token
    /// </p>
    ///第三步，通过 access token 获取用户的open_id
    /// </p>
    /// 第四步，通过 open_id 获取用户信息
    ///http://git.oschina.net/ooo9999/kfinal
    /// </summary>
    public abstract class OauthConnector
    {
        public enum OauthType
        {
            facebook, github, linkedin, oschina, qq, twitter, wechat, weibo, baidu
        }

        // 第一步，构建跳转的URL，跳转后用户登录成功，返回到callback url，并带上code
        // 第二步，通过code，获取access token
        // 第三步，通过 access token 获取用户的open_id
        // 第四步，通过 open_id 获取用户信息      
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public OauthType Type { get; set; }
        public string RedirectUri { get; set; }
       

        public OauthConnector(String appkey, String appSecret)
        {
            this.ClientId = appkey;
            this.ClientSecret = appSecret;
        }
        public String GetAuthorizeUrl(String state, String redirectUri)
        {
            this.RedirectUri = redirectUri;
            return CreateAuthorizeUrl(state);
        }

        protected String HttpGet(String url)
        {
            HttpHelper httpHelper = HttpHelper.Getinstance();
            var html = httpHelper.GetAsync<string>(url, null, null).Result;          
            return html;
        }

        protected abstract String CreateAuthorizeUrl(String state);

        public abstract AccessToken GetAccessToken(String code);

        protected abstract OauthUser GetOauthUser(String code);

        public OauthUser GetUser(String code)
        {
            return GetOauthUser(code);
        }
        /// <summary>
        /// 刷新AccessToken
        /// </summary>
        /// <param name="refresh_token"></param>
        /// <returns></returns>
        public abstract AccessToken RefreshAccessToken(String refresh_token);
    }
}
