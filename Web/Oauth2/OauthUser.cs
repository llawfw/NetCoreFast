﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Oauth2
{
    public class OauthUser:ResultMsg
    {
        public string OpenId { get ; set ; }
        public string Nickname { get ; set ; }
        public string Avatar { get ; set; }
        /// <summary>
        /// male/female/unknow
        /// </summary>
        public string Gender { get ; set ; }
        public string Source { get ; set ; }
    }
}
