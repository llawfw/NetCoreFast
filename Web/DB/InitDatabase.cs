﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Model.Entity.Sys;
using Model;

namespace Web.DB
{
    /// <summary>
    /// 初始化数据
    /// </summary>
    public static class InitDatabase
    {
        public static bool Initialize(MyDbContext context)
        {
            // Look for any SysUsers.
            if (context.SysUsers.Any())
            {
                return false;   // DB has been seeded
            }

            var Users = new SysUser[]
            {
            new SysUser{Username="admin",Pswd=Common.Util.Security.Md5("123456"),Role="admin"}
            };
            foreach (SysUser s in Users)
            {
                context.SysUsers.Add(s);
            }
            InitDatas.Init(context);
            context.SaveChanges();
            return true;
        }
    }
}
