﻿using BLL.Sys;
using Microsoft.Extensions.Caching.Distributed;
using Model.Entity.Sys;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Web.Extension;
using Web.Util;

namespace Web.Security
{
    /// <summary>
    /// 加载权限资源应用
    /// </summary>
    public class AuthorityLoadWebApp : IWebApp
    {
        public ISysRoleBll roleBll { get; set; }
        public ISysRoleAuthorityBll roleAuthorityBll { get; set; }
        public ISysAuthorityBll authorityBll { get; set; }
        public ISysAuthorityResBll authorityResBll { get; set; }
        public ISysResBll resBll { get; set; }
        public IDistributedCache cache { get; set; }
        public string Name { get; } = "权限加载应用";
        public string Desc { get; } = "加载系统角色->权限->资源";

        public void OnStarted()
        {
            /*
            //初始测试数据
            //权限
            Authority authority = new Authority() { Name = "图片管理", Code = "image" };
            authorityBll.Add(authority);
            //权限资源
            Res res = resBll.SelectOne(o => o.Id == 3);
            AuthorityRes authorityRes = new AuthorityRes() { Authority = authority,Res= res };
            authorityResBll.Add(authorityRes);
            //角色
            Role role = new Role() { Name = "管理员", Code = "manger" };
            roleBll.Add(role);
            //角色权限
            RoleAuthority roleAuthority = new RoleAuthority() {Role=role,Authority=authority };
            roleAuthorityBll.Add(roleAuthority);
            */

            RoleResMap roleRess = new RoleResMap();
            //查询资源
            var ress = resBll.SelectAllAsync().Result;
            foreach (var o in ress)
            {
                roleRess.AddRes(o.Value);
            }
            // cache.Set("44", "叮咚");
            // Console.WriteLine($"初始权限资源"+cache.GetType()+"_"+cache.Get<string>("44"));
            //查询角色
            var roles = roleBll.SelectAll();
            foreach (var r in roles)
            {
                //Console.WriteLine("角色：" + r.Name);
                //角色对应资源序号索引
                List<int> ressindex = new List<int>();
                //角色对应权限
                var roleAuthorites = roleAuthorityBll.SelectAll(o => o.RoleId == r.Id);
                foreach (var a in roleAuthorites)
                {
                    //Console.WriteLine("权限：" + a.Authority.Name);
                    //权限对应资源
                    var authorityRess = authorityResBll.SelectAll(o => o.AuthorityId == a.AuthorityId);
                    foreach (var _authorityRes in authorityRess)
                    {
                        //Console.WriteLine("资源：" + _authorityRes.Res.Name);
                        int idx = roleRess.GetResIndex(_authorityRes.Res.Value);
                        if (idx != -1)//资源存在添加到角色资源列表
                        {
                            ressindex.Add(idx);
                        }
                    }
                }
                //保存对应角色和资源下标索引
                roleRess.AddRoleRess(r.Code, ressindex);
            }
            cache.Set(RoleResMap.CACHE_KEY, roleRess);
            //Console.WriteLine($"info: 权限资源加载成功!【{DateTime.Now}】");
            NLogHelper.logger.Info($"权限资源加载成功!");
            // var rr = cache.Get<RoleResMap>(RoleResMap.CACHE_KEY);
            //Console.WriteLine("资源：" + JsonConvert.SerializeObject(rr.GetRoleRess("manger")));
            // Console.WriteLine("应该有资源：" + rr.RoleHasRes("manger", "res_add"));
        }
        public void OnStopped()
        {
        }
        public void OnStopping()
        {

        }
    }
    /// <summary>
    /// 角色资源映射
    /// </summary>
    [Serializable]
    public class RoleResMap
    {
        /// <summary>
        /// 本对象实例缓存键名称
        /// </summary>
        public static string CACHE_KEY = "RoleResMap";
        /// <summary>
        /// 所有资源。资源不区分大小写
        /// </summary>
        public List<string> ress = new List<string>();
        /// <summary>
        /// 角色拥有的资源索引index
        /// </summary>
        public Dictionary<string, List<int>> roleRess = new Dictionary<string, List<int>>();
        /// <summary>
        /// 添加资源
        /// </summary>
        /// <param name="res">不区分大小写</param>
        public void AddRes(string res)
        {
            ress.Add(res.ToLower());
        }
        /// <summary>
        /// 添加角色对应资源
        /// </summary>
        /// <param name="roleCode">不区分大小写</param>
        /// <param name="ressindex"></param>
        public void AddRoleRess(string roleCode, List<int> ressindex)
        {
            if (!roleRess.ContainsKey(roleCode.ToLower()))
            {
                roleRess.Add(roleCode.ToLower(), ressindex);
            }
        }
        /// <summary>
        /// 获取角色拥有的所有资源值
        /// </summary>
        /// <param name="roleCode">角色码，不区分大小写</param>
        /// <returns></returns>
        public IEnumerable<string> GetRoleRess(string roleCode)
        {
            roleCode = roleCode.ToLower();
            if (!roleRess.ContainsKey(roleCode)) return null;
            List<int> s = roleRess[roleCode];
            return ress.Where((item, index) => s.IndexOf(index) != -1);
        }
        /// <summary>
        /// 角色是否拥有资源
        /// </summary>
        /// <param name="roleCode">角色码</param>
        /// <param name="resValue">资源值</param>
        /// <returns></returns>
        public bool RoleHasRes(string roleCode, string resValue)
        {
            var ress = GetRoleRess(roleCode);//资源
            return ress != null && ress.Contains(resValue.ToLower());
        }
        /// <summary>
        /// 获取资源在资源数字下标
        /// </summary>
        /// <param name="value">资源值，不区分大小写</param>
        /// <returns></returns>
        public int GetResIndex(string value)
        {
            for (int i = 0; i < ress.Count; i++)
            {
                if (ress[i] == value.ToLower()) return i;
            }
            return -1;
        }
    }
}
