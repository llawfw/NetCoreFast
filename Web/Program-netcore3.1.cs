using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Web.Autofac;
using Web.DB;
using NLog.Web;
using Model;
using Microsoft.Extensions.DependencyInjection;

namespace Web
{
   /*
    public class Program
    {
        public static void Main(string[] args)
        {           
            CreateHostBuilder(args).Build().InitDB<MyDbContext>().Run();//运行项目，执行本语句
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            //设置自定义修改启动端口配置文件
            var configuration = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory).AddJsonFile("host.json").Build();
            return Host.CreateDefaultBuilder(args)
                //.ConfigureServices(sc => sc.AddHostedService<LifetimeEventsHostedService>())
                .UseAutofacServiceProviderFactory()//将默认ServiceProviderFactory指定为AutofacServiceProviderFactory
                    .ConfigureWebHostDefaults(webBuilder =>
                    {
                        //应用配置文件
                        webBuilder.UseConfiguration(configuration).UseStartup<Startup>();
                    });
        }
    }*/
}
