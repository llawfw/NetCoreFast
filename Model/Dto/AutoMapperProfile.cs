using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Model;
using Model.Entity;
namespace Model.Dto
{

    /// <summary>
    /// 配置构造函数，用来创建关系映射.用来配置所有的映射关系。
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
                               
               CreateMap< Model.Entity.Sys.SysArea , Model.Dto.Sys.SysAreaListDto>();
               CreateMap< Model.Dto.Sys.SysAreaAddDto ,Model.Entity.Sys.SysArea >();
               CreateMap< Model.Dto.Sys.SysAreaUpdateDto , Model.Entity.Sys.SysArea>();
                               
               CreateMap< Model.Entity.Sys.SysAuthority , Model.Dto.Sys.SysAuthorityListDto>();
               CreateMap< Model.Dto.Sys.SysAuthorityAddDto ,Model.Entity.Sys.SysAuthority >();
               CreateMap< Model.Dto.Sys.SysAuthorityUpdateDto , Model.Entity.Sys.SysAuthority>();
                               
               CreateMap< Model.Entity.Sys.SysAuthorityRes , Model.Dto.Sys.SysAuthorityResListDto>();
               CreateMap< Model.Dto.Sys.SysAuthorityResAddDto ,Model.Entity.Sys.SysAuthorityRes >();
               CreateMap< Model.Dto.Sys.SysAuthorityResUpdateDto , Model.Entity.Sys.SysAuthorityRes>();
                               
               CreateMap< Model.Entity.Sys.Sysconfig , Model.Dto.Sys.SysconfigListDto>();
               CreateMap< Model.Dto.Sys.SysconfigAddDto ,Model.Entity.Sys.Sysconfig >();
               CreateMap< Model.Dto.Sys.SysconfigUpdateDto , Model.Entity.Sys.Sysconfig>();
                               
               CreateMap< Model.Entity.Sys.SysDepartment , Model.Dto.Sys.SysDepartmentListDto>();
               CreateMap< Model.Dto.Sys.SysDepartmentAddDto ,Model.Entity.Sys.SysDepartment >();
               CreateMap< Model.Dto.Sys.SysDepartmentUpdateDto , Model.Entity.Sys.SysDepartment>();
                               
               CreateMap< Model.Entity.Sys.SysDocument , Model.Dto.Sys.SysDocumentListDto>();
               CreateMap< Model.Dto.Sys.SysDocumentAddDto ,Model.Entity.Sys.SysDocument >();
               CreateMap< Model.Dto.Sys.SysDocumentUpdateDto , Model.Entity.Sys.SysDocument>();
                               
               CreateMap< Model.Entity.Sys.SysRes , Model.Dto.Sys.SysResListDto>();
               CreateMap< Model.Dto.Sys.SysResAddDto ,Model.Entity.Sys.SysRes >();
               CreateMap< Model.Dto.Sys.SysResUpdateDto , Model.Entity.Sys.SysRes>();
                               
               CreateMap< Model.Entity.Sys.SysRole , Model.Dto.Sys.SysRoleListDto>();
               CreateMap< Model.Dto.Sys.SysRoleAddDto ,Model.Entity.Sys.SysRole >();
               CreateMap< Model.Dto.Sys.SysRoleUpdateDto , Model.Entity.Sys.SysRole>();
                               
               CreateMap< Model.Entity.Sys.SysRoleAuthority , Model.Dto.Sys.SysRoleAuthorityListDto>();
               CreateMap< Model.Dto.Sys.SysRoleAuthorityAddDto ,Model.Entity.Sys.SysRoleAuthority >();
               CreateMap< Model.Dto.Sys.SysRoleAuthorityUpdateDto , Model.Entity.Sys.SysRoleAuthority>();
                               
               CreateMap< Model.Entity.Sys.SysUser , Model.Dto.Sys.SysUserListDto>();
               CreateMap< Model.Dto.Sys.SysUserAddDto ,Model.Entity.Sys.SysUser >();
               CreateMap< Model.Dto.Sys.SysUserUpdateDto , Model.Entity.Sys.SysUser>();
                               
               CreateMap< Model.Entity.Sys.SysVisitRecord , Model.Dto.Sys.SysVisitRecordListDto>();
               CreateMap< Model.Dto.Sys.SysVisitRecordAddDto ,Model.Entity.Sys.SysVisitRecord >();
               CreateMap< Model.Dto.Sys.SysVisitRecordUpdateDto , Model.Entity.Sys.SysVisitRecord>();
            
            //属性匹配，匹配源类中WorkEvent.Date到EventDate
            //  .ForMember(dest => dest.EventDate, opt => opt.MapFrom(src => src.WorkEvent.Date))
            //.ForMember(dest => dest.SomeValue, opt => opt.Ignore())//忽略目标类中的属性
            //.ForMember(dest => dest.TotalAmount, opt => opt.MapFrom(src => src.TotalAmount ?? 0))//复杂的匹配
            //.ForMember(dest => dest.OrderDate, opt => opt.UserValue<DateTime>(DateTime.Now)); 固定值匹配;
        }
    }

}