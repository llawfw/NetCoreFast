﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using Microsoft.EntityFrameworkCore;

namespace Model.Entity
{
    /// <summary>
    /// 实体类的公共属性
    /// </summary>
    public abstract class ID : ISys, IID
    {
        /// <summary>
        /// ID编号，系统自动生成
        /// </summary>
        [DtoGen(NotAdd = true)]
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 添加时间，系统自动生成
        /// </summary>
       // [SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate  =true)]
        public DateTime? Added_time { get; set; } = DateTime.Now;//添加时间
        /// <summary> 
        /// 修改时间
        /// </summary>
        [DtoGen(AddUpdateButInvisible = true)]
        public DateTime? Modify_time { get; set; } = DateTime.Now;
        /// <summary>
        /// 是否是系统资源标识，系统自动判断赋值，默认为true。true，为系统、行政，即学校或系统，false为个人。为true表示是系统发布的信息资源
        /// </summary>
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(AddUpdateButInvisible = true)]
        public bool? Sys { get; set; } = true;
        /// <summary>
        /// 公开，是否公开显示到前台界面，默认true
        /// </summary>
        public bool? Open { get; set; } = true;
        /// <summary>
        /// 显示到前台界面，审核是否通过。有些资源需要申请为公开，然后审核通过后才能显示到前台界面,默认为true
        /// </summary>
        public bool? Passed { get; set; } = true;
        /// <summary>
        /// 状态是否启用，可以作为软删除标志
        /// </summary>
        public bool? Enabled { get; set; } = true;
        /// <summary>
        /// 记录排序因子,默认系统根据Sort降序排序，后添加时间Added_time降序排序，客户端前台可以通过查询参数指定排列方式，默认为0
        /// </summary>    
        public int? Sort { get; set; } = 0;
    }
}
