﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model.Entity
{
    /// <summary>
    /// 表示系统自动赋值的属性
    /// </summary>
    public interface ISys
    {
        /// <summary>
        /// 标致是否系统资源
        /// </summary>
        public bool? Sys { get; set; }      
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? Modify_time { get; set; }
    }
}
