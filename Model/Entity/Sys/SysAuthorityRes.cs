﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using Microsoft.EntityFrameworkCore;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 权限资源
    /// </summary>
    /// <see cref="https://docs.microsoft.com/zh-cn/ef/core/modeling/relationships?tabs=fluent-api%2Cfluent-api-simple-key%2Csimple-key"/>
    [Serializable]
    [Table("Sys_AuthorityRes")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(UserId))]
    [Index(nameof(AuthorityId), nameof(ResId), Name = "idx_Sys_AuthorityRes")]
    public class SysAuthorityRes : UserID
    {
        /// <summary>
        /// 权限
        /// </summary>
        [Required(ErrorMessage = "权限必填")]
        public int AuthorityId { get; set; }
        /// <summary>
        /// 权限对象。客户端做增加修改时不需要给此属性赋值（修改对应的authorityId即可）
        /// </summary>
       // [SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true)]
        public virtual SysAuthority Authority { get; set; }
        /// <summary>
        /// 资源
        /// </summary>
        [Required(ErrorMessage = "资源必填")]
        public int ResId { get; set; }
        /// <summary>
        /// 资源对象。客户端做增加修改时不需要给此属性赋值（修改对应的resId即可）
        /// </summary>
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true)]
        public virtual SysRes Res { get; set; }
    }
}