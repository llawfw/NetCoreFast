﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 角色权限
    /// </summary>
    /// <see cref = " https://docs.microsoft.com/zh-cn/ef/core/modeling/relationships?tabs=fluent-api%2Cfluent-api-simple-key%2Csimple-key" />
    [Serializable]
    [Table("Sys_RoleAuthority")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(UserId))]
    [Index(nameof(AuthorityId), nameof(RoleId))]
    public class SysRoleAuthority : UserID
    {
        /// <summary>
        /// 角色
        /// </summary>
        [Required(ErrorMessage = "角色必填")]
        public int RoleId { get; set; }
        /// <summary>
        /// 对应角色对象， 客户端做增加修改时不需要给此属性赋值（只需要修改roleId属性值）
        /// </summary>
       // [SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true)]
        public virtual SysRole Role { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        [Required(ErrorMessage = "权限必填")]
        public int AuthorityId { get; set; }
        /// <summary>
        /// 对应权限对象， 客户端做增加修改时不需要给此属性赋值（只需要修改authorityId属性值）
        /// </summary>
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true)]
        public virtual SysAuthority Authority { get; set; }
    }
}