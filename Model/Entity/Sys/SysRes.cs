﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 资源
    /// </summary>
    [Serializable]
    [Table("Sys_Res")]
    [Index( nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(UserId))]
    [Index(nameof(Name))]
    public class SysRes : UserID
    {
        /// <summary>
        /// 资源名称
        /// </summary>
        [Display(Name = "名称")]
        [Required(ErrorMessage = "名称必填")]
        [StringLength(50)]
        public string Name { get; set; }
        /// <summary>
        ///  资源值,如网址、方法名
        /// </summary>
        [Required(ErrorMessage = "资源值必填")]
        public string Value { get; set; }
        /// <summary>
        /// 资源简介
        /// </summary>
        [Display(Name = "简介")]
        public string? Intro { get; set; }
    }
}