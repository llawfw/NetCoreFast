﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Common.MyAttribute;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 部门
    /// </summary>
    [Serializable]
    [Table("Sys_Department")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(ParentId))]
    [Index(nameof(Name))]
    public class SysDepartment : UserTreeID<SysDepartment>
    {
        public SysDepartment()
        {
            Childs = new HashSet<SysDepartment>();
        }
        /// <summary>
        /// 部门名称
        /// </summary>
        [Required(ErrorMessage = "部门名称必填")]
        [StringLength(50)]
        public string Name { get; set; }
        /// <summary>
        /// 部门电话
        /// </summary>
        public string? Tel { get; set; }
        /// <summary>
        /// 部门邮箱
        /// </summary>
        public string? Email { get; set; }
        /// <summary>
        /// 部门领导id
        /// </summary>
        public int? LeaderId { get; set; }
        /// <summary>
        /// 部门领导。客户端做增加修改时不需要给此属性赋值（修改对应的LeaderId即可）
        /// </summary>
        [ForeignKey("LeaderId")]
        //[SwaggerSchema(ReadOnly = true)]
        [DtoGen(NotAddUpdate = true)]
        public virtual  SysUser Leader { get; set; }//延迟加载与无跟踪在一起使用的时候就不能加载导航属性了。
        /// <summary>
        /// 领导姓名
        /// </summary>
        [NotMapped]
        [FromModel("Leader.Realname")]
        [DtoGen(NotAddUpdate = true)]
        public string LeaderRealname { get; set; }
        /// <summary>
        /// 领导用户名
        /// </summary>
        [NotMapped]
        [FromModel("Leader.Username")]
        [DtoGen(NotAddUpdate = true)]
        public string LeaderUsername { get; set; }
        /// <summary>
        /// 部门简介
        /// </summary>
        [Display(Name = "简介")]
        public string? Intro { get; set; }

    }
}
