﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Swashbuckle.AspNetCore.Annotations;
using Common.MyAttribute;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 访问记录
    /// </summary>
    [Serializable]
    [Table("Sys_VisitRecord")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(UserId))]
    //[Index(nameof(Name))]
    [ControllerGen(List =true,Delete =true)]
    public class SysVisitRecord : UserID
    {
        /// <summary>
        /// 客户端IP
        /// </summary>
        public string Ip { get; set; }
        /// <summary>
        /// 请求路径
        /// </summary>
        public string? Path { get; set; }
        /// <summary>
        /// 查询参数
        /// </summary>
        public string? QueryString { get; set; }
        /// <summary>
        /// 请求方法
        /// </summary>
        public string? Method { get; set; }
        /// <summary>
        /// 用户代理
        /// </summary>
        public string? UserAgent { get; set; }
        /// <summary>
        /// 耗时
        /// </summary>
        public long? Time { get; set; }
    }
}