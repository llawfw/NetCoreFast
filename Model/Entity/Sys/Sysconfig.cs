﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Model;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entity.Sys
{
    /// <summary>
    /// 系统配置
    /// </summary>
    [Serializable]
    [Table("Sys_Config")]
    [Index(nameof(Added_time), nameof(Sys), nameof(Open), nameof(Passed), nameof(Sort), nameof(UserId))]
    public class Sysconfig : UserID
    {
        /// <summary>
        /// 站点名字
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 网站标题
        /// </summary>
        public string Title { get; set; }//将显示在浏览器标题等位置

        /// <summary>
        /// 网站LOGO
        /// </summary>
        public string? Logo { get; set; }// 站点logo
        /// <summary>
        /// 百度盘refresh_token,有效期10年
        /// </summary>
        public string? Baidupan_refresh_token { get; set; }
        /// <summary>
        /// 访问量
        /// </summary>
        public long? Visit_count { get; set; }

        //@EntityFieldMeta(label = "第三方流量统计代码", tips = "设置网站统计代码，复制第三方统计代码到此即可", order = 1021)
        // @Lob @Basic(fetch = LAZY)
        // @Column(columnDefinition = "LongText")
        //public String statistics_code{get;set;}
        //@EntityFieldMeta(label = "第三方分享代码 ", tips = "请使用百度“图标式”代码风格且保留2个按钮图标", order = 1021)
        //@Basic(fetch = LAZY)
        // @Column(columnDefinition = "LongText")
        //public String share_code{get;set;}

        /// <summary>
        /// 站点状态
        /// </summary>
        //public bool state { get; set; }//对站点进行管理关闭状态将无法访问但不妨碍后台管理
        // @EntityFieldMeta(label = "状态说明", tips = "站点处于关闭状态时，关闭原因显示在前台", order = 1021)
        // @Lob @Basic(fetch = LAZY)
        // @Column(columnDefinition = "LongText")
        // public String state_info { get; set; }
        // @EntityFieldMeta(label = "网站版权", order = 1021)

        //public String copy_right { get; set; }// 商城版权
        // @EntityFieldMeta(label = "公司名称", order = 1021)

        // public String company_name { get; set; }// 公司名称
        // @EntityFieldMeta(label = "热门搜索", tips = "显示在搜索框下面 前台点击时直接作为关键词进行搜索 新增多个关键词是用\" , \"隔开", order = 1021)

        //public String hot_search { get; set; }// 热门搜索
        // @EntityFieldMeta(label = "会员站点数", tips = "会员最多允许建立多少个站点（或者店铺）", order = 1021)

        // public Integer user_site_num { get; set; }//

        // @EntityFieldMeta(label = "注册角色", tips = "会员注册时默认赋予的角色,多个用\",\"分割", order = 1021)

        // public String register_roles { get; set; }
        //    @EntityFieldMeta(label = "允许游客(匿名)咨询", tips = "是否允许游客在进行咨询", order = 1021)

        // public boolean anonymous_consult { get; set; }
        //   @EntityFieldMeta(label = "验证码类型", tips = "normal普通验证码voice语音验证码", order = 1021)

        //public String security_code_type { get; set; }
        // @EntityFieldMeta(label = "前台注册使用验证码", order = 1021)

        /// public boolean security_code_register { get; set; }
        // @EntityFieldMeta(label = "前台登陆使用验证码", order = 1021)

        // public boolean security_code_login { get; set; }
        //  @EntityFieldMeta(label = "咨询使用验证码", order = 1021)

        // public boolean security_code_consult { get; set; }
        //   @EntityFieldMeta(label = "AES对称加密密钥", tips = "16位,不填会使用默认值", order = 1021)

        // public String security_aes_key { get; set; }
        //    @EntityFieldMeta(label = "平台客服电话", tips = "设置平台客服电话，多个用,分割开", order = 1021)
        // @Column(columnDefinition = "LongText")
        //   public String service_telphone { get; set; }
        //  @EntityFieldMeta(label = "平台客服QQ", tips = "设置平台客服QQ，多个用,分割开", order = 1021)
        //// @Column(columnDefinition = "LongText")
        //   public String service_qq { get; set; }

        //  @EntityFieldMeta(label = "是否启用qq互联功能", tips = "开启后可使用QQ账号登录系统", order = 1021)
        // @Column(columnDefinition = "bit default 0")
        //  public boolean qq_login { get; set; }
        //   @EntityFieldMeta(label = "应用标识", tips = "<a href=\"http://connect.qq.com\" target=\"_blank\">立即在线申请</a>", order = 1021)

        //public String qq_login_id { get; set; }
        //   @EntityFieldMeta(label = "应用密钥", order = 1021)

        //public String qq_login_key { get; set; }
        //   @EntityFieldMeta(label = "域名验证信息", order = 1021)
        // @Column(columnDefinition = "LongText")
        //    public String qq_domain_code { get; set; }
        //    @EntityFieldMeta(label = "是否启用新浪微博互联功能", tips = "开启后可使用新浪微博账号登录商城系统", order = 1021)
        // @Column(columnDefinition = "bit default 0")
        //   public boolean sina_login { get; set; }
        //   @EntityFieldMeta(label = "应用标识", tips = "<a href=\"http://open.weibo.com/index.php\" target=\"_blank\">立即在线申请</a>", order = 1021)

        //  public String sina_login_id { get; set; }
        //   @EntityFieldMeta(label = "应用密钥", order = 1021)

        /// public String sina_login_key { get; set; }
        //    @EntityFieldMeta(label = "域名验证信息", order = 1021)
        // @Column(columnDefinition = "LongText")
        //   public String sina_domain_code { get; set; }
        //    @EntityFieldMeta(label = "是否启用微信互联功能", tips = "开启后可使用微信扫描登录商城系统", order = 1021)

        // public boolean weixin_login { get; set; }// 是否启用微信登陆
        //     @EntityFieldMeta(label = "应用标识(appId)", tips = "<a href=\"https://open.weixin.qq.com/\" target=\"_blank\">立即在线申请</a>", order = 1021)

        //  public String weixin_open_appId { get; set; }// 微信开放平台提交应用审核通过后获得,用于微信登陆
        //    @EntityFieldMeta(label = "应用密钥(appSecret)", order = 1021)

        // public String weixin_open_appSecret { get; set; }// 应用密钥AppSecret，在微信开放平台提交应用审核通过后获得，用于微信登陆
        //    @EntityFieldMeta(label = "应用密钥", tips = "百度应用秘钥，查询天气等百度服务需要此参数", order = 1021)

        // public String baidu_ak { get; set; }// 百度应用秘钥
        //    @EntityFieldMeta(label = "登陆图片", type = "image", tips = "友情提示:该处设置用户前台登录页左侧图片，可上传4张，每次随机显示一张", order = 1021)
        // @OneToMany(mappedBy = "config")
        //    @ElementCollection
        // @CollectionTable(name = "sys_login_images")

        // public List<String> login_images = new ArrayList<String>() { get; set;}
        // @EntityFieldMeta(label = "短信功能开启", group = "sms", groupLabel = "短信设置", groupOrder = 100, tips = "设置是否启用手机短信功能", order = 1021)

        // public boolean sms_enbale { get; set; }
        // @EntityFieldMeta(label = "短信网关", group = "sms", groupLabel = "短信设置", order = 1021)

        //  public String sms_url { get; set; }
        //  @EntityFieldMeta(label = "短信平台用户名", group = "sms", groupLabel = "短信设置", order = 1021)

        // public String sms_username { get; set; }
        //  @EntityFieldMeta(label = "短信平台密码", group = "sms", groupLabel = "短信设置", tips = "短信平台发送密码", order = 1021)

        // public String sms_password { get; set; }
        // @EntityFieldMeta(label = "短信发送测试", group = "sms", groupLabel = "短信设置", order = 1021)

        //  public String sms_test { get; set; }

        //  @EntityFieldMeta(label = "邮件功能开启", tips = "设置是否启用邮件功能", group = "email", groupLabel = "Email设置 ", groupOrder = 150, order = 1021)

        public bool? Email_enable { get; set; }
        //  @EntityFieldMeta(label = "SMTP服务器", tips = "SMTP邮件服务器地址", group = "email", groupLabel = "Email设置 ", order = 1021)

        // public String email_host { get; set; }
        // @EntityFieldMeta(label = "STMP端口", tips = "SMTP邮件服务器端口，常见默认为25", group = "email", groupLabel = "Email设置 ", order = 1021)

        // public int email_port { get; set; }
        //  @EntityFieldMeta(label = "邮件发送账号", tips = "设置发件人邮件账号", group = "email", groupLabel = "Email设置 ", order = 1021)

        //  public String email_account { get; set; }
        // @EntityFieldMeta(label = "发件人", tips = "设置邮件发件人名称", group = "email", groupLabel = "Email设置 ", order = 1021)

        // public String email_username { get; set; }
        // @EntityFieldMeta(label = "邮箱登陆密码", tips = "设置发件人邮箱登录密码", group = "email", groupLabel = "Email设置 ", order = 1021)

        /// public String email_password { get; set; }
        // @EntityFieldMeta(label = "邮件发送测试", tips = "邮件发送测试收件账号", group = "email", groupLabel = "Email设置 ", order = 1021)

        // public String email_test { get; set; }

        // @EntityFieldMeta(label = "关键字", group = "seo", groupLabel = "SEO设置", groupOrder = 200, tips = "网站关键字，出现在前台页面头部的 Meta 标签中，用于记录该页面的关键字，多个关键字间请用半角逗号 \",\" 隔开", order = 1021)

        //  public String keywords { get; set; }// 商城关键词
        // @EntityFieldMeta(label = "网站描述", group = "seo", groupLabel = "SEO设置", tips = "网站描述，出现在前台页面头部的 Meta 标签中，用于记录该页面的概要与描述", order = 1021)

        //  public String description { get; set; }// 商城描述

        //  @EntityFieldMeta(label = "开启域名解析", group = "domain", groupLabel = "域名解析", groupOrder = 15, tips = "是否开启域名解析服务，包括顶级、二级、三级域名解析", order = 1021)

        // public boolean dns = true{get;set;}
        //@EntityFieldMeta(label = "允许修改次数", group = "domain", groupLabel = "域名解析", tips = "该数值限制修改店域名次数", order = 1021)

        //public int domain_modify_times { get; set; }
        //@EntityFieldMeta(label = "系统保留域名", group = "domain", groupLabel = "域名解析", tips = "域名解析开启时那些网址不需要解析,多个用(baidu.com)|(sin.com)", order = 1021)


        //public String domain_retain { get; set; }
        // @EntityFieldMeta(label = "解析上下文", group = "domain", groupLabel = "域名解析", tips
        // = "域名上下文", order = 1021)
        // public String domain_context{get;set;}

        ///@EntityFieldMeta(label = "文件存放类型", groupLabel = "上传设置", group = "upload", groupOrder = 500, tips = "sidImg按照文件名存放(例:/店铺id/图片)sidYearImg 按照年份存放(例:/店铺id/年/图片)sidYearMonthImg 按照年月存放(例:/店铺id/年/月/图片)sidYearMonthDayImg按照年月日存放(例:/店铺id/年/月/日/图片)", order = 1021)

        //  public String upload_save_type { get; set; }
        //@EntityFieldMeta(label = "文件服务器", groupLabel = "上传设置", group = "upload", tips = "使用图片服务器后能够有效缓解商城服务器压力,上传图片的服务器地址，主要用于上传,若未设置显示服务器则此为显示服务器,多个用逗号分隔开，暂不支持多个", order = 1021)

        //  public String upload_server { get; set; }
        //@EntityFieldMeta(label = "保存根目录", groupLabel = "上传设置", group = "upload", tips = "图片上传保存根目录，以file://(window)。默认为网站根目录", order = 1021)

        //  public String upload_file_root { get; set; }// 文件上传地址
        //@EntityFieldMeta(label = "保存文件夹", groupLabel = "上传设置", group = "upload", tips = "图片上传路径，相对上传保存根目录，填写文件夹名，默认为upload，改变名称可以有效保护图片资源", order = 1021)

        //  public String upload_file_path { get; set; }// 文件上传地址
        //@EntityFieldMeta(label = "图片大小", groupLabel = "上传设置", group = "upload", tips = "KB &nbsp{get;set;}&nbsp{get;set;}&nbsp{get;set;}&nbsp{get;set;}(1024KB=1MB)当前最大允许上传10MB 的文件，您的设置请勿超过该值。", order = 1021)

        // public int upload_image_size { get; set; }
        //@EntityFieldMeta(label = "图片的扩展名", groupLabel = "上传设置", group = "upload", tips = "图片扩展名，用于判断上传图片是否为后台允许，多个后缀名间请用 &quot{get;set;}|&quot{get;set;}或者&quot{get;set;},&quot{get;set;}间隔 <br />\n   隔开。", order = 1021)

        // public String upload_image_ext = "bmp,gif,jpg,jpeg,png"{ get; set; }
        /**
         * 允许的文档后缀
         */
        //@EntityFieldMeta(label = "文档的扩展名", group = "upload", groupLabel = "文件上传设置", tips = "允许上传的文档后缀", order = 1021)

        // public String upload_doc_ext = "doc,docx,xls,xlsx,ppt,pptx,html,htm,txt,rar,zip,gz,bz2,pdf,pdf"{ get; set; }
        /**
         * 允许的音乐后缀
         */
        //@EntityFieldMeta(label = "音频的扩展名", group = "upload", groupLabel = "文件上传设置", tips = "允许上传的音频后缀", order = 1021)

        //  public String upload_music_ext = "mp3,wav,wma"{ get; set; }
        /**
         * 允许的视频后缀
         */
        //@EntityFieldMeta(label = "视频的扩展名", group = "upload", groupLabel = "文件上传设置", tips = "允许上传的视频后缀", order = 1021)

        //  public String upload_video_ext = "swf,flv,wmv,mid,avi,mpg,asf,rm,rmvb"{ get; set; }

        //@EntityFieldMeta(label = "默认会员头像", type = "image", tips = "128px&nbsp{get;set;}*&nbsp{get;set;}128px", order = 1021)
        // @OneToOne(cascade = {javax.persistence.CascadeType.ALL}, fetch =
        // FetchType.LAZY)
        //public String member_icon { get; set; }
        //@EntityFieldMeta(label = "模板目录", tips = "模板存放目录,默认/WEB-INF/velocity/", order = 1021)
        // @OneToOne(cascade = {javax.persistence.CascadeType.ALL}, fetch =
        // FetchType.LAZY)
        //public String template_dir { get; set; }

        //@EntityFieldMeta(label = "索引目录", tips = "索引存放目录,默认用戶目录", order = 1021)
        // @OneToOne(cascade = {javax.persistence.CascadeType.ALL}, fetch =
        // FetchType.LAZY)
        //public String lucene_dir { get; set; }
        //@EntityFieldMeta(label = "索引更新时间", order = 1021)

        //  @Temporal(javax.persistence.TemporalType.DATE)

        //  public Date lucene_update_time { get; set; }

        //@EntityFieldMeta(label = "网站名称", order = 1021)

        //  public int integralRate { get; set; }
        //@EntityFieldMeta(label = "是否启用积分", groupLabel = "积分设置", group = "integral", groupOrder = 50, tips = "积分启用后，会员注册、登录会获取对应积分，卖家商品可以使用积分抵扣，商品购买成功后可以获取积分", order = 1021)

        //  public boolean integral { get; set; }
        //@EntityFieldMeta(label = "会员注册", groupLabel = "积分设置", group = "integral", order = 1021)

        //   public int integral_member_register { get; set; }
        //@EntityFieldMeta(label = "会员每天登陆", groupLabel = "积分设置", group = "integral", order = 1021)

        //  public int integral_member_daylogin { get; set; }

        //@EntityFieldMeta(label = "是否启用预存款", tips = "预存款启用后，会员可以给自己帐户充值进行交易", order = 1021)

        //   public boolean deposit { get; set; }
        //@EntityFieldMeta(label = "是否启用金币", tips = "金币功能启用后，店主可通过平台提供的交易方式进行购买，金币可用来购买广告、直通车等", order = 1021)

        // public boolean gold { get; set; }
        //@EntityFieldMeta(label = "金币兑换率", tips = "一元人民币可兑换(?)枚金币", order = 1021)
    }
}