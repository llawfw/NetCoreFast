﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Model.Migrations
{
    public partial class mysql1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_Area",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    ParentId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Area", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_Area_Sys_Area_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Sys_Area",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_Authority",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Code = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Intro = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Authority", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_AuthorityRes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AuthorityId = table.Column<int>(type: "int", nullable: false),
                    ResId = table.Column<int>(type: "int", nullable: false),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_AuthorityRes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_AuthorityRes_Sys_Authority_AuthorityId",
                        column: x => x.AuthorityId,
                        principalTable: "Sys_Authority",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_Config",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Title = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Logo = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Baidupan_refresh_token = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Visit_count = table.Column<long>(type: "bigint", nullable: true),
                    Email_enable = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Config", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_Department",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Tel = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Email = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    LeaderId = table.Column<int>(type: "int", nullable: true),
                    Intro = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    ParentId = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Department", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_Department_Sys_Department_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Sys_Department",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_User",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DepartmentId = table.Column<int>(type: "int", nullable: true),
                    Wx_openid = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Wx_unionid = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    Username = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Pswd = table.Column<string>(type: "varchar(40)", maxLength: 40, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Realname = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Role = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Tel = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Email = table.Column<string>(type: "varchar(30)", maxLength: 30, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Birthday = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    QQ = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Address = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Gender = table.Column<int>(type: "int", nullable: true),
                    Pic = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Nation = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IDCard = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Zip = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    AreaId = table.Column<int>(type: "int", nullable: true),
                    Available_balance = table.Column<double>(type: "double", nullable: true),
                    Freeze_blance = table.Column<double>(type: "double", nullable: true),
                    Integral = table.Column<int>(type: "int", nullable: true),
                    Gold = table.Column<int>(type: "int", nullable: true),
                    Friend_num = table.Column<int>(type: "int", nullable: true),
                    Attention_num = table.Column<int>(type: "int", nullable: true),
                    Attentioned_num = table.Column<int>(type: "int", nullable: true),
                    Credit = table.Column<int>(type: "int", nullable: true),
                    Last_login_date = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Login_date = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Last_login_ip = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Login_ip = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Login_count = table.Column<int>(type: "int", nullable: true),
                    IMEI = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Emergency_number = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Credentials = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_User_Sys_Area_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Sys_Area",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Sys_User_Sys_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Sys_Department",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_Document",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Name = table.Column<string>(type: "varchar(127)", maxLength: 127, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Path = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Size = table.Column<float>(type: "float", nullable: true),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Document", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_Document_Sys_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Sys_User",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_Res",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Value = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Intro = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Res", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_Res_Sys_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Sys_User",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_Role",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Code = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Intro = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Role", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_Role_Sys_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Sys_User",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_VisitRecord",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Ip = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Path = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    QueryString = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Method = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UserAgent = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Time = table.Column<long>(type: "bigint", nullable: true),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_VisitRecord", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_VisitRecord_Sys_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Sys_User",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Sys_RoleAuthority",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    AuthorityId = table.Column<int>(type: "int", nullable: false),
                    Added_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Modify_time = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Sys = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Open = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Passed = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Enabled = table.Column<bool>(type: "tinyint(1)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_RoleAuthority", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sys_RoleAuthority_Sys_Authority_AuthorityId",
                        column: x => x.AuthorityId,
                        principalTable: "Sys_Authority",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sys_RoleAuthority_Sys_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Sys_Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sys_RoleAuthority_Sys_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Sys_User",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Area_Added_time_Sys_Open_Passed_Sort_ParentId",
                table: "Sys_Area",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "ParentId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Area_Name",
                table: "Sys_Area",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Area_ParentId",
                table: "Sys_Area",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Authority_Added_time_Sys_Open_Passed_Sort_UserId",
                table: "Sys_Authority",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Authority_Code",
                table: "Sys_Authority",
                column: "Code");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Authority_UserId",
                table: "Sys_Authority",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "idx_Sys_AuthorityRes",
                table: "Sys_AuthorityRes",
                columns: new[] { "AuthorityId", "ResId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_AuthorityRes_Added_time_Sys_Open_Passed_Sort_UserId",
                table: "Sys_AuthorityRes",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_AuthorityRes_ResId",
                table: "Sys_AuthorityRes",
                column: "ResId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_AuthorityRes_UserId",
                table: "Sys_AuthorityRes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Config_Added_time_Sys_Open_Passed_Sort_UserId",
                table: "Sys_Config",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Config_UserId",
                table: "Sys_Config",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Department_Added_time_Sys_Open_Passed_Sort_ParentId",
                table: "Sys_Department",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "ParentId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Department_LeaderId",
                table: "Sys_Department",
                column: "LeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Department_Name",
                table: "Sys_Department",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Department_ParentId",
                table: "Sys_Department",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Department_UserId",
                table: "Sys_Department",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Document_Added_time_Sys_Open_Passed_Sort_UserId",
                table: "Sys_Document",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Document_Name",
                table: "Sys_Document",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Document_UserId",
                table: "Sys_Document",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Res_Added_time_Sys_Open_Passed_Sort_UserId",
                table: "Sys_Res",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Res_Name",
                table: "Sys_Res",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Res_UserId",
                table: "Sys_Res",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Role_Added_time_Sys_Open_Passed_Sort_UserId",
                table: "Sys_Role",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Role_Name",
                table: "Sys_Role",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_Role_UserId",
                table: "Sys_Role",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_RoleAuthority_Added_time_Sys_Open_Passed_Sort_UserId",
                table: "Sys_RoleAuthority",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_RoleAuthority_AuthorityId_RoleId",
                table: "Sys_RoleAuthority",
                columns: new[] { "AuthorityId", "RoleId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_RoleAuthority_RoleId",
                table: "Sys_RoleAuthority",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_RoleAuthority_UserId",
                table: "Sys_RoleAuthority",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_User_Added_time_Sys_Open_Passed_Sort",
                table: "Sys_User",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_User_AreaId",
                table: "Sys_User",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_User_DepartmentId",
                table: "Sys_User",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Sys_User_Username_Tel_Email",
                table: "Sys_User",
                columns: new[] { "Username", "Tel", "Email" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_VisitRecord_Added_time_Sys_Open_Passed_Sort_UserId",
                table: "Sys_VisitRecord",
                columns: new[] { "Added_time", "Sys", "Open", "Passed", "Sort", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sys_VisitRecord_UserId",
                table: "Sys_VisitRecord",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sys_Authority_Sys_User_UserId",
                table: "Sys_Authority",
                column: "UserId",
                principalTable: "Sys_User",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Sys_AuthorityRes_Sys_Res_ResId",
                table: "Sys_AuthorityRes",
                column: "ResId",
                principalTable: "Sys_Res",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Sys_AuthorityRes_Sys_User_UserId",
                table: "Sys_AuthorityRes",
                column: "UserId",
                principalTable: "Sys_User",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Sys_Config_Sys_User_UserId",
                table: "Sys_Config",
                column: "UserId",
                principalTable: "Sys_User",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Sys_Department_Sys_User_LeaderId",
                table: "Sys_Department",
                column: "LeaderId",
                principalTable: "Sys_User",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Sys_Department_Sys_User_UserId",
                table: "Sys_Department",
                column: "UserId",
                principalTable: "Sys_User",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sys_Department_Sys_User_LeaderId",
                table: "Sys_Department");

            migrationBuilder.DropForeignKey(
                name: "FK_Sys_Department_Sys_User_UserId",
                table: "Sys_Department");

            migrationBuilder.DropTable(
                name: "Sys_AuthorityRes");

            migrationBuilder.DropTable(
                name: "Sys_Config");

            migrationBuilder.DropTable(
                name: "Sys_Document");

            migrationBuilder.DropTable(
                name: "Sys_RoleAuthority");

            migrationBuilder.DropTable(
                name: "Sys_VisitRecord");

            migrationBuilder.DropTable(
                name: "Sys_Res");

            migrationBuilder.DropTable(
                name: "Sys_Authority");

            migrationBuilder.DropTable(
                name: "Sys_Role");

            migrationBuilder.DropTable(
                name: "Sys_User");

            migrationBuilder.DropTable(
                name: "Sys_Area");

            migrationBuilder.DropTable(
                name: "Sys_Department");
        }
    }
}
