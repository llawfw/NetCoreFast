# 初始项目
## 1、安装必备软件
  （1）安装git客户端[TortoiseGit](https://tortoisegit.org/download/)，并用码云账户登录。 **本项目托管在码云，初始项目需要用git下载NetCoreFast项目，如果想用CodeGen.exe初始更新项目，一定要有git客户端且能登录到码云**     
  （2）[安装Redis](https://www.runoob.com/redis/redis-install.html) 。即时聊天和缓存用到了redis   
  （3）下载[CodeGen.zip](https://foruda.gitee.com/attach_file/1679311330383145313/codegen.zip?token=bafcdf428ce1d915ba271d97253cf3e7&ts=1679311339&attname=CodeGen.zip)代码生成器并解压  
## 2、初始项目
第1步、新建项目文件夹，文件夹名字就是项目的名字。如:D:\project\CMS，其中CMS就是项目的名字。    
第2步、将解压后的CodeGen.exe文件复制到CMS文件夹，并运行CodeGen.exe文件       
### 2.1、生成NetCoreFast Api接口项目    
第1步、输入"1"，选择“NetCoreFast Api接口项目生成”     
第2步、输入"1"，选择“初始项目”。此过程会从码云下载[NetCoreFast](https://gitee.com/qinyongcheng/NetCoreFast)原始项目，并将项目名（解决方案名）改为项目文件夹的名字CMSApi.项目初始成功！    
第3步、用vs打开CMSApi项目，并在Model层添加实体类，然后vs“重新生成”项目（注：添加了实体类一定要重新生成项目）    
第4步、输入"2"，选择“生成代码”，会自动生成bll、controller类     
第5步、输入"3"，选择“删除生成的代码”，会删除生成的bll、controller类【可选】    
第6步、输入"4"，选择“升级项目”，会自动从码云下载[NetCoreFast](https://gitee.com/qinyongcheng/NetCoreFast)最新代码更新项目，因而建议不要更改NetCoreFast代码，便于升级【可选】
 ### 2.2、生成Elementplus-admin-codegen后台前端管理界面项目    
第1步、输入"2"，选择“Elementplus-admin-codegen后台前端管理界面项目生成”     
第2步、输入"1"，选择“初始项目”。此过程会从码云下载[elementplus-admin-codegen](https://gitee.com/qinyongcheng/elementplus-admin-codegen)原始项目，并将项目名改为项目文件夹的名字CMSUI.项目初始成功！      
第3步、输入"4"，选择“升级项目”，会自动从码云下载[elementplus-admin-codegen](https://gitee.com/qinyongcheng/elementplus-admin-codegen)最新代码更新项目，因而建议不要更改[elementplus-admin-codegen](https://gitee.com/qinyongcheng/elementplus-admin-codegen)代码，便于升级【可选】       