# 升级项目 
建议不要更改本框架相关类，可以编写自己单独的类，方便升级。   
运行CodeGen.exe文件，输入"3"选择更新项目。此过程会从码云下载NetCoreFast原始项目，并将代码合并到您的项目。以下文件在更新的过程中不会合并,以避免覆盖您的项目相关信息  
```
excludes.Add(".git");
excludes.Add(@"\.git");
excludes.Add(@"\bin");
excludes.Add(@"\obj");
excludes.Add(@"\Migrations");
excludes.Add(@"\Properties");
excludes.Add(@"README.md");
excludes.Add(@"LICENSE");
excludes.Add($"Startup.cs");
excludes.Add($"Program.cs");
excludes.Add($"InitDatabase.cs");
excludes.Add($"MyDbContext.cs");
excludes.Add($"appsettings.Development.json");
excludes.Add($"appsettings.Production.json");
excludes.Add($"host.json");
excludes.Add($"nlog.config");
excludes.Add(@".csproj");
excludes.Add(@".csproj.user");
excludes.Add(@".sln");
```
 