# 自动生成类代码扩展
如果类代码是自动生成的，向类里增加方法，重新自动生成，会将增加的方法删除，需要一种机制将我们扩展的代码和自动生成的代码合并。
框架有两个扩展合并点，即BLL.Etc目录扩展点和Web.Controllers.Etc目录扩展点。
### 1、BLL.Etc扩展点用于为自动生成的业务逻辑类添加扩展方法   
比如框架自动生成业务类BLL.Sys.IUserBll接口及实现BLL.Sys.Impl.UserBll。自动生成的业务类满足了大多数需求，但如果需要自定义业务方法
比如Login登录方法，则不能直接在BLL.Sys.IUserBll接口和BLL.Sys.Impl.UserBll实现编写login方法，需要在BLL.Etc目录增加扩展代码。具体如下：       
* 在BLL.Etc目录添加模块，模块名称与业务类所在模块名称相同，比如上面的"Sys",即BLL.Etc.Sys  
* 在模块目录下，比如BLL.Etc.Sys目录添加与业务实现类相同的类文件,即扩展文件，如BLL.Etc.Sys.UserBll.cs
* 在扩展文件添加对应业务接口及实现类的扩展方法（也可以是覆盖方法），如: 
           
```
using System;
using System.Collections.Generic;
using System.Text;
using DAL;
using Common.Util;
using Model.Entity.Sys;
using Common.MyAttribute;
namespace BLL.Etc.Sys
{
    /// <summary>
    /// 扩展bll。用于合并到自动生成的对应Bll类。
    /// </summary>
	[NotAutofac]//表示不会注入到容器，仅仅作为合并代码
    public interface IUserBll
    {
        /// <summary>
        /// 用户名密码登录。扩展方法
        /// </summary>
        public User Login(string nuserame, string pswd);     

    }
    /// <summary>
    /// 扩展bll。用于合并到自动生成的对应Bll类。
    /// </summary>
	[NotAutofac]//表示不会注入到容器，仅仅作为合并代码
    public class UserBll : BaseBll<User>, IUserBll

    {
        public new bool Add(User o)//覆盖方法
        {
            o.Pswd = Security.Md5(o.Pswd);
            return dal.Add(o);
        }
        public User Login(string userame, string pswd)
        {
            pswd = Security.Md5(pswd);
            return dal.SelectOne(o => o.Username == userame && o.Pswd == pswd);
        }       
    }
}

```     
运行CodeGen.exe选择自动生成代码，框架会自动将BLL.Etc.Sys.IUserBll及BLL.Etc.Sys.UserBll
里面的方法（包括注释和引入using）合并到正常对应的业务类，如BLL.Etc.IUserBll及BLL.Sys.Impl.UserBll


### 2、Web.Controllers.Etc扩展点用于为自动生成的控制器类添加扩展方法   
比如框架自动生成控制器类Web.Controllers.Sys.DocumentController。自动生成的控制器类满足了大多数需求，但如果需要自定义action方法
比如Upload登录方法，则不能直接在Web.Controllers.Sys.DocumentController编写Upload方法，需要在Web.Controllers.Etc目录增加扩展代码。具体如下：       
* 在Web.Controllers.Etc目录添加模块，模块名称与控制器类所在模块名称相同，比如上面的"Sys",即Web.Controllers.Etc.Sys 
* 在模块目录下，比如Web.Controllers.Etc.Sys目录添加与控制器类相同的类文件,即扩展文件，如Web.Controllers.Etc.Sys.DocumentController.cs
* 在扩展文件添加对应扩展action方法（也可以是覆盖方法），如: 
          
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Sys;
using Common;
using Web.Extension;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Web.Filter;
using Model.Entity.Sys;
using Model.Dto.Sys;
using Common.MyAttribute;

namespace Web.Controllers.Etc.Sys
{
    /// <summary>
    /// 图片资源扩展接口（对自动生成代码controller类的扩展，自动生成时会将本类体内的代码合并到自动生成的类里面）
    /// </summary>
    [NonController]
    [NotAutofac]//表示不会注入到容器，仅仅作为合并代码
    public class DocumentController : MyApiBaseController<Document, ImageListDto, ImageAddDto, ImageUpdateDto>
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        public DocumentController(IImageBll bll, IWebHostEnvironment webHostEnvironment) : base(bll)
        {
            this.webHostEnvironment = webHostEnvironment;
        }
        /// <summary>
        /// 删除图片
        /// </summary>
        /// <param name="id">id值，单个值如：1</param>
        /// <param name="where">不能传递其他条件，目前只能根据id删除单个</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public override async Task<Result<object>> Delete(string id, [FromQuery] Dictionary<string, string> where)
        {
            var idInt = int.Parse(id.Split(",")[0]);
            ……
            return await bll.DeleteAsync(where) ? Result<object>.Success("删除成功") : Result<object>.Error("删除失败");
        }
        /// <summary>
        /// 图片上传
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public async Task<Result<object>> Upload(IFormFile file)
        {
            var ext = Path.GetExtension(file.FileName).ToLowerInvariant();
            if (string.IsNullOrEmpty(ext) || !ImgExt.Contains(ext))
            {
                return Result<object>.Error("上传失败,请选择jpg|jpeg|png|gif类型图片");
            }
            if (file.Length > 0)
            {
               ……
                return Result<object>.Success("上传成功").SetData(o);
            }
            else
            {
                return Result<object>.Success("上传失败,空文件");
            }
        }
    }
}

```     
运行CodeGen.exe选择自动生成代码，框架会自动将Web.Controllers.Etc.Sys.DocumentController.cs
里面的方法（包括注释和引入using）合并到正常对应的控制器类，如Web.Controllers.Sys.DocumentController.cs
